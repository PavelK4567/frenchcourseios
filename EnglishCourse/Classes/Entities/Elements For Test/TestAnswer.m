//
//  Answer.m
//  K1000
//
//  Created by Action Item on 5/8/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "TestAnswer.h"

@implementation TestAnswer

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        self.isCorrectAnswer = [aDecoder decodeBoolForKey:@"correct"];
        self.text = [aDecoder decodeObjectForKey:@"text"];
        self.answerId = [aDecoder decodeObjectForKey:@"answerId"];
    }

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];

    [aCoder encodeBool:_isCorrectAnswer forKey:@"correct"];
    [aCoder encodeObject:_text forKey:@"text"];
    [aCoder encodeObject:_answerId forKey:@"answerId"];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@: %@: elementId: [%@] text: [%@] isCorrectAnswer: [%d]", [super description], NSStringFromClass([self class]), self.answerId, self.text, self.isCorrectAnswer];
}

- (void)fillWithDictionaryElement:(id)element
{
    [super fillWithDictionaryElement:element];

    self.isCorrectAnswer = [[self getObjectFromKey:element key:@"correct"] boolValue];
    self.text = [self getObjectFromKey:element key:@"text"];
    self.answerId = [self getObjectFromKey:element key:@"id"];
}

- (id)copyWithZone:(NSZone *)zone
{
    TestAnswer *answer = [[[self class] allocWithZone:zone] init];
    answer.answerId = [self.answerId copy];
    answer.text = [self.text copy];
    answer.isCorrectAnswer = self.isCorrectAnswer;

    return answer;
}

@end
