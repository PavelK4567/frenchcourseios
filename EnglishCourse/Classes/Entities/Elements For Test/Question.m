//
//  Question.m
//  K1000
//
//  Created by Action-Item on 5/5/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "Question.h"

@implementation Question

- (id)init
{
    if (self = [super init])
    {
        _userSelectedAnswer = -1;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder])
	{
        self.picture = [aDecoder decodeObjectForKey:@"picture"];
        self.question = [aDecoder decodeObjectForKey:@"question"];
        self.answers = [aDecoder decodeObjectForKey:@"options"];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];

    [aCoder encodeObject:_picture forKey:@"picture"];
    [aCoder encodeObject:_question forKey:@"question"];
    [aCoder encodeObject:_answers forKey:@"options"];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@: %@: picture: [%@] question : [%@] options count: [%d]", [super description], NSStringFromClass([self class]), self.picture, self.question, self.answers.count];
}

- (void)fillWithDictionaryElement:(id)element
{
    [super fillWithDictionaryElement:element];

    self.picture = [self getObjectFromKey:element key:@"picture"];
    self.question = [self getObjectFromKey:element key:@"question"];

    NSArray *options = [self getObjectFromKey:element key:@"options" defaultValue:nil];
    if (options.count)
    {
        _answers = [[NSMutableArray alloc] init];
        for (NSDictionary *dict in options)
        {
            TestAnswer *option = [[TestAnswer alloc] init];
            [option fillWithDictionaryElement:dict];
            [_answers addObject:option];
        }
    }
}

- (NSURL *)imageURL
{
  
  if (![self.picture length]) return nil;
  NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"testYourSelf" ofType:@"bundle"];
		if ([[NSFileManager defaultManager] fileExistsAtPath:bundlePath]) {
      NSBundle *bundle = [[NSBundle alloc] initWithPath:bundlePath];
      if (bundle) {
        NSString *filepath = [bundlePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", self.picture]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filepath])
          return [NSURL fileURLWithPath:filepath];
      }
    }
  return [NSURL fileURLWithPath:@"testIcon2"];
}

- (int)correctAnswerIndex
{
    for (int indexAnswer = 0; indexAnswer < self.answers.count ; indexAnswer++)
    {
        if ([[_answers objectAtIndex:indexAnswer] isCorrectAnswer])
        {
            return indexAnswer;
        }
    }

    return -1;
}

- (id)copyWithZone:(NSZone *)zone
{
    Question *question = [[[self class] allocWithZone:zone] init];
    question.question = [self.question copy];
    question.picture = [self.picture copy];
    question.imageURL = [self.imageURL copy];
    question.answers = [[NSMutableArray alloc] initWithArray:self.answers copyItems:YES];
    question.userSelectedAnswer = -1;

    return question;
}

@end
