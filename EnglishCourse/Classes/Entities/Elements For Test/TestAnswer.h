//
//  Answer.h
//  K1000
//
//  Created by Action Item on 5/8/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "Element.h"

@interface TestAnswer : Element <NSCopying>

@property(nonatomic, strong) NSString *answerId;
@property(nonatomic, strong) NSString *text;
@property(nonatomic, unsafe_unretained) BOOL isCorrectAnswer;

@end
