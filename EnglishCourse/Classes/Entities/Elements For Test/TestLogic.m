//
//  TestLogic.m
//  K1000
//
//  Created by Action Item on 5/8/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "TestLogic.h"




@implementation TestLogic
{
    NSOperationQueue *_queue;
    BOOL _unzipedTestsMetaData;
}

- (id)init
{
	if (self = [super init])
	{
        [self initializeWithCoder:nil];
    }
	return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super init])
	{
        [self  initializeWithCoder:aDecoder];
    }
	return self;
}

- (void)initializeWithCoder:(NSCoder *)aDecoder
{
  /*[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loggedin:) name:APP_NOTIFICATION_LOGIN object:nil];
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loggedout:) name:APP_NOTIFICATION_LOGOUT object:nil];
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:FBNetworkReachabilityDidChangeNotification object:nil];
   */
  _unzipedTestsMetaData = [aDecoder decodeBoolForKey:@"unzipedTestsMetaData"];
  _tests = [aDecoder decodeObjectForKey:@"tests"];
  _queue = [[NSOperationQueue alloc] init];
  _queue.maxConcurrentOperationCount = 1;
  if (!_tests)
    _tests = [[NSMutableDictionary alloc] init];
  
  _tests =  [self deserializeTestJson];
    // we check if bundles path was mistakenly deleted by the user
    /*if (![Shared isFileExists:[self testsPath:NO]])
    {
        _unzipedTestsMetaData = NO;
    }*/

    // Uncomment to allow unzipping of bundles on every version change.
    // This is still not perfect (maybe you also need to do _bundles = nil)
    /*
     CGFloat lastVersion = [aDecoder decodeBoolForKey:@"lastUnzippedVersion"];
     if (lastVersion != [VERSION floatValue])
     {
     _unzipedTestsMetaData = NO;
     }
     */

    /*if (![self extractLocalPreloadedTestsIfNeeded:^{
        // extract was successfull
    }])*/
    {
        // extract was not successfull
    }
}

/*- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_tests forKey:@"tests"];
    [aCoder encodeFloat:[VERSION floatValue] forKey:@"lastUnzippedVersion"];
    [aCoder encodeBool:_unzipedTestsMetaData forKey:@"unzipedTestsMetaData"];
}

- (void)sendAllTestOpenedCacheIfNeeded
{
    if (![[FBNetworkReachability sharedInstance] reachable])
    {
        return;
    }
    if (![userProfileLogic() isLoggedIn])
    {
        return;
    }

    [LogicManager saveToFile];
}

- (NSString *)testsPath:(BOOL)forceCreate
{
    NSString *path = [Shared getDocumentsPath:@"tests" forceCreate:forceCreate];
    return path;
}

- (NSString *)testsZipFilePath
{
    return [[NSBundle mainBundle] pathForResource:@"testyourself" ofType:@"zip"];
}

- (NSArray *)allTests
{
    return [_tests allValues];
}
*/
- (NSArray *)filterQuestionsFromEachCategory:(int)maxQuestions
{
    NSMutableArray *categories = [[NSMutableArray alloc] initWithArray:[_tests allValues] copyItems:YES];

    // filter questions from all categories
    for (TestCategory *category in categories)
    {
        int questionsToRemove = category.questions.count - maxQuestions;
        if (questionsToRemove < 0) continue;

        while (questionsToRemove > 0)
        {
            [category.questions removeObjectAtIndex:arc4random()%category.questions.count];
            questionsToRemove--;
        }
    }

    return categories;
}
/*
- (TestCategory *)testCategoryOfType:(CategoryType)categoryType
{
    return [_tests objectForKey:@(categoryType)];
}

// Tip: better to call this in thread
- (BOOL)extractTestsZip:(NSString *)zipPath deleteZipIfSuccess:(BOOL)delete
{
    if (![zipPath length])
    {
        DLog(@"Requested to unzip but zipPath is nil: %@", zipPath);
        return NO;
    }

    NSString *testsPath = [self testsPath:YES];
    // TODO: unzip the bundle file (if not already unzipped)
    ZipArchive *archive = [[ZipArchive alloc] init];
    BOOL success = [archive unzipOpenFile:zipPath];
    if (success)
    {
        success = [archive unzipFileTo:testsPath overWrite:YES];
        if (!success)
        {
            DLog(@"Failed to unzip file %@" , zipPath);
            [archive closeZipFile2];
            return NO;
        }

        [archive closeZipFile2];

        if (delete)
        {
            NSError *error = nil;
            [[NSFileManager defaultManager] removeItemAtPath:zipPath error:&error];
        }
        [LogicManager saveToFile];
        return YES;
    }
    else
    {
        DLog(@"Failed to open zip file %@", zipPath);
        return NO;
    }
}
*/
- (NSMutableDictionary *)deserializeTestJson
{
  NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
  NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"testYourSelf" ofType:@"bundle"];
		if ([[NSFileManager defaultManager] fileExistsAtPath:bundlePath]) {
      self.bundle = [[NSBundle alloc] initWithPath:bundlePath];
      if (self.bundle) {
        [self.bundle load];
        NSString *jsonPath = [self.bundle pathForResource:@"testyourself" ofType:@"json"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:jsonPath])
          return nil;
 
        NSData *data = [NSData dataWithContentsOfFile:jsonPath];
        NSError *error = nil;
        NSDictionary *jsonRoot = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        NSArray *types = [jsonRoot objectForKey:@"types"];
        for (NSDictionary *typeDict in types)
        {
          TestCategory *testCategory = [[TestCategory alloc] init];
          [testCategory fillWithDictionaryElement:typeDict];
          
          [dict setObject:testCategory forKey:@(testCategory.categoryType)];
        }
      }
    }
  return dict;
}
/*
- (BOOL)extractLocalPreloadedTestsIfNeeded:(SimpleCompletion)completed
{
    if (_unzipedTestsMetaData)
    {
        if (completed)
            completed();
        return NO;
    }

    __block SimpleCompletion weakCompleted = completed;

    [_queue addOperationWithBlock:^{
        // 1. get all zip files in preload folder
        // 2. extract files to bundle path
        // 3. notify once done
        NSMutableDictionary *testCategories = nil;

        @try
        {
            _isExtractingPreloadedTests = YES;

            NSString *zipFile = [self testsZipFilePath];
            BOOL success = [self extractTestsZip:zipFile deleteZipIfSuccess:NO];
            if (success)
            {
                testCategories = [self deserializeTestJson];
            }
        }
        @finally
        {
            DispatchOnMain(
                           if (testCategories)
                           {
                               _unzipedTestsMetaData = YES;
                               _tests = testCategories;
                           }
                           [LogicManager saveToFile];

                           _isExtractingPreloadedTests = NO;
                           [Shared postNotification:APP_FINISHED_EXTRACTING_PRELOADED_BUNDLES];

                           if (weakCompleted)
                           weakCompleted();
                           );
        }
    }];
    return YES;

}
*/
/* Notifications */

/*- (void)reachabilityChanged:(NSNotification *)note
{
    if ([[FBNetworkReachability sharedInstance] reachable])
    {
        [self sendAllTestOpenedCacheIfNeeded];
    }
}

- (void)loggedin:(NSNotification *)note
{

}

- (void)loggedout:(NSNotification *)note
{
}
*/
/* End */
@end
