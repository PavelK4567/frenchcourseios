//
//  Category.h
//  K1000
//
//  Created by Action Item on 5/8/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "Element.h"
#import "Question.h"

enum {
    categoryTypeOpposites = 1,
    categoryTypeCompleteTheSentence = 2,
    categoryTypeTranslations = 4,
    categoryTypePics = 3
};
typedef NSInteger CategoryType;

enum {
    testCategoryOnline,
    testCategoryDownloading,
    testCategoryZipAvailableOffline,
    testCategoryAvailableOffline
};
typedef NSInteger TestCategoryStatus;

@interface TestCategory : Element <NSCopying>

@property(nonatomic, strong) NSString *questionGenericTitle;
@property(nonatomic, strong) NSMutableArray *questions;
@property(nonatomic, unsafe_unretained) CategoryType categoryType;
@property(nonatomic, unsafe_unretained) TestCategoryStatus status;

@end
