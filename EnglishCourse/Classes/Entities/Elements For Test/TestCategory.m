//
//  Category.m
//  K1000
//
//  Created by Action Item on 5/8/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "TestCategory.h"

@implementation TestCategory

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder])
	{
        self.questionGenericTitle = [aDecoder decodeObjectForKey:@"questionGenericTitle"];
        self.questions = [aDecoder decodeObjectForKey:@"questions"];
        self.categoryType = [aDecoder decodeIntForKey:@"categoryType"];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];

    [aCoder encodeObject:_questionGenericTitle forKey:@"questionGenericTitle"];
    [aCoder encodeObject:_questions forKey:@"questions"];
    [aCoder encodeInt:_categoryType forKey:@"categoryType"];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@: %@: title: [%@] questions count: [%d] category type: [%d]", [super description], NSStringFromClass([self class]), self.questionGenericTitle, self.questions.count, self.categoryType];
}

- (void)fillWithDictionaryElement:(id)element
{
    [super fillWithDictionaryElement:element];

    self.questionGenericTitle = [self getObjectFromKey:element key:@"question_generic_title"];
    self.categoryType = [[self getObjectFromKey:element key:@"type"] intValue];

    NSArray *questions = [self getObjectFromKey:element key:@"questions" defaultValue:nil];
    if (questions.count)
    {
        _questions = [[NSMutableArray alloc] init];
        for (NSDictionary *dict in questions)
        {
            Question *question = [[Question alloc] init];
            [question fillWithDictionaryElement:dict];
            [_questions addObject:question];
        }
    }
}

- (id)copyWithZone:(NSZone *)zone
{
    TestCategory *category = [[[self class] allocWithZone:zone] init];
    category.questions = [[NSMutableArray alloc] initWithArray:self.questions copyItems:YES];
    category.categoryType = self.categoryType;
    category.questionGenericTitle = [self.questionGenericTitle copy];

    return category;
}

@end

