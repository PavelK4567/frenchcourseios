//
//  WordElement.m
//  K1000
//
//  Created by Action Item on 4/24/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "WordElement.h"

@implementation WordElement

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder])
	{
        self.order = [aDecoder decodeIntForKey:@"order"];
        self.englishWord = [aDecoder decodeObjectForKey:@"englishWord"];
        self.portugeseWord = [aDecoder decodeObjectForKey:@"portugeseWord"];
        self.exampleText = [aDecoder decodeObjectForKey:@"exampleText"];
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];

    [aCoder encodeInt:_order forKey:@"order"];
    [aCoder encodeObject:_englishWord forKey:@"englishWord"];
    [aCoder encodeObject:_portugeseWord forKey:@"portugeseWord"];
    [aCoder encodeObject:_exampleText forKey:@"exampleText"];
}

- (NSComparisonResult)compare:(WordElement *)word
{
    if (self.order < word.order)
        return NSOrderedAscending;
    if (self.order > word.order)
        return NSOrderedDescending;

    return NSOrderedSame;
}

- (void)fillWithDictionaryElement:(id)element
{
    [super fillWithDictionaryElement:element];

    self.order = [[self getObjectFromKey:element key:@"order"] intValue];
    self.englishWord = [self getObjectFromKey:element key:@"translation"];
    self.portugeseWord = [self getObjectFromKey:element key:@"word"];
    self.exampleText = [self getObjectFromKey:element key:@"text"];
}

@end
