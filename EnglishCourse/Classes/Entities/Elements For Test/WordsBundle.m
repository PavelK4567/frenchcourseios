//
//  WordsBundle.m
//  K1000
//
//  Created by Action Item on 4/22/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "WordsBundle.h"

@implementation WordsBundle

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder])
	{
    self.name = [aDecoder decodeObjectForKey:@"name"];
    self.bundleIndex = [aDecoder decodeIntForKey:@"bundleIndex"];
    self.status = [aDecoder decodeIntForKey:@"status"];
    self.wordElements = [aDecoder decodeObjectForKey:@"wordElements"];
    self.imageURL = [aDecoder decodeObjectForKey:@"imageURL"];
    self.videoURL = [aDecoder decodeObjectForKey:@"videoURL"];
    self.bundlePath = [aDecoder decodeObjectForKey:@"bundlePath"];
  }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
  [super encodeWithCoder:aCoder];

  [aCoder encodeObject:_name forKey:@"name"];
  [aCoder encodeInt:_bundleIndex forKey:@"bundleIndex"];
  [aCoder encodeInt:_status forKey:@"status"];
  [aCoder encodeObject:_wordElements forKey:@"wordElements"];
  [aCoder encodeObject:_imageURL forKey:@"imageURL"];
  [aCoder encodeObject:_videoURL forKey:@"videoURL"];
  [aCoder encodeObject:_bundlePath forKey:@"bundlePath"];
}

- (NSComparisonResult)compare:(WordsBundle *)bundle
{
  if (self.bundleIndex < bundle.bundleIndex)
    return NSOrderedAscending;
  if (self.bundleIndex > bundle.bundleIndex)
    return NSOrderedDescending;

  return NSOrderedSame;
}

- (BOOL)isEqual:(id)object
{
  if (![object isKindOfClass:[WordsBundle class]]) return NO;

  if (object == self) return YES;
  if ([(WordsBundle *)object bundleIndex] == self.bundleIndex)
    return YES;

  return NO;
}

- (NSURL *)imageURL
{
  static NSBundle *imagesBundle = 0;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    imagesBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"images" ofType:@"bundle"]];
    [imagesBundle load];
  });
  DLog(@"%@", [[imagesBundle URLForResource:[NSString stringWithFormat:@"%d", self.bundleIndex] withExtension:@"jpg"] absoluteString]);
  return [imagesBundle URLForResource:[NSString stringWithFormat:@"%d", self.bundleIndex] withExtension:@"jpg"];
}

- (NSURL *)voiceForWordAtIndex:(int)wordOrder
{
  static NSBundle *audioBundle = 0;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    audioBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"audio" ofType:@"bundle"]];
    [audioBundle load];
  });
  DLog(@"%@", [[audioBundle URLForResource:[NSString stringWithFormat:@"%d", wordOrder] withExtension:@"mp3"] absoluteString]);
  return [audioBundle URLForResource:[NSString stringWithFormat:@"%d", wordOrder] withExtension:@"mp3"];
}

- (void)fillWithDictionaryElement:(id)element
{
  [super fillWithDictionaryElement:element];

  if ([self getObjectFromKey:element key:@"order"])
    self.bundleIndex = [[self getObjectFromKey:element key:@"order"] intValue];
  if ([self getObjectFromKey:element key:@"name"])
    self.name = [self getObjectFromKey:element key:@"name"];
  if ([self getObjectFromKey:element key:@"video_url"])
    self.videoURL = [self urlVal:([self getObjectFromKey:element key:@"video_url"])];
  NSArray *words = [self getObjectFromKey:element key:@"words" defaultValue:nil];
  if (words.count)
  {
    _wordElements = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in words)
    {
      WordElement *element = [[WordElement alloc] init];
      [element fillWithDictionaryElement:dict];
      [_wordElements addObject:element];
    }
    [_wordElements sortUsingSelector:@selector(compare:)];
  }
}
-(NSURL *)urlVal:(NSString *)str
{
  if (![str length])
    return nil;
  if (![str hasPrefix:@"http:"] && ![str hasPrefix:@"https:"])
    str = [@"http://" stringByAppendingString:str];
  return [NSURL URLWithString:str];
}
- (BOOL)allowOpenning
{
  return YES;
  //return self.bundleIndex <= userProfileLogic().user.currentBundleIndex;
}

- (BOOL)isOneAboveCurrentUserAllowedBundleIndex
{
  return YES;
  //return userProfileLogic().user.currentBundleIndex == self.bundleIndex-1;
}

- (BOOL)isLocked
{
  return ![self allowOpenning] && ![self isOneAboveCurrentUserAllowedBundleIndex];
}

- (BOOL)isDownloading
{
  return _status == wordsBundleDownloading;
}

- (BOOL)isAvailableOffline
{
  return _status == wordsBundleAvailableOffline;
}

@end
