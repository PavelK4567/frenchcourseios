//
//  Level.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "InstanceSignature.h"

@interface Level : BaseEntity

@property (nonatomic, strong) NSMutableArray  *instanceSignaturesArray;
@property (nonatomic, copy) NSString *levelDataMD5;

+ (Level *)buildLevel:(NSDictionary *)dataDict;

@end
