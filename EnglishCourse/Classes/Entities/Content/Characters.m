//
//  Characters.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/19/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Characters.h"

@implementation Characters

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.charactersArray = [aDecoder decodeObjectForKey:@"characters"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.charactersArray forKey:@"characters"];
    
}

- (void)replaceCharactersIfDifferent:(NSDictionary *)dataDict
{
    
    NSArray *dictCharactersArray = dataDict[kCharacters];
    
    for (int index = 0; index < [self.charactersArray count]; index++) {
        
        Character *character = self.charactersArray[index];
        NSDictionary *characterDict = dictCharactersArray[index];
        
        if (character.mID == [characterDict[kId] integerValue]) {
            
            [character replaceCharacterIfDifferent:characterDict];
            
        }
        
    }
    
}

+ (Characters *)buildCharacters:(NSDictionary *)dataDict {
 
    NSArray *charactersJSONArray = dataDict[kCharacters];
    
    Characters *charactersInfo = [Characters new];
    NSMutableArray *charactersArray = [NSMutableArray arrayWithCapacity:1];
    
    for (NSDictionary *characterDict in charactersJSONArray) {
        
        Character *characterInfo = [Character buildCharacter:characterDict];
        
        [charactersArray addObject:characterInfo];
        
    }
    
    charactersInfo.charactersArray = [charactersArray copy];
    
    return charactersInfo;
    
}

@end
