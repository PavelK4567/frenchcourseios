//
//  Section.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "Unit.h"

@interface Section : BaseEntity

@property (nonatomic, assign) NSInteger sectionID;
@property (nonatomic, copy) NSString  *sectionName;
@property (nonatomic, copy) NSString  *sectionDescription;
@property (nonatomic, assign) NSInteger sectionOrder;
@property (nonatomic, strong) NSArray   *unitsArray;
@property (nonatomic, copy) NSString  *fbLink;

+ (Section *)buildSection:(NSDictionary *)dataDict;

@end
