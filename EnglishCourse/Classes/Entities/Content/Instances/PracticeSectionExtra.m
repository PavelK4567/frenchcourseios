//
//  PracticeSectionExtra.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 1/14/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "PracticeSectionExtra.h"

@implementation PracticeSectionExtra

+ (PracticeSectionExtra *)createInstanceWithData:(PractiseSection *)instanceDataDict withIndex:(NSInteger)index andLength:(NSInteger)length {
    
    PracticeSectionExtra *instance = nil;
    
    if (instanceDataDict) {
        
        instance = [PracticeSectionExtra new];
        
        instance.orderNumber = instanceDataDict.orderNumber;
        instance.question = instanceDataDict.question;
        instance.image = instanceDataDict.image;
        instance.isLongLayout = instanceDataDict.isLongLayout;
        
        instance.audio = instanceDataDict.audio;
        
        instance.index = index;
        instance.length = length;
        
        NSArray *answersArray = instanceDataDict.answersArray;
        
        instance.answersArray = answersArray;
        
    }
    
    return instance;

}

@end
