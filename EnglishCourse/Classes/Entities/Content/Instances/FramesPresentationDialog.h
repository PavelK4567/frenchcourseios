//
//  FramesPresentationDialog.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"
#import "SegmentFramesPresentation.h"

@interface FramesPresentationDialog : BaseInstance

@property (nonatomic, copy) NSString *sectionTitle;
@property (nonatomic, copy) NSString *sectionText;
@property (nonatomic, strong) NSArray  *segmentsArray;
@property (nonatomic, assign) BOOL longVersion;

+ (FramesPresentationDialog *)createInstanceWithData:(NSDictionary *)instanceDataDict;

@end
