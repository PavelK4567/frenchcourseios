//
//  FillTheMissingWords.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"
#import "Feedback.h"
#import "SegmentMissingWords.h"

@interface FillTheMissingWords : BaseInstance

@property (nonatomic, strong) Feedback  *correctFeedback;
@property (nonatomic, assign) NSInteger leftCharacterID;
@property (nonatomic, assign) NSInteger rightCharacterID;
@property (nonatomic, strong) NSArray   *segmentsArray;

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;

@end
