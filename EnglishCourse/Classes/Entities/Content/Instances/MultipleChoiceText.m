//
//  MultipleChoiceText.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MultipleChoiceText.h"

@implementation MultipleChoiceText

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    self.image = [aDecoder decodeObjectForKey:@"image"];
    self.isLongLayout = [aDecoder decodeBoolForKey:@"isLongLayout"];
    self.question = [aDecoder decodeObjectForKey:@"question"];
    self.correctFeedback = [aDecoder decodeObjectForKey:@"correctFeedback"];
    self.incorrectFeedback = [aDecoder decodeObjectForKey:@"incorrectFeedback"];
    self.answersArray = [aDecoder decodeObjectForKey:@"answers"];
    self.isInitial = [aDecoder decodeBoolForKey:@"initial"];
    self.title = [aDecoder decodeObjectForKey:@"title"];
    
    self.mID = [aDecoder decodeIntegerForKey:@"mID"];
    self.type = [aDecoder decodeIntegerForKey:@"type"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeBool:self.isLongLayout forKey:@"isLongLayout"];
    [aCoder encodeObject:self.question forKey:@"question"];
    [aCoder encodeObject:self.correctFeedback forKey:@"correctFeedback"];
    [aCoder encodeObject:self.incorrectFeedback forKey:@"incorrectFeedback"];
    [aCoder encodeObject:self.answersArray forKey:@"answers"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    [aCoder encodeObject:self.title forKey:@"title"];
    
    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
}

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    MultipleChoiceText *instance = nil;
    
    if (instanceDataDict) {
        instance = [MultipleChoiceText new];
        
        instance.mID = [instanceDataDict[kInstanceID] integerValue];
        instance.unitID = [instanceDataDict[kUnitID] integerValue];
        instance.order = [instanceDataDict[kOrder] integerValue];
        instance.type = [instanceDataDict[kType] integerValue];
        instance.name = instanceDataDict[kName];
        instance.question = instanceDataDict[kQuestion];
        instance.instructionText = instanceDataDict[kInstructionText];
        instance.instructionSound = instanceDataDict[kInstructionSound];
        instance.isLongLayout = [instanceDataDict[kIsLongLayout] boolValue];
        instance.image = instanceDataDict[kImage];
        
        NSArray *answersArray = instanceDataDict[kAnswers];
        
        NSMutableArray *answersDataObjectsArray = nil;
        
        if (answersArray && [answersArray count] > 0) {
            answersDataObjectsArray = [NSMutableArray arrayWithCapacity:1];
            
            for (NSDictionary *answerDict in answersArray) {
                Answer *answer = [Answer createInstanceWithData:answerDict];
                
                [answersDataObjectsArray addObject:answer];
            }
            
        }
        
        instance.answersArray = [answersDataObjectsArray copy];
        
        instance.correctFeedback = [Feedback createInstanceWithData:instanceDataDict[kCorrectFeedback]];
        instance.incorrectFeedback = [Feedback createInstanceWithData:instanceDataDict[kIncorrectFeedback]];
        
        instance.isInitial = [instanceDataDict[kInitial] boolValue];
        
        instance.title = instanceDataDict[kTitle];
        
        instance.tempDataObj = nil;
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![((MultipleChoiceText *)self.tempInstance).image isEqualToString:self.image]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    [self.correctFeedback removeDifferentResources:((MultipleChoiceText *)self.tempInstance).correctFeedback.audio];
    [self.incorrectFeedback removeDifferentResources:((MultipleChoiceText *)self.tempInstance).incorrectFeedback.audio];
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kInstructionSound] isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    if (![instanceDict[kImage] isEqualToString:self.image]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    [self.correctFeedback removeUnusedResources:instanceDict[kCorrectFeedback]];
    [self.incorrectFeedback removeUnusedResources:instanceDict[kIncorrectFeedback]];
}

- (void)replaceWithInstance:(MultipleChoiceText *)instance {
    [super replaceWithInstance:instance];
    
    self.image = instance.image;
    self.isLongLayout = instance.isLongLayout;
    self.question = instance.question;
    self.correctFeedback = instance.correctFeedback;
    self.incorrectFeedback = instance.incorrectFeedback;
    self.answersArray = [instance.answersArray copy];
    self.isInitial = instance.isInitial;
    self.title = instance.title;
}

@end
