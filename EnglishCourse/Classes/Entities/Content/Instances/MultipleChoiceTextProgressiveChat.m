//
//  MultipleChoiceTextProgressiveChat.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MultipleChoiceTextProgressiveChat.h"

@implementation MultipleChoiceTextProgressiveChat

- (id)initWithCoder:(NSCoder *)aDecode {
    
    self = [super initWithCoder:aDecode];
    
    self.segmentsArray = [aDecode decodeObjectForKey:@"segmentsArray"];
    self.isInitial = [aDecode decodeBoolForKey:@"initial"];
    self.title = [aDecode decodeObjectForKey:@"title"];
    
    self.mID = [aDecode decodeIntegerForKey:@"mID"];
    self.type = [aDecode decodeIntegerForKey:@"type"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.segmentsArray forKey:@"segmentsArray"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    [aCoder encodeObject:self.title forKey:@"title"];
    
    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
}

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    MultipleChoiceTextProgressiveChat *instance = nil;
    
    if (instanceDataDict) {
        instance = [MultipleChoiceTextProgressiveChat new];
        
        instance.mID = [instanceDataDict[kInstanceID] integerValue];
        instance.unitID = [instanceDataDict[kUnitID] integerValue];
        instance.order = [instanceDataDict[kOrder] integerValue];
        instance.type = [instanceDataDict[kType] integerValue];
        instance.name = instanceDataDict[kName];
        instance.instructionText = instanceDataDict[kInstructionText];
        instance.instructionSound = instanceDataDict[kInstructionSound];
        
        NSArray *instanceSegments = instanceDataDict[kSegments];
        
        NSMutableArray *instanceSegmentsArray = nil;
        
        if (instanceSegments && [instanceSegments count] > 0) {
            instanceSegmentsArray = [NSMutableArray arrayWithCapacity:1];
            
            for (NSDictionary *segmentDict in instanceSegments) {
                SegmentProgressiveChat *segmentChat = [SegmentProgressiveChat new];
                
                segmentChat.characterID = [segmentDict[kCharacterID] integerValue];
                segmentChat.audio = segmentDict[kAudio];
                segmentChat.isAnswer = [segmentDict[kIsAnswer] boolValue];
                segmentChat.text = segmentDict[kText];
                
                NSArray *answersArray = segmentDict[kAnswers];
                
                NSMutableArray *segmentAnswersArray = nil;
                
                if (answersArray && [answersArray count] > 0) {
                    segmentAnswersArray = [NSMutableArray arrayWithCapacity:1];
                    
                    for (NSDictionary *answerDict in answersArray) {
                        Answer *answer = [Answer new];
                        
                        answer.orderNumber = [answerDict[kOrderNr] integerValue];
                        answer.text = answerDict[kText];
                        answer.isCorrect = [answerDict[kIsCorrect] boolValue];
                        
                        [segmentAnswersArray addObject:answer];
                    }
                    
                    segmentChat.answersArray = [segmentAnswersArray copy];
                }
                
                [instanceSegmentsArray addObject:segmentChat];
            }
            
            instance.segmentsArray = [instanceSegmentsArray copy];
        }
        
        instance.isInitial = [instanceDataDict[kInitial] boolValue];
        
        instance.title = instanceDataDict[kTitle];
        
        instance.tempDataObj = nil;
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![((MultipleChoiceTextProgressiveChat *)self.tempInstance).instructionSound isEqualToString:self.instructionSound]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }

    NSArray *instanceSegments = ((MultipleChoiceTextProgressiveChat *)self.tempInstance).segmentsArray;

    NSArray *segmentsArray = self.segmentsArray;

    for (int index = 0; index < [segmentsArray count]; index++) {

        SegmentProgressiveChat *segment = segmentsArray[index];
        SegmentProgressiveChat *tempSegment = instanceSegments[index];

        [segment removeDifferentInstanceResources:tempSegment];
        
    }
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![((SimplePageProfile *)self.tempInstance).instructionSound isEqualToString:self.instructionSound]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    if (![instanceDict[kInstructionSound] isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    NSArray *instanceSegments = instanceDict[kSegments];
    
    NSArray *segmentsArray = self.segmentsArray;
    
    for (int index = 0; index < [segmentsArray count]; index++) {
        
        SegmentProgressiveChat *segment = segmentsArray[index];
        NSDictionary *segmentDict = instanceSegments[index];
        
        [segment removeUnusedResources:segmentDict];
        
    }
}

- (void)replaceWithInstance:(MultipleChoiceTextProgressiveChat *)instance {
    [super replaceWithInstance:instance];
    
    self.segmentsArray =  [instance.segmentsArray copy];
    self.isInitial = instance.isInitial;
    self.title =  instance.title;
}
@end
