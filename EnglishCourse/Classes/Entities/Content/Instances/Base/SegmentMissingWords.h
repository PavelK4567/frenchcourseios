//
//  SegmentMissingWords.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseSegment.h"
#import "WordMissingWords.h"

@interface SegmentMissingWords : BaseSegment

@property (nonatomic, copy) NSString *translation;
@property (nonatomic, strong) NSArray  *wordsArray;

+ (SegmentMissingWords *)createInstanceWithData:(NSDictionary *)instanceDataDict;

@end
