//
//  BaseInstance.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"

@implementation BaseInstance

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    //self.mID = [aDecoder decodeIntegerForKey:@"mID"];
    self.unitID = [aDecoder decodeIntegerForKey:@"unitID"];
    self.order = [aDecoder decodeIntegerForKey:@"order"];
    //self.type = [aDecoder decodeIntegerForKey:@"type"];
    self.name = [aDecoder decodeObjectForKey:@"name"];
    self.instructionText = [aDecoder decodeObjectForKey:@"instructionText"];
    self.instructionSound = [aDecoder decodeObjectForKey:@"instructionSound"];
    self.pages = [aDecoder decodeIntegerForKey:@"pages"];
    self.levelOrder = [aDecoder decodeIntegerForKey:@"levelOrder"];
    self.tempInstance = [aDecoder decodeObjectForKey:@"tempInstance"];
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    //[aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.unitID forKey:@"unitID"];
    [aCoder encodeInteger:self.order forKey:@"order"];
    //[aCoder encodeInteger:self.type forKey:@"type"];
    [aCoder encodeObject:self.instructionText forKey:@"instructionText"];
    [aCoder encodeObject:self.instructionSound forKey:@"instructionSound"];
    [aCoder encodeInteger:self.pages forKey:@"pages"];
    [aCoder encodeInteger:self.levelOrder forKey:@"levelOrder"];
    [aCoder encodeObject:self.tempInstance forKey:@"tempInstance"];
    
    
}

- (void)removeInstanceObjects {
    
}

- (void)removeSpecificInstanceObject:(NSString *)instanceObject {
    
}

- (void)replaceWithInstance:(BaseInstance *)instance {
    self.mID = instance.mID;
    self.unitID = instance.unitID;
    self.order = instance.order;
    self.type = instance.type;
    self.name = instance.name;
    self.instructionText = instance.instructionText;
    self.instructionSound = instance.instructionSound;
    self.pages = instance.pages;
    self.levelOrder = instance.levelOrder;
    self.refObjectsArray = [instance.refObjectsArray copy];
    self.tempDataObj = instance.tempDataObj;
}

- (void)replaceWithTempDataObject {
    
    self.mID = ((BaseInstance *)self.tempDataObj).mID;
    self.unitID = ((BaseInstance *)self.tempDataObj).unitID;
    self.order = ((BaseInstance *)self.tempDataObj).order;
    self.type = ((BaseInstance *)self.tempDataObj).type;
    self.name = ((BaseInstance *)self.tempDataObj).name;
    self.instructionText = ((BaseInstance *)self.tempDataObj).instructionText;
    self.instructionSound = ((BaseInstance *)self.tempDataObj).instructionSound;
    self.pages = ((BaseInstance *)self.tempDataObj).pages;
    self.levelOrder = ((BaseInstance *)self.tempDataObj).levelOrder;
    
    self.tempDataObj = nil;
    
}
- (void)removeDifferentInstanceResources
{
}
- (void)replaceWithTempInstance{
    
}
@end
