//
//  WordMultipleChoiceImage.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface WordMultipleChoiceImage : BaseEntity

@property (nonatomic, assign) NSInteger orderNumber;
@property (nonatomic, copy) NSString  *targetText;
@property (nonatomic, copy) NSString  *nativeText;
@property (nonatomic, copy) NSString  *image;
@property (nonatomic, copy) NSString  *sound;

+ (WordMultipleChoiceImage *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;
- (void)removeDifferentInstanceResources:(WordMultipleChoiceImage *)tempSegment;

@end
