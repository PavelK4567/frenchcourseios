//
//  PractiseSection.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "PractiseSection.h"

@implementation PractiseSection

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.orderNumber = [aDecoder decodeIntegerForKey:@"orderNumber"];
    self.question = [aDecoder decodeObjectForKey:@"question"];
    self.image = [aDecoder decodeObjectForKey:@"image"];
    self.isLongLayout = [aDecoder decodeBoolForKey:@"isLongLayout"];
    self.answersArray = [aDecoder decodeObjectForKey:@"answersArray"];
    self.audio = [aDecoder decodeObjectForKey:@"audio"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.orderNumber forKey:@"orderNumber"];
    [aCoder encodeObject:self.question forKey:@"question"];
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeBool:self.isLongLayout forKey:@"isLongLayout"];
    [aCoder encodeObject:self.answersArray forKey:@"answersArray"];
    [aCoder encodeObject:self.audio forKey:@"audio"];
    
}

+ (PractiseSection *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    PractiseSection *instance = nil;
    
    if (instanceDataDict) {
        instance = [PractiseSection new];
        
        instance.orderNumber = [instanceDataDict[kOrderNr] integerValue];
        instance.question = instanceDataDict[kQuestion];
        instance.image = instanceDataDict[kImage];
        instance.isLongLayout = [instanceDataDict[kIsLongLayout] boolValue];
        
        instance.audio = instanceDataDict[kAudio];
        
        NSArray *answersArray = instanceDataDict[kAnswers];
        
        NSMutableArray *answersDataArray = nil;
        
        if (answersArray && [answersArray count] > 0) {
            answersDataArray = [NSMutableArray arrayWithCapacity:1];
            
            for (NSDictionary *answerDict in answersArray) {
                Answer *practiseAnswer = [Answer createInstanceWithData:answerDict];
                
                [answersDataArray addObject:practiseAnswer];
            }
            
            instance.answersArray = [answersDataArray copy];
        }
        
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources:(PractiseSection *)tempSection
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![tempSection.image isEqualToString:self.image]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    if (![tempSection.audio isEqualToString:self.audio]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kImage] isEqualToString:self.image]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    if (![instanceDict[kAudio] isEqualToString:self.audio]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
    
}

@end
