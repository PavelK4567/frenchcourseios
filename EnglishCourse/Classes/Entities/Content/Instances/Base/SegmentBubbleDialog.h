//
//  SegmentBubbleDialog.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface SegmentBubbleDialog : BaseEntity

@property (nonatomic, assign) NSInteger orderNumber;
@property (nonatomic, assign) NSInteger characterID;
@property (nonatomic, copy) NSString  *text;

@end
