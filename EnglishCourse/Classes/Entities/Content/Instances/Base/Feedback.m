//
//  Feedback.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Feedback.h"

@implementation Feedback

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.header = [aDecoder decodeObjectForKey:@"header"];
    self.body = [aDecoder decodeObjectForKey:@"body"];
    self.audio = [aDecoder decodeObjectForKey:@"audio"];
    self.footer = [aDecoder decodeObjectForKey:@"footer"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.header forKey:@"header"];
    [aCoder encodeObject:self.body forKey:@"body"];
    [aCoder encodeObject:self.audio forKey:@"audio"];
    [aCoder encodeObject:self.footer forKey:@"footer"];
    
}

+ (Feedback *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    Feedback *instance = nil;
    
    if (instanceDataDict) {
        instance = [Feedback new];

        instance.header = instanceDataDict[kHeader];
        instance.body = instanceDataDict[kBody];
        instance.audio = instanceDataDict[kAudio];
        instance.footer = instanceDataDict[kFooter];
        
    }
    
    return instance;
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kAudio] isEqualToString:self.audio]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
}

- (void)removeDifferentResources:(NSString *)resourceForRemove
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![resourceForRemove isEqualToString:self.audio]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
}

@end
