//
//  SegmentMissingWords.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SegmentMissingWords.h"

@implementation SegmentMissingWords

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];

    self.translation = [aDecoder decodeObjectForKey:@"translation"];
    self.wordsArray = [aDecoder decodeObjectForKey:@"wordsArray"];
    self.text = [aDecoder decodeObjectForKey:@"text"];
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.translation forKey:@"translation"];
    [aCoder encodeObject:self.wordsArray forKey:@"wordsArray"];
    [aCoder encodeObject:self.text forKey:@"text"];
    
}

+ (SegmentMissingWords *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    SegmentMissingWords *instance = nil;
    
    if (instanceDataDict) {
        instance = [SegmentMissingWords new];
        
        instance.text = instanceDataDict[kText];
        instance.orderNumber = [instanceDataDict[kOrderNr] integerValue];
        instance.translation = instanceDataDict[kTranslation];
        
        NSArray *wordsArray = instanceDataDict[kWords];
        
        NSMutableArray *wordsDataObjectsArray = nil;
        
        if (wordsArray && [wordsArray count] > 0) {
            wordsDataObjectsArray = [NSMutableArray arrayWithCapacity:1];
            
            for (NSDictionary *wordDict in wordsArray) {
                WordMissingWords *wordMissing = [WordMissingWords createInstanceWithData:wordDict];
                
                [wordsDataObjectsArray addObject:wordMissing];
            }
                        
            instance.wordsArray = [wordsDataObjectsArray copy];
        }
        
        
    }
    
    return instance;
}

@end
