//
//  PracticeQuestionObject.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/29/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"

@interface PracticeQuestionObject : BaseInstance

@property (nonatomic, copy) NSString *image;
@property (nonatomic, assign) BOOL     isLongLayout;
@property (nonatomic, copy) NSString *question;
@property (nonatomic, strong) NSArray  *answersArray;

@end
