//
//  PractiseSection.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "Answer.h"

@interface PractiseSection : BaseEntity

@property (nonatomic, assign) NSInteger orderNumber;
@property (nonatomic, copy) NSString  *question;
@property (nonatomic, copy) NSString  *image;
@property (nonatomic, assign) BOOL      isLongLayout;
@property (nonatomic, strong) NSArray   *answersArray;
@property (nonatomic, strong) NSString *audio;

+ (PractiseSection *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;
- (void)removeDifferentInstanceResources:(PractiseSection *)tempSection;

@end
