//
//  SegmentProgressiveChat.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SegmentProgressiveChat.h"

@implementation SegmentProgressiveChat

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.characterID = [aDecoder decodeIntegerForKey:@"characterID"];
    self.isAnswer = [aDecoder decodeBoolForKey:@"isAnswer"];
    self.audio = [aDecoder decodeObjectForKey:@"audio"];
    self.answersArray = [aDecoder decodeObjectForKey:@"answers"];
    self.text = [aDecoder decodeObjectForKey:@"text"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.characterID forKey:@"characterID"];
    [aCoder encodeBool:self.isAnswer forKey:@"isAnswer"];
    [aCoder encodeObject:self.audio forKey:@"audio"];
    [aCoder encodeObject:self.answersArray forKey:@"answers"];
    [aCoder encodeObject:self.text forKey:@"text"];
    
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kAudio] isEqualToString:self.audio]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
}

- (void)removeDifferentInstanceResources:(SegmentProgressiveChat *)tempSegment
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![tempSegment.audio isEqualToString:self.audio]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
}

@end
