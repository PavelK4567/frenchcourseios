//
//  SegmentProgressiveChat.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseSegment.h"

@interface SegmentProgressiveChat : BaseSegment

@property (nonatomic, assign) NSInteger characterID;
@property (nonatomic, assign) BOOL      isAnswer;
@property (nonatomic, copy) NSString  *audio;
@property (nonatomic, strong) NSArray   *answersArray;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;
- (void)removeDifferentInstanceResources:(SegmentProgressiveChat *)tempSegment;

@end
