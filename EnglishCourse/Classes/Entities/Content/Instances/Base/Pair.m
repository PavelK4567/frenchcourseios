//
//  Pair.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Pair.h"

@implementation Pair

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.nativeText = [aDecoder decodeObjectForKey:@"nativeText"];
    self.targetText = [aDecoder decodeObjectForKey:@"targetText"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.nativeText forKey:@"nativeText"];
    [aCoder encodeObject:self.targetText forKey:@"targetText"];
    
}

@end
