//
//  MultipleChoiceImagePractice.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Feedback.h"

@interface MultipleChoiceImagePracticeObject : BaseInstance

@property (nonatomic, strong) NSArray *wordMultipleChoiceImagesArray;
@property (nonatomic, strong) NSString *audio;
@property (nonatomic) NSInteger answer;
@property (nonatomic, strong) Feedback *correctFeedback;
@property (nonatomic, strong) Feedback *incorrectFeedback;
@property (nonatomic, strong) NSString *question;

@end
