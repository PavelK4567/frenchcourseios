//
//  CourseStructure.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "CourseStructure.h"

@implementation CourseStructure

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.course =  [aDecoder decodeObjectForKey:@"course"];
    self.characters = [aDecoder decodeObjectForKey:@"characters"];
    self.properties = [aDecoder decodeObjectForKey:@"properties"];
    self.levelsArray = [aDecoder decodeObjectForKey:@"levels"];
    self.unitPerformaceThreshold = [aDecoder decodeObjectForKey:@"unitPerformaceThreshold"];
    self.scoreFirstTime = [aDecoder decodeObjectForKey:@"scoreFirstTime"];
    self.usageTime = [aDecoder decodeObjectForKey:@"usageTime"];
    self.unitsCreditsApple = [aDecoder decodeObjectForKey:@"unitsCreditsApple"];
    self.unitsCreditsGuest = [aDecoder decodeObjectForKey:@"unitsCreditsGuest"];
    self.scoreDiploma = [aDecoder decodeObjectForKey:@"scoreDiploma"];
    self.unitsCreditsVivo = [aDecoder decodeObjectForKey:@"unitsCreditsVivo"];
    self.scoreWrongAnswer = [aDecoder decodeObjectForKey:@"scoreWrongAnswer"];
    self.scoreSecondTime = [aDecoder decodeObjectForKey:@"scoreSecondTime"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.course forKey:@"course"];
    [aCoder encodeObject:self.characters forKey:@"characters"];
    [aCoder encodeObject:self.properties forKey:@"properties"];
    [aCoder encodeObject:self.levelsArray forKey:@"levels"];
    [aCoder encodeObject:self.unitPerformaceThreshold forKey:@"unitPerformaceThreshold"];
    [aCoder encodeObject:self.scoreFirstTime forKey:@"scoreFirstTime"];
    [aCoder encodeObject:self.usageTime forKey:@"usageTime"];
    [aCoder encodeObject:self.unitsCreditsApple forKey:@"unitsCreditsApple"];
    [aCoder encodeObject:self.unitsCreditsGuest forKey:@"unitsCreditsGuest"];
    [aCoder encodeObject:self.scoreDiploma forKey:@"scoreDiploma"];
    [aCoder encodeObject:self.unitsCreditsVivo forKey:@"unitsCreditsVivo"];
    [aCoder encodeObject:self.scoreWrongAnswer forKey:@"scoreWrongAnswer"];
    [aCoder encodeObject:self.scoreSecondTime forKey:@"scoreSecondTime"];
    
}

- (void)replaceCoursePropertiesWith:(CourseProperties *)courseProperties {
    
    [self.properties removeResources];
    self.properties = nil;
    
    self.properties = courseProperties;
    [self.properties updateResources];
    
}

- (void)replaceCourseInfoIfDifferent:(NSDictionary *)courseInfoDict {
    
    if (![courseInfoDict[kCourse] isEqualToString:self.course]) {
        
        self.course = courseInfoDict[kCourse];
        self.unitPerformaceThreshold = courseInfoDict[kUnitPerformaceThreshold];
        self.scoreFirstTime = courseInfoDict[kScoreFirstTime];
        self.usageTime = courseInfoDict[kUsageTime];
        self.unitsCreditsGuest = courseInfoDict[kUnitsCreditsGuest];
        self.scoreDiploma = courseInfoDict[kScoreDiploma];
        self.scoreWrongAnswer = courseInfoDict[kScoreWrongAnswer];
        self.scoreSecondTime = courseInfoDict[kScoreSecondTime];
        
        if (![courseInfoDict[kCharacters] isEqualToString:self.characters]) {
            
            self.characters = courseInfoDict[kCharacters];
            
        }
        
    }
    
}

+ (CourseStructure *)buildCourseStructure:(NSDictionary *)dataDict {
    
    CourseStructure *courseStructure = [CourseStructure new];
    
    courseStructure.course = dataDict[kCourse];
    courseStructure.characters = dataDict[kCharacters];
    courseStructure.levelsArray = dataDict[kLevels];
    courseStructure.unitPerformaceThreshold = dataDict[kUnitPerformaceThreshold];
    courseStructure.scoreFirstTime = dataDict[kScoreFirstTime];
    courseStructure.usageTime = dataDict[kUsageTime];
    courseStructure.unitsCreditsApple = dataDict[kUnitsCreditsApple];
    courseStructure.unitsCreditsGuest = dataDict[kUnitsCreditsGuest];
    courseStructure.scoreDiploma = dataDict[kScoreDiploma];
    courseStructure.unitsCreditsVivo = dataDict[kUnitsCreditsVivo];
    courseStructure.scoreWrongAnswer = dataDict[kScoreWrongAnswer];
    courseStructure.scoreSecondTime = dataDict[kScoreSecondTime];
    
    return courseStructure;
    
}

@end
