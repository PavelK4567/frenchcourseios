//
//  CourseStructure.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "CourseProperties.h"

@interface CourseStructure : BaseEntity

@property (nonatomic, copy) NSString *course;
@property (nonatomic, copy) NSString *characters;
@property (nonatomic, strong) CourseProperties *properties;
@property (nonatomic, strong) NSArray  *levelsArray;
@property (nonatomic, copy) NSString *unitPerformaceThreshold;
@property (nonatomic, copy) NSString *scoreFirstTime;
@property (nonatomic, copy) NSString *usageTime;
@property (nonatomic, copy) NSString *unitsCreditsApple;
@property (nonatomic, copy) NSString *unitsCreditsGuest;
@property (nonatomic, copy) NSString *scoreDiploma;
@property (nonatomic, copy) NSString *unitsCreditsVivo;
@property (nonatomic, copy) NSString *scoreWrongAnswer;
@property (nonatomic, copy) NSString *scoreSecondTime;


- (void)replaceCoursePropertiesWith:(CourseProperties *)courseProperties;

- (void)replaceCourseInfoIfDifferent:(NSDictionary *)courseInfoDict;

+ (CourseStructure *)buildCourseStructure:(NSDictionary *)dataDict;

@end
