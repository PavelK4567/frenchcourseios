//
//  InstanceInfo.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 12/3/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "InstanceInfo.h"

@implementation InstanceInfo

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    
    self.instanceID = [aDecoder decodeIntegerForKey:@"instanceID"];
    self.order = [aDecoder decodeIntegerForKey:@"order"];
    self.pages = [aDecoder decodeIntegerForKey:@"pages"];
    self.levelOrder = [aDecoder decodeIntegerForKey:@"levelOrder"];
    self.type = [aDecoder decodeIntegerForKey:@"type"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.instanceID forKey:@"instanceID"];
    [aCoder encodeInteger:self.order forKey:@"order"];
    [aCoder encodeInteger:self.pages forKey:@"pages"];
    [aCoder encodeInteger:self.levelOrder forKey:@"levelOrder"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
}

+ (InstanceInfo *)buildInstnaceInfo:(NSDictionary *)dataDict {
    
    InstanceInfo *instanceInfo = [InstanceInfo new];
    
    instanceInfo.instanceID = [dataDict[kInstanceID] integerValue];
    instanceInfo.order = [dataDict[kOrder] integerValue];
    instanceInfo.pages = [dataDict[kPages] integerValue];
    instanceInfo.levelOrder = [dataDict[kLevelOrder] integerValue];
    instanceInfo.type = [dataDict[kType] integerValue];
    instanceInfo.isInitial = [dataDict[kInitial] boolValue];
    
    return instanceInfo;
    
}

@end
