//
//  Level.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Level.h"

@implementation Level

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.instanceSignaturesArray = [aDecoder decodeObjectForKey:@"instanceSignatures"];
    self.levelDataMD5 = [aDecoder decodeObjectForKey:@"levelDataMD5"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.instanceSignaturesArray forKey:@"instanceSignatures"];
    [aCoder encodeObject:self.levelDataMD5 forKey:@"levelDataMD5"];
    
}

+ (Level *)buildLevel:(NSDictionary *)dataDict {
    
    NSArray *instancesSigArray = dataDict[kInstances];
    
    Level *level = [Level new];
    NSMutableArray *instanceSignaturesArray = [NSMutableArray arrayWithCapacity:1];
    
    level.levelDataMD5 = dataDict[kLevelData];
    
    for (NSDictionary *instanceSigDict in instancesSigArray) {
        
        InstanceSignature *instanceSig = [InstanceSignature buildInstanceSignature:instanceSigDict];
        
        [instanceSignaturesArray addObject:instanceSig];
        
    }
    
    level.instanceSignaturesArray = instanceSignaturesArray;
    
    return level;
    
}

@end
