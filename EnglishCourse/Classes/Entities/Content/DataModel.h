//
//  DataModel.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 12/4/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "CourseStructure.h"
#import "Characters.h"

@interface DataModel : BaseEntity

@property (nonatomic, strong) CourseStructure *courseData;
@property (nonatomic, strong) Characters *charactersData;

- (void)saveModel;
- (void)deleteModel;

@end
