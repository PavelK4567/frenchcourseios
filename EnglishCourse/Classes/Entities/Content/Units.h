//
//  Units.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface Units : BaseEntity

@property (nonatomic, strong) NSArray *unitsArray;

@end
