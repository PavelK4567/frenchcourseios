//
//  SyncService.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/27/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataService.h"
#import "LMDownloadManager.h"
#import "InstanceFactory.h"
#import "Utilities.h"
#import "BaseEntity.h"
#import "BaseInstance.h"

#import "CourseStructure.h"
#import "Characters.h"
#import "LevelData.h"
#import "Level.h"
#import "Section.h"
#import "Unit.h"

#import "LevelDataRequest.h"

@protocol SyncServiceDelegate <NSObject>

@optional

- (void)returnBuildStatusWithError:(BOOL)hasError errorMessage:(NSString *)errorMessage;
- (void)unitHasCompletedWithDownloading:(NSInteger)levelOrder sectionIndex:(NSInteger)sectionIndex unitIndex:(NSInteger)unitIndex;
- (void)syncTempInstnacesHasFinished;
- (void)refreshUnitsData;
- (void)returnSyncStatusWithError:(BOOL)hasError cause:(EAlertViewCause)cause;

@end

@interface SyncService : NSObject <LMDownloadManagerDelegate>

@property (nonatomic, strong) NSString *sharedDataPath;

@property (nonatomic, weak) id<SyncServiceDelegate>delegate;

@property (nonatomic, strong)NSMutableArray *lastInstanceSignaturesArray;
+ (SyncService *)sharedInstance;

- (void)buildInitialDataModel:(id<SyncServiceDelegate>)delegate;
- (void)syncContent;
- (void)syncContentForLevel:(NSInteger)levelOrder;

- (void)syncCourseData;

- (void)replaceAllTemporaryObjects;

- (BOOL)checkUnitInstancesDownloadedCorrectly:(NSArray *)sections;

- (void)continueSyncing;

@end
