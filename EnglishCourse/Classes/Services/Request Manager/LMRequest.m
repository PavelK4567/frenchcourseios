//
//  LMRequest.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 2/4/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "LMRequest.h"

@implementation LMRequest

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self             = [self init];
    self.header      = [aDecoder decodeObjectForKey:@"requestHeaders"];
    self.originalURL = [aDecoder decodeObjectForKey:@"originalURL"];
    self.requestType = [aDecoder decodeIntegerForKey:@"requestType"];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.header forKey:@"requestHeaders"];
    [aCoder encodeObject:self.originalURL forKey:@"originalURL"];
    [aCoder encodeInteger:self.requestType forKey:@"requestType"];
}

@end
