//
//  LMDownloadManager.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 1/16/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LMBaseRequest.h"

#import "LevelRequest.h"
#import "LevelDataRequest.h"
#import "InstanceRequest.h"

#import "CourseInfoRequest.h"
#import "CourseRequest.h"
#import "CharactersRequest.h"

#define DOWNLOAD_MANAGER ((LMDownloadManager *)[LMDownloadManager sharedInstance])

@protocol LMDownloadManagerDelegate <NSObject>

@optional

- (void)levelRequestDidFinishWithExecution:(NSInteger)levelOrder withError:(BOOL)withError;
- (void)levelDataRequestDidFinishWithExecution:(NSInteger)levelOrder withError:(BOOL)withError;
- (void)instanceRequestDidFinishWithExecution:(NSInteger)levelOrder
                                 sectionIndex:(NSInteger)sectionIndex
                                    unitIndex:(NSInteger)unitIndex
                                instanceOrder:(NSInteger)instanceOrder
                                        pages:(NSInteger)pages
                                    nextIndex:(NSInteger)nextIndex
                                       synced:(BOOL)synced
                                    withError:(BOOL)withError;

- (void)instanceRequestDidFinishWithExecution:(NSInteger)levelOrder
                                 sectionIndex:(NSInteger)sectionIndex
                                    unitIndex:(NSInteger)unitIndex
                                    unit:(Unit *)unit
                                instanceOrder:(NSInteger)instanceOrder
                                        pages:(NSInteger)pages
                                    nextIndex:(NSInteger)nextIndex
                                       synced:(BOOL)synced
                                    withError:(BOOL)withError;

- (void)courseInfoRequestDidFinishWithExecution:(BOOL)withError;
- (void)courseRequestDidFinishWithExecution:(BOOL)withError;
- (void)charactersRequestDidFinishWithExecution:(BOOL)withError;

- (void)syncDidFinishWithExecution:(BOOL)hasError cause:(EAlertViewCause)cause;

@end

@interface LMDownloadManager : NSObject<LMBaseRequestDelegate>

@property (nonatomic, strong) NSDictionary *requestsData;
@property (nonatomic, assign) NSInteger currentLevel;
@property (nonatomic, strong) NSString *levelVersion;
@property (nonatomic, strong) NSMutableArray *requestQueue;

@property (nonatomic, assign) NSInteger iteratorIndex;
@property (nonatomic, assign) BOOL isExecuting;

@property (nonatomic, assign) BOOL isObserverRegistered;

@property (nonatomic, weak) id<LMDownloadManagerDelegate>delegate;

+ (LMDownloadManager *)sharedInstance;

- (void)executeLevelDataRequestForLevelID:(NSInteger)levelOrder;
- (void)executeLevelRequestForLevelID:(NSInteger)levelOrder;

- (void)executeCourseInfoRequest;
- (void)executeCourseRequest;
- (void)executeCharactersRequest;

- (void)setCurrentLevelForSync:(NSInteger)currentLevel levelVersion:(NSString *)levelVersion;



- (void)createRequestForInstanceFor:(NSString *)contentVersion
                         levelIndex:(NSInteger)levelIndex
                       sectionIndex:(NSInteger)sectionIndex
                          unit:(Unit *)unit
                            orderID:(NSInteger)orderID
                              pages:(NSInteger)pages
                         instanceID:(NSInteger)instanceID
                             synced:(BOOL)synced;

- (void)executeNextRequest;

- (BOOL)isManagerExecutingRequests;
- (void)startRequestsExecution;
- (void)cancelRequestsExecution;

- (BOOL)checkLevelDataForLevelIndex:(NSInteger)levelIndex;

- (void)saveRequestsData:(NSDictionary *)requestsData;
- (NSDictionary *)loadRequestsData;
- (void)loadRequests;

- (BOOL)checkCompletedUnit:(NSArray *)downloadedInstances sectionIndex:(NSInteger)sectionIndex unitIndex:(NSInteger)unitIndex;

- (BOOL)checkDownloadProgress;
- (void)requestDataCleanUp;

- (BOOL)checkExistingRequestData:(NSInteger)levelOrder;

- (void)listAllRequestsWithProp;

- (BOOL)checkInstanceExecutionEnded:(NSInteger)levelID;

- (void)executedNotExecutedInstances:(NSArray *)sections;

@end
