//
//  CourseInfoRequest.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 2/7/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "CourseInfoRequest.h"

@implementation CourseInfoRequest

- (void)executeRequest
{
    
    DLog(@"Course Json Request is executed");
    [[Utilities sharedInstance] appendLogFileWithString:@"- Course Json Request is executed\n"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
    
    NSString *levelDataURL = [self generateURL];
    
    __weak typeof(self) weakSelf = self;
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:levelDataURL]];
    [request setDownloadDestinationPath:[dataPath stringByAppendingPathComponent:kInitialCourseJSON]];
    [request setCompletionBlock:^{
        //DLog(@"INFO: %@", request.error.description);
        //[[Utilities sharedInstance] appendLogFileWithString:[NSString stringWithFormat:@"- INFO: %@", request.error.description]];
        [self.delegate requestDidFinishWithExecution:weakSelf withError:NO errorMessage:nil];
    }];
    [request setFailedBlock:^{
        //DLog(@"ERROR: %@", request.error.description);
        //[[Utilities sharedInstance] appendLogFileWithString:[NSString stringWithFormat:@"- INFO: %@", request.error.description]];
        [self.delegate requestDidFinishWithExecution:weakSelf withError:YES errorMessage:@"FAILED"];
    }];
    [request startAsynchronous];
    
}

- (NSString *)generateURL
{
    return [NSString stringWithFormat:kCallCourseInfo, self.levelVersion];
}

@end
