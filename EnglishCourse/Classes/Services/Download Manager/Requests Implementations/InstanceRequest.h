//
//  InstanceRequest.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 1/16/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "LMBaseRequest.h"

#import "Unit.h"
#import "InstanceFactory.h"

@interface InstanceRequest : LMBaseRequest

@property (nonatomic, assign) NSInteger instanceID;

@property (nonatomic, assign) NSInteger levelIndex;
@property (nonatomic, assign) NSInteger sectionIndex;
@property (nonatomic, assign) NSInteger unitIndex;
@property (nonatomic, assign) NSInteger pages;

@property (nonatomic, assign) NSInteger nextIndex;
@property (nonatomic, strong) Unit *unit;

@property (nonatomic, assign) BOOL isSynced;

- (void)printDataAsString;




@end
