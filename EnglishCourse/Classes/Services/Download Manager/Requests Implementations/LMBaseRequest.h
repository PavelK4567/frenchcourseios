//
//  LMBaseRequest.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 1/16/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LMEnums.h"

@class LMBaseRequest;

@protocol LMBaseRequestDelegate <NSObject>

- (void)requestDidFinishWithExecution:(LMBaseRequest *)request withError:(BOOL)withError errorMessage:(NSString *)errorMessage;

@end

@interface LMBaseRequest : NSObject

@property (nonatomic, assign) NSInteger levelOrder;
@property (nonatomic, copy) NSString *levelVersion;
@property (nonatomic, assign) NSInteger instanceOrder;

@property (nonatomic, assign) RequestType requestType;
@property (nonatomic, assign) BOOL isRequestFinished;
@property (nonatomic, assign) BOOL isRequestRunning;

@property (nonatomic, assign) NSInteger numberOfTries;

@property (nonatomic, weak) __block id<LMBaseRequestDelegate>delegate;

- (void)executeRequest;
- (NSString *)generateURL;

@end
