//  PurchaseManager.h
//  Created by Dimitar Tasev on 10.08.14.
//  Copyright (c) 2014 hAcx. All rights reserved.


#import <StoreKit/StoreKit.h>


UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;
typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray *products);


@interface PurchaseManager : NSObject

+ (PurchaseManager *)sharedInstance;


- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;
- (void)buyProduct:(SKProduct *)product;
- (BOOL)productPurchased:(NSString *)productIdentifier;
- (void)restoreCompletedTransactions;

@end


#define PURCHASE_MANAGER ((PurchaseManager *)[PurchaseManager sharedInstance])
