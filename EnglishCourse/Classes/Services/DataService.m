//
//  DataService.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/7/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "DataService.h"

@implementation DataService

+ (DataService *)sharedInstance {
    
    static DataService *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (instance == nil) {
            instance = [DataService new];
        }
    });
    
    return instance;
    
}

- (void)saveDataModel:(DataModel *)dataModel {
  
  //dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
  //dispatch_async(queue, ^{
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cachedFilePath = [documentsDirectory stringByAppendingPathComponent:@"appdata"];
    
    DataModel *cachedDataModel = [NSKeyedUnarchiver unarchiveObjectWithFile:cachedFilePath];
    
    if (cachedDataModel) {
        
        [NSKeyedArchiver archiveRootObject:dataModel toFile:cachedFilePath];
        
    } else {
        paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
        cachedFilePath = [documentsDirectory stringByAppendingPathComponent:@"appdata"];
        
        [NSKeyedArchiver archiveRootObject:dataModel toFile:cachedFilePath];
    }
    
    [[Utilities sharedInstance] addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:cachedFilePath]];
  //});
  
}

- (DataModel *)loadAppDataModel {
    
    DataModel *dataModel = nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cachedFilePath = [documentsDirectory stringByAppendingPathComponent:@"appdata"];
    
    dataModel = [NSKeyedUnarchiver unarchiveObjectWithFile:cachedFilePath];
    
    if (!dataModel) {
        paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
        cachedFilePath = [documentsDirectory stringByAppendingPathComponent:@"appdata"];
        
        dataModel = [NSKeyedUnarchiver unarchiveObjectWithFile:cachedFilePath];
    }
    
    return dataModel;
}

- (void)loadDataModel {
    if (!self.dataModel || self.dataModel == nil) {
        //NSLog(@">>DATA MODEL IS LOADED FROM FS<<");
        self.dataModel = [self loadAppDataModel];
    }
}

- (void)deleteDataModel
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cachedFilePath = [documentsDirectory stringByAppendingPathComponent:@"appdata"];
    
    [[NSFileManager defaultManager] removeItemAtPath:cachedFilePath error:NULL];
}

- (BaseEntity *)findCourseByID:(NSInteger)courseID {
    return nil;
}

- (NSArray *)findAllLevelsForCourse {
    return self.dataModel.courseData.levelsArray;
}

- (BaseEntity *)findLevelByID:(NSInteger)levelID {
    return self.dataModel.courseData.levelsArray[levelID];
}

- (NSArray *)findAllModulesFromLevel:(NSInteger)levelID {
    return [(LevelData *)self.dataModel.courseData.levelsArray[levelID] sectionsArray];
}

- (BaseEntity *)findSpecificModuleFromLevelByID:(NSInteger)levelID moduleID:(NSInteger)moduleID {
    LevelData *levelData = self.dataModel.courseData.levelsArray[levelID];
    return levelData.sectionsArray[moduleID];
}

- (NSArray *)findAllLessonsFromSpecificModuleInLevel:(NSInteger)levelID moduleID:(NSInteger)moduleID {
    LevelData *levelData = self.dataModel.courseData.levelsArray[levelID];
    Section *sectionData = levelData.sectionsArray[moduleID];
    return [sectionData unitsArray];
}

- (BaseEntity *)findSpecificLessonFromModuleInLevel:(NSInteger)levelID moduleID:(NSInteger)moduleID lessonID:(NSInteger)lessonID {
    LevelData *levelData = self.dataModel.courseData.levelsArray[levelID];
    Section *sectionData = levelData.sectionsArray[moduleID];
    return sectionData.unitsArray[lessonID];
}

- (NSArray *)findAllInstancesFromSpecificLesson:(NSInteger)levelID moduleID:(NSInteger)moduleID lessonID:(NSInteger)lessonID {
    LevelData *levelData = self.dataModel.courseData.levelsArray[levelID];
    Section *sectionData = levelData.sectionsArray[moduleID];
    Unit *lessonData = sectionData.unitsArray[lessonID];
    return [lessonData instancesArray];
}

- (BaseEntity *)findSpecificInstnaceFromLesson:(NSInteger)levelID moduleID:(NSInteger)moduleID lessonID:(NSInteger)lessonID instnaceID:(NSInteger)instanceID {
    LevelData *levelData = self.dataModel.courseData.levelsArray[levelID];
    Section *sectionData = levelData.sectionsArray[moduleID];
    Unit *lessonData = sectionData.unitsArray[lessonID];
    return lessonData.instancesArray[instanceID];
}

- (NSArray *)findAllCharacters {
    return self.dataModel.charactersData.charactersArray;
}

- (Character *)findCharacterByID:(NSInteger)characterID {
    Character *character = nil;
    
    NSArray *charactersArray = self.dataModel.charactersData.charactersArray;
    
    for (Character *characterObject in charactersArray) {
        
        if (characterID == characterObject.mID) {
            
            character = characterObject;

            break;
            
        }
        
    }
    
    return character;
}

- (NSInteger) numOfLessonInLevel:(NSInteger)levelID {
  NSInteger num = 0;
  NSArray *arrayDataSource = [self findAllModulesFromLevel:levelID];
  for (int i = 0; i < [arrayDataSource count]; i++) {
    Section *tempSection = arrayDataSource[i];
    num += [tempSection.unitsArray count];
  }
  
  return num;
}
@end
