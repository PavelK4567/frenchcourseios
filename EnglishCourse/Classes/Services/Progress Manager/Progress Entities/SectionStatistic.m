//
//  SectionStatistic.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/25/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SectionStatistic.h"
#import "Section.h"
#import "ProgressFactory.h"
#import "Section.h"

@implementation SectionStatistic

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.sectionID = [aDecoder decodeIntegerForKey:@"id"];
    self.sectionOrder = [aDecoder decodeIntegerForKey:@"sectionOrder"];
    self.unitsArray = [aDecoder decodeObjectForKey:@"unitsArray"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.sectionID forKey:@"id"];
    [aCoder encodeInteger:self.sectionOrder forKey:@"sectionOrder"];
    [aCoder encodeObject:self.unitsArray forKey:@"unitsArray"];
}

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type
{
    Section *sectionInfo = (Section *)dataObject;
    SectionStatistic *statistic = [SectionStatistic new];
    statistic.sectionID = sectionInfo.sectionID;
    statistic.sectionOrder = sectionInfo.sectionOrder;
    statistic.unitsArray = [[self buldChildObjects:sectionInfo.unitsArray] mutableCopy];
    return statistic;
}

+ (NSArray *)buldChildObjects:(NSArray *)childItems
{
    NSMutableArray *childObjectList = [[NSMutableArray alloc] initWithCapacity:[childItems count]];
    
    for(Unit *item in childItems) {
    
        BaseStatistic *statistic = [ProgressFactory buildStatisticFromObject:item childObject:nil statisticType:UnitStatisticType];
        [childObjectList addObject:statistic];
    }
    
    return childObjectList;
}

@end
