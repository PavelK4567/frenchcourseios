//
//  LevelStatistic.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/25/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseStatistic.h"

@interface LevelStatistic : BaseStatistic

@property (nonatomic, assign) NSInteger      levelID;
@property (nonatomic, assign) NSInteger      score;
@property (nonatomic, assign) NSInteger      lastVisitedUnitId;
@property (nonatomic, assign) NSInteger      lastVisitedUnitIndex;
@property (nonatomic, assign) NSInteger      lastVisitedSectionIndex;
@property (nonatomic, assign) NSInteger      lastVisitedInstanceId;
@property (nonatomic, assign) NSInteger      maxUnitId;
@property (nonatomic, strong) NSMutableArray *sectionsArray;
@property (nonatomic        ) BOOL           levelCompleted;
@property (nonatomic        ) BOOL           diplomaAchieved;


+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type;
@end
