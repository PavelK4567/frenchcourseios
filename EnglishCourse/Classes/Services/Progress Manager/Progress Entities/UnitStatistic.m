//
//  UnitStatistic.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/25/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "UnitStatistic.h"
#import "Unit.h"
#import "InstanceStatistic.h"
#import "ProgressFactory.h"

@implementation UnitStatistic

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self                     = [self init];
    self.unitID              = [aDecoder decodeIntegerForKey:@"id"];
    self.sectionID           = [aDecoder decodeIntegerForKey:@"sectionID"];
    self.order               = [aDecoder decodeIntegerForKey:@"order"];
    self.lessonProgress      = [aDecoder decodeIntegerForKey:@"lessonProgress"];
    self.lastVisitedInstance = [aDecoder decodeIntegerForKey:@"lastVisitedInstance"];
    self.achievement         = [aDecoder decodeBoolForKey:@"achievement"];
    self.instancesArray      = [aDecoder decodeObjectForKey:@"instancesArray"];
    self.isVisited           = [aDecoder decodeBoolForKey:@"isVisited"];
    self.isAvailable         = [aDecoder decodeBoolForKey:@"isAvailable"];
    self.isOpen              = [aDecoder decodeBoolForKey:@"isOpen"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.unitID forKey:@"id"];
    [aCoder encodeInteger:self.sectionID forKey:@"sectionID"];
    [aCoder encodeInteger:self.order forKey:@"order"];
    [aCoder encodeInteger:self.lessonProgress forKey:@"lessonProgress"];
    [aCoder encodeInteger:self.lastVisitedInstance forKey:@"lastVisitedInstance"];
    [aCoder encodeObject :self.instancesArray forKey:@"instancesArray"];
    [aCoder encodeBool:self.achievement forKey:@"achievement"];
    [aCoder encodeBool:self.isVisited forKey:@"isVisited"];
    [aCoder encodeBool:self.isAvailable forKey:@"isAvailable"];
    [aCoder encodeBool:self.isOpen forKey:@"isOpen"];
}

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type
{
    
    Unit *unitInfo                    = (Unit *)dataObject;
    UnitStatistic *unitStatistic      = [UnitStatistic new];

    unitStatistic.unitID              = unitInfo.unitID;
    unitStatistic.sectionID           = unitInfo.sectionID;
    unitStatistic.order               = unitInfo.order;
    unitStatistic.lessonProgress      = 0;
    unitStatistic.lastVisitedInstance = 0;
    unitStatistic.instancesArray      = unitInfo.instancesArray ? [[self buldChildObjects:unitInfo.instancesArray] mutableCopy] : [NSMutableArray new] ;
    unitStatistic.achievement         = NO;
    unitStatistic.isVisited           = NO;
    unitStatistic.isAvailable         = NO;
    unitStatistic.isOpen              = NO;
    
    return unitStatistic;
}

+ (NSArray *)buldChildObjects:(NSArray *)childItems
{

    NSMutableArray *childItemList = [[NSMutableArray alloc] initWithCapacity:[childItems count]];
    for(InstanceInfo *item in childItems) {
        
        BaseStatistic *statistic = [ProgressFactory buildStatisticFromObject:item childObject:nil statisticType:InstanceStatisticType];
        [childItemList addObject:statistic];
    }
    
    return childItemList;
}

@end
