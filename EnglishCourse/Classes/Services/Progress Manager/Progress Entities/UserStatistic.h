//
//  UserStatistic.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 1/8/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseStatistic.h"

@interface UserStatistic : BaseStatistic

@property (nonatomic, strong) NSString       *userID;
@property (nonatomic, assign) NSInteger      currentLevelId;
@property (nonatomic, assign) NSInteger      lastVisitedLessonId;
@property (nonatomic, assign) NSInteger      weeklyUsageTime;
@property (nonatomic, assign) NSInteger      trackingWeek;
@property (nonatomic)         BOOL           isSync;
@property (nonatomic, strong) NSMutableArray *levelsArray;
@property (nonatomic, strong) NSMutableArray *userUsageTimeArray;

@end
