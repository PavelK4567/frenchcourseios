//
//  InstanceStatistic.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/23/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseStatistic.h"


@interface InstanceStatistic : BaseStatistic

@property (nonatomic, assign) NSInteger      instanceID;
@property (nonatomic, assign) NSInteger      order;
@property (nonatomic, assign) NSInteger      pages;
@property (nonatomic, assign) NSInteger      levelOrder;
@property (nonatomic, assign) NSInteger      type;
@property (nonatomic, assign) NSInteger      numberOfCorrectAnswers;
@property (nonatomic, assign) NSInteger      numberOfIncorrectAnswers;
@property (nonatomic, assign) NSInteger      numberOfSkippedQuestions;
@property (nonatomic, assign) NSInteger      numberOfNotAnsweredQuestions;
@property (nonatomic, assign) NSInteger      numberOfTotalQuestions;
@property (nonatomic, strong) NSMutableArray *pagesArray;
@property (nonatomic        ) BOOL           status;
@property (nonatomic        ) BOOL           isFirstTime;
@property (nonatomic        ) BOOL           isVisited;

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type;

- (float)getStatisticValueForInstance;

@end
