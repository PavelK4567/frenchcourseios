//
//  NetworkService.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/7/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "NetworkService.h"

@implementation NetworkService

+ (NetworkService *)sharedInstance {
    
    static NetworkService *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (instance == nil) {
            instance = [NetworkService new];
        }
    });
    
    return instance;
    
}

- (void)executeCallForCourseInfoWithContentVersion:(NSString *)contentVersion delegate:(id<NetworkServiceDelegate>)delegate {
    
    NSString *fileNameToExtract = [[NSBundle mainBundle] pathForResource:@"course" ofType:@"json"];
    NSURL *localFileURL = [[NSURL alloc] initFileURLWithPath:fileNameToExtract];
    
    NSData *jsonData = [NSData dataWithContentsOfURL:localFileURL];
    
    NSError *error;
    
    NSDictionary *jsonParsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:&error];
    
    DLog(@"Data content %@",jsonParsedData);
    
    CourseStructure *courseStructure = [CourseStructure buildCourseStructure:jsonParsedData];
    
    [delegate returnCourseInfoWithError:NO message:nil courseInfo:courseStructure];
    
}

- (void)executeCallForCourseDataDownloadWithContentVersion:(NSString *)contentVersion delegate:(id<NetworkServiceDelegate>)delegate {
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"course" ofType:@"zip"];
    
    [[Utilities sharedInstance] extractDownloadedArchive:file delegate:nil];
    
    
    NSString *courseProp = [[NSBundle mainBundle] pathForResource:@"courseProp" ofType:@"json"];
    NSURL *localFileURL = [[NSURL alloc] initFileURLWithPath:courseProp];
    
    NSData *jsonData = [NSData dataWithContentsOfURL:localFileURL];
    
    NSError *error;
    
    NSDictionary *jsonParsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:&error];
    
    DLog(@"Data content %@",jsonParsedData);
    
    CourseProperties *courseProperties = [CourseProperties buildCourseProperties:jsonParsedData];
    
    [delegate returnCourseDataWithError:NO message:nil courseData:courseProperties];
    
}

- (void)executeCallForCharactersInfoDownload:(NSString *)contentVersion delegate:(id<NetworkServiceDelegate>)delegate {
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"characters" ofType:@"zip"];
    
    [[Utilities sharedInstance] extractDownloadedArchive:file delegate:nil];
    
    
    NSString *characters = [[NSBundle mainBundle] pathForResource:@"characters" ofType:@"json"];
    NSURL *localFileURL = [[NSURL alloc] initFileURLWithPath:characters];
    
    NSData *jsonData = [NSData dataWithContentsOfURL:localFileURL];
    
    NSError *error;
    
    NSDictionary *jsonParsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:&error];
    
    DLog(@"Data content %@",jsonParsedData);
    
    Characters *charactersInfo = [Characters buildCharacters:jsonParsedData];
    
    [delegate returnCharactersInfoWithError:NO message:nil charactersInfo:charactersInfo];
    
}

- (void)executeCallForLevelInfo:(NSString *)contentVersion orderNumber:(NSInteger)orderNumber delegate:(id<NetworkServiceDelegate>)delegate {
    
    NSString *fileNameToExtract = [[NSBundle mainBundle] pathForResource:@"course" ofType:@"json"];
    NSURL *localFileURL = [[NSURL alloc] initFileURLWithPath:fileNameToExtract];
    
    NSData *jsonData = [NSData dataWithContentsOfURL:localFileURL];
    
    NSError *error;
    
    NSDictionary *jsonParsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:&error];
    
    DLog(@"Data content %@",jsonParsedData);
   
    Level *level = [Level buildLevel:jsonParsedData];
    
    [delegate returnLevelsInfoWithError:NO message:nil levelsInfo:level];
    
}

- (void)executeCallForLevelDataDownloadWithContentVersion:(NSString *)contentVersion orderNumber:(NSInteger)orderNumber delegate:(id<NetworkServiceDelegate>)delegate {
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"levelData" ofType:@"zip"];
    
    [[Utilities sharedInstance] extractDownloadedArchive:file delegate:nil];
    
    
    NSString *levelDataJSONPath = [[NSBundle mainBundle] pathForResource:@"levelData" ofType:@"json"];
    NSURL *localFileURL = [[NSURL alloc] initFileURLWithPath:levelDataJSONPath];
    
    NSData *jsonData = [NSData dataWithContentsOfURL:localFileURL];
    
    NSError *error;
    
    NSDictionary *jsonParsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:&error];
    
    DLog(@"Data content %@",jsonParsedData);
    
    LevelData *levelData = [LevelData buildLevelData:jsonParsedData];
    
    [delegate returnLevelsDataWithError:NO message:nil levelsData:levelData];
    
}

- (void)executeCallForInstanceDataDownloadWithOrderNumber:(NSInteger)orderNumber levelVersion:(NSInteger)levelVersion instanceOrderID:(NSInteger)instanceOrderID delegate:(id<NetworkServiceDelegate>)delegate {
    
    NSString *orderNumberAsString = [NSString stringWithFormat:@"%ld",orderNumber];
    
    NSString *file = [[NSBundle mainBundle] pathForResource:orderNumberAsString ofType:@"zip"];
    
    [[Utilities sharedInstance] extractDownloadedArchive:file delegate:nil];
    
    
    NSString *characters = [[NSBundle mainBundle] pathForResource:orderNumberAsString ofType:@"json"];
    NSURL *localFileURL = [[NSURL alloc] initFileURLWithPath:characters];
    
    NSData *jsonData = [NSData dataWithContentsOfURL:localFileURL];
    
    NSError *error;
    
    NSDictionary *jsonParsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:&error];
    
    DLog(@"Data content %@",jsonParsedData);
    
    BaseInstance *instanceByType = [InstanceFactory createInstanceForType:[jsonParsedData[kType] integerValue] instanceDataDict:jsonParsedData];
    
    [delegate returnInstnaceDataWithError:NO message:nil instnaceData:instanceByType];
    
}

@end
