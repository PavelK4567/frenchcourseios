	//
//  SyncService.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/27/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SyncService.h"
#import "ProgressMenager.h"

@interface SyncService(Private)

- (NSDictionary *)parseDataObjectToDictionaryAtPath:(NSString *)dataPath;

- (CourseStructure *)parseCourseStructureAtPath:(NSString *)dataPath;
- (CourseProperties *)parseCoursePropertiesAtPath:(NSString *)dataPath;
- (Characters *)parseCharactersAtPath:(NSString *)dataPath;
- (Level *)parseLevelInfoAtPath:(NSString *)dataPath;
- (LevelData *)parseLevelDataAtPath:(NSString *)dataPath;

- (NSArray *)compareInstanceHashesFrom:(NSArray *)currentHashesArray newHasheshArray:(NSArray *)newHasheshArray;
- (void)createRequestForEachInstanceFrom:(NSArray *)instnacesArray levelIndex:(NSInteger)levelIndex;

- (BOOL)checkFinishedUnit:(Unit *)finishedUnit;

@end

@implementation SyncService

+ (SyncService *)sharedInstance {
    
    static SyncService *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        if (instance == nil) {
            instance = [SyncService new];
        }
        
    });
    
    return instance;
    
}

- (void)buildInitialDataModel:(id<SyncServiceDelegate>)delegate {

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
    
    NSString *levelFolderPath = nil;
    
    DataModel *appDataModel = [DataModel new];
    
    self.sharedDataPath = dataPath;

    CourseStructure *course = [self parseCourseStructureAtPath:dataPath];
    Characters *characters = [self parseCharactersAtPath:dataPath];
    
    NSMutableArray *levelsDataArray = [NSMutableArray arrayWithCapacity:1];
    
    for (NSString *levelFolderName in LEVELS_FOLDERS_ARRAY) {
        
        levelFolderPath = [dataPath stringByAppendingPathComponent:levelFolderName];
        
        LevelData *levelData = [self parseLevelDataAtPath:levelFolderPath];
        if(levelData)
            [levelsDataArray addObject:levelData];

    }
    
    course.levelsArray = [levelsDataArray copy];
    
    appDataModel.courseData = course;
    appDataModel.charactersData = characters;
    
    [appDataModel saveModel];
    
    [DataService sharedInstance].dataModel = appDataModel;
    
    [[Utilities sharedInstance] removeAllTempFiles];

    //[[Utilities sharedInstance] removeInitialData];
    
    [[Utilities sharedInstance] removeModelDataRootFolderWithContent];

    [[Utilities sharedInstance] createModelDataRootFolder];
    
    [delegate returnBuildStatusWithError:NO errorMessage:nil];
    
}

- (void)syncContent
{
    
    
    
}

- (void)syncContentForLevel:(NSInteger)levelOrder
{
    
    if (!DOWNLOAD_MANAGER.isExecuting || levelOrder != DOWNLOAD_MANAGER.currentLevel) {
        
        //NSLog(@">>syncContentForLevel is executed");
        
        [DOWNLOAD_MANAGER loadRequests];
        
        [DOWNLOAD_MANAGER cancelRequestsExecution];
        
        DOWNLOAD_MANAGER.delegate = self;
        
        if ([DOWNLOAD_MANAGER checkExistingRequestData:levelOrder]) {
            
            [DOWNLOAD_MANAGER setCurrentLevelForSync:levelOrder levelVersion:[USER_MANAGER takeLevelData][levelOrder][kLevelVersion]];
            
            [DOWNLOAD_MANAGER startRequestsExecution];
            
            [DOWNLOAD_MANAGER executeNextRequest];
            
        } else {
            [DOWNLOAD_MANAGER setCurrentLevelForSync:levelOrder levelVersion:[USER_MANAGER takeLevelData][levelOrder][kLevelVersion]];
            
            [DOWNLOAD_MANAGER executeLevelRequestForLevelID:levelOrder];
        }
        
    }
    
}

- (void)syncCourseData
{
    DOWNLOAD_MANAGER.delegate = self;
    
    [DOWNLOAD_MANAGER executeCourseInfoRequest];
}

- (void)replaceAllTemporaryObjects
{
    
    DataModel *model = [DATA_SERVICE dataModel];

    CourseStructure *structure = [model courseData];
    
    if (structure.levelsArray) {
        
        for (LevelData *levelObjects in [structure levelsArray]) {
            
            NSArray *sectionsArray = [levelObjects sectionsArray];
            
            if (sectionsArray) {
                
                for (Section *sectionData in sectionsArray) {
                    
                    NSArray *unitsArray = [sectionData unitsArray];
                    
                    if (unitsArray) {
                        
                        for (Unit *unitData in unitsArray) {
                            
                            NSMutableArray *instancesArray = [unitData instancesArray];
                            
                            if (instancesArray) {
                                
                                for (int index = 0; index < [instancesArray count]; index++) {
                                    
                                    BaseInstance *instance = instancesArray[index];
                                    
                                    if (instance.tempInstance != nil) {
                                        
                                        //BaseInstance *tempInstance = instance.tempInstance;//[instance.tempInstance copy];
                                        [instance replaceWithInstance:instance.tempInstance];
                                        instance.tempInstance = nil;
                                       /* [instance removeDifferentInstanceResources];
                                        instance.tempInstance = nil;
                                        
                                        //instancesArray[index] = nil;
                                        instancesArray[index] = tempInstance;
                                        */
                                        [[DATA_SERVICE dataModel] saveModel];
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                    }

                }
                
            }

        }
        
        [self syncCourseData];
        
    }
    
}

- (NSDictionary *)parseDataObjectToDictionaryAtPath:(NSString *)dataPath {
    
    NSData *dataObjectForParsing = nil;
    NSDictionary *dataDictionary = nil;
    NSError *error;
    
    dataObjectForParsing = [[NSFileManager defaultManager] contentsAtPath:dataPath];
    
    if (dataObjectForParsing) {
        dataDictionary = [NSJSONSerialization JSONObjectWithData:dataObjectForParsing options:NSJSONReadingMutableLeaves error:&error];
    }
    
    return dataDictionary;
    
}

- (CourseStructure *)parseCourseStructureAtPath:(NSString *)dataPath {
    
    CourseStructure *course = nil;
    
    NSString *courseJSONPath = [dataPath stringByAppendingPathComponent:kInitialCourseJSON];
    
    NSDictionary *courseDataParsed = [self parseDataObjectToDictionaryAtPath:courseJSONPath];
    
    if (courseDataParsed) {
        course = [CourseStructure buildCourseStructure:courseDataParsed];
        
        course.properties = [self parseCoursePropertiesAtPath:dataPath];
    }
    
    return course;
    
}

- (CourseProperties *)parseCoursePropertiesAtPath:(NSString *)dataPath {
    
    CourseProperties *properties = nil;
    
    NSString *coursePropJSONPath = [dataPath stringByAppendingPathComponent:kInitialCourseZIP];
    
    NSDictionary *propertiesDataParsed = [[Utilities sharedInstance] extractArchiveAndReturnData:coursePropJSONPath];
    
    if (propertiesDataParsed) {
        properties = [CourseProperties buildCourseProperties:propertiesDataParsed];
    }
    
    return properties;
    
}

- (Characters *)parseCharactersAtPath:(NSString *)dataPath {
    
    Characters *characters = nil;
    
    NSString *coursePropJZIPPath = [dataPath stringByAppendingPathComponent:kInitialCharactersZIP];
    
    NSDictionary *charactersDataParsed = [[Utilities sharedInstance] extractArchiveAndReturnData:coursePropJZIPPath];
    
    if (charactersDataParsed) {
        
        characters = [Characters buildCharacters:charactersDataParsed];
        
    }

    return characters;
    
}

- (Level *)parseLevelInfoAtPath:(NSString *)dataPath {
    
    Level *levelInfo = nil;
    
    NSString *levelOneJSONPath = [dataPath stringByAppendingPathComponent:kInitialLevelJSON];
    
    NSDictionary *levelInfoDataParsed = [self parseDataObjectToDictionaryAtPath:levelOneJSONPath];
    
    if (levelInfoDataParsed) {
        levelInfo = [Level buildLevel:levelInfoDataParsed];
    }
    
    return levelInfo;
    
}

- (LevelData *)parseLevelDataAtPath:(NSString *)dataPath {
    
    LevelData *levelData = nil;
    
    NSString *levelDataZIPPath = [dataPath stringByAppendingPathComponent:kInitialLevelDataZIP];
    
    NSDictionary *levelDataParsed = [[Utilities sharedInstance] extractArchiveAndReturnData:levelDataZIPPath];

    if (levelDataParsed) {
        
        levelData = [LevelData buildLevelData:levelDataParsed];
        
        levelData.levelInfo = [self parseLevelInfoAtPath:dataPath];
    }

    return levelData;
    
}

- (NSArray *)compareInstanceHashesFrom:(NSArray *)currentHashesArray newHasheshArray:(NSArray *)newHasheshArray
{
    
    NSMutableArray *resultArray = [NSMutableArray new];
    
    for (InstanceSignature *newSignature in newHasheshArray) {
        
        for (InstanceSignature *existingSignature in currentHashesArray) {
            
            if ((newSignature.instanceID == existingSignature.instanceID) && ![newSignature.md5Hash isEqualToString:existingSignature.md5Hash] && !existingSignature.isInitial) {
                
                if(newSignature) {
                    [resultArray addObject:@(newSignature.instanceID)];
                }
                break;
            }
            
        }
        
    }
    
    return [resultArray copy];
    
}

- (void)createRequestForEachInstanceFrom:(NSArray *)instnacesArray levelIndex:(NSInteger)levelIndex
{
    CourseStructure *courseStructure = [DATA_SERVICE dataModel].courseData;
    
    NSArray *levelsArray = courseStructure.levelsArray;
    
    LevelData *selectedLevelData = levelsArray[levelIndex];
    
    NSArray *sectionsArray = selectedLevelData.sectionsArray;
    
    for (int sectionIdx = 0; sectionIdx < [sectionsArray count]; sectionIdx++) {
        
        Section *currentSection = sectionsArray[sectionIdx];
        
        NSArray *unitsArray = currentSection.unitsArray;
        
        for (int unitIdx = 0; unitIdx < [unitsArray count]; unitIdx++) {
            
            Unit *unit = unitsArray[unitIdx];
            NSArray *instanceInfoArray = unit.instancesInfosArray;
            NSArray *instancesArray = unit.instancesArray;
            
            
            //for (BaseInstance *baseInstance in instancesArray) {
            for (NSNumber *indexObj in instnacesArray) {
                for (NSDictionary *instanceData in instanceInfoArray) {
                    if ([indexObj integerValue] == [instanceData[kId] integerValue]) {
                    [[LMDownloadManager sharedInstance] createRequestForInstanceFor:[USER_MANAGER takeContentVersion]
                                                                         levelIndex:levelIndex
                                                                       sectionIndex:sectionIdx
                                                                               unit:unit
                                                                            orderID:[instanceData[kOrder] integerValue]
                                                                              pages:[instanceData[kPages] integerValue]
                                                                         instanceID:[instanceData[kId] integerValue]
                                                                             synced:YES];
                       // break;
                    }
                }
                /*int instanceIndex = 0;
                for (NSNumber *indexObj in instnacesArray) {
                    
                    if ([baseInstance mID] == [indexObj integerValue]) {
                        if([instanceInfoArray count] > instanceIndex){
                        int instanceOrder = [[((NSDictionary *)(instanceInfoArray[instanceIndex])) objectForKey:@"order"] intValue];
                        [[LMDownloadManager sharedInstance] createRequestForInstanceFor:[USER_MANAGER takeContentVersion]
                                                                             levelIndex:levelIndex
                                                                           sectionIndex:sectionIdx
                                                                              unit:unit
                                                                                orderID:instanceOrder
                                                                                  pages:baseInstance.pages
                                                                             instanceID:baseInstance.mID
                                                                                 synced:YES];
                        }
                        break;
                        
                    }
                    
                    instanceIndex++;
                }*/
                
                
            }
            
        }
        
    }
    
}

#pragma mark -
#pragma mark - LMDownloadManagerDelegate implementation

- (void)levelRequestDidFinishWithExecution:(NSInteger)levelOrder withError:(BOOL)withError
{
    if (!withError) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        Level *levelInfo = [self parseLevelInfoAtPath:dataPath];
        NSMutableArray *instanceSignaturesArray = levelInfo.instanceSignaturesArray;
        
        CourseStructure *courseStructure = [DATA_SERVICE dataModel].courseData;
        
        LevelData *currentLevelData = courseStructure.levelsArray[levelOrder];
        Level *levelDataLevelInfo = currentLevelData.levelInfo;//:)
        NSMutableArray *levelDataInstanceSignaturesArray = levelDataLevelInfo.instanceSignaturesArray;
        
        if (![defaults boolForKey:kDataCompleteDownload]) {
            
            NSMutableArray *newInstancesSignatures = [NSMutableArray arrayWithCapacity:1];
            
            for (InstanceSignature *signature in instanceSignaturesArray) {
                
                BOOL isInstanceFound = NO;
                
                for (InstanceSignature *existingSignature in levelDataInstanceSignaturesArray) {
                    
                    if ([signature instanceID] == [existingSignature instanceID]) {
                        isInstanceFound = YES;
                        break;
                    }
                    
                }
                
                if (!isInstanceFound) {
                    [newInstancesSignatures addObject:signature];
                }
                
            }
            
            if ([newInstancesSignatures count] > 0) {
                
                for (InstanceSignature *signature in newInstancesSignatures) {
                    
                    if(signature) {
                        [levelDataInstanceSignaturesArray addObject:signature];
                    }
                }
                
                [newInstancesSignatures removeAllObjects];
                
            }
            
            newInstancesSignatures = nil;
            
            [[DATA_SERVICE dataModel] saveModel];
            
            [[LMDownloadManager sharedInstance] executeLevelDataRequestForLevelID:levelOrder];
            
        } else {
            
            /*NSArray *instancesForSync = [self compareInstanceHashesFrom:levelDataLevelInfo.instanceSignaturesArray newHasheshArray:levelInfo.instanceSignaturesArray];
            
            if (instancesForSync && [instancesForSync count] > 0) {
                
                [self createRequestForEachInstanceFrom:instancesForSync levelIndex:levelOrder];
                
                [DOWNLOAD_MANAGER startRequestsExecution];  
                
                [DOWNLOAD_MANAGER executeNextRequest];
                
            }*/
            
        }
        self.lastInstanceSignaturesArray = [levelInfo.instanceSignaturesArray copy];
        NSArray *instancesForSync = [self compareInstanceHashesFrom:levelDataLevelInfo.instanceSignaturesArray newHasheshArray:levelInfo.instanceSignaturesArray];
        
        if (instancesForSync && [instancesForSync count] > 0) {
            
            [self createRequestForEachInstanceFrom:instancesForSync levelIndex:levelOrder];
            
            [DOWNLOAD_MANAGER startRequestsExecution];
            
            [DOWNLOAD_MANAGER executeNextRequest];
            
        }
        
    }
}

- (void)levelDataRequestDidFinishWithExecution:(NSInteger)levelOrder withError:(BOOL)withError
{
    if (!withError) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
        
        NSString *levelDataZIPPath = [dataPath stringByAppendingPathComponent:kInitialLevelDataZIP];
        
        NSDictionary *levelDataParsed = [[Utilities sharedInstance] extractArchiveAndReturnData:levelDataZIPPath];
        
        CourseStructure *courseStructure = [DATA_SERVICE dataModel].courseData;
        
        LevelData *currentLevelData = courseStructure.levelsArray[levelOrder];
        
        [currentLevelData replaceResourceIfDifferent:levelDataParsed[kImage]];
            
        NSArray *sectionsArray = currentLevelData.sectionsArray;
        
        for (int sectionIdx = 0; sectionIdx < [sectionsArray count]; sectionIdx++) {
            
            Section *currentSection = sectionsArray[sectionIdx];
            
            NSArray *unitsArray = currentSection.unitsArray;
            
            for (int unitIdx = 0; unitIdx < [unitsArray count]; unitIdx++) {
                
                Unit *unit = unitsArray[unitIdx];
                
                NSArray *sectionsFromLevelData = levelDataParsed[kSections];
                NSDictionary *sectionFromLevelData = sectionsFromLevelData[sectionIdx];
                NSArray *sectionUnitsFromLevelData = sectionFromLevelData[kUnits];
                NSDictionary *unitFromSectionFromLevelData = sectionUnitsFromLevelData[unitIdx];
                NSArray *instancesFromSectionUnit = unitFromSectionFromLevelData[kInstances];
                
                [unit replaceDifferentResources:unitFromSectionFromLevelData];
                
                unit.numberOfInstances = [instancesFromSectionUnit count];
                
                unit.instancesInfosArray = instancesFromSectionUnit;
                
                if (!unit.instancesArray) {
                
                    if (instancesFromSectionUnit) {
                        
                        for (NSDictionary *instanceData in instancesFromSectionUnit) {

                            [[LMDownloadManager sharedInstance] createRequestForInstanceFor:[USER_MANAGER takeContentVersion]
                                                                                 levelIndex:levelOrder
                                                                               sectionIndex:sectionIdx
                                                                                  unit:unit
                                                                                    orderID:[instanceData[kOrder] integerValue]
                                                                                      pages:[instanceData[kPages] integerValue]
                                                                                 instanceID:[instanceData[kId] integerValue]
                                                                                     synced:NO];
                        }
                        
                    }
                    
                } else if (unit.instancesArray && instancesFromSectionUnit && ([unit.instancesArray count] < [instancesFromSectionUnit count])) {
                    
                    NSInteger numberOfDownloadedInstances = [unit.instancesArray count];
                    
                    for (int index = (int)numberOfDownloadedInstances; index <  [instancesFromSectionUnit count] ; index++) {
                        
                        NSDictionary *instanceData = instancesFromSectionUnit[index];
                        
                        [[LMDownloadManager sharedInstance] createRequestForInstanceFor:[USER_MANAGER takeContentVersion]
                                                                             levelIndex:levelOrder
                                                                           sectionIndex:sectionIdx
                                                                              unit:unit
                                                                                orderID:[instanceData[kOrder] integerValue]
                                                                                  pages:[instanceData[kPages] integerValue]
                                                                             instanceID:[instanceData[kId] integerValue]
                                                                                 synced:NO];

                        
                    }
                    
                }
                
            }
            
        }
        
        [DOWNLOAD_MANAGER startRequestsExecution];
              
        [DOWNLOAD_MANAGER executeNextRequest];
        
    }
        
}

- (void)instanceRequestDidFinishWithExecution:(NSInteger)levelOrder
                                 sectionIndex:(NSInteger)sectionIndex
                                    unitIndex:(NSInteger)unitIndex
                                    unit:(Unit *)unit
                                instanceOrder:(NSInteger)instanceOrder
                                        pages:(NSInteger)pages
                                    nextIndex:(NSInteger)nextIndex
                                       synced:(BOOL)synced
                                    withError:(BOOL)withError
{
      dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
      dispatch_async(queue, ^{
    
          [[DATA_SERVICE dataModel] saveModel];
          
          CourseStructure *courseStructure = [DATA_SERVICE dataModel].courseData;
          
          LevelData *currentLevelData = courseStructure.levelsArray[levelOrder];
          
          NSArray *sectionsFromLevelData = [currentLevelData sectionsArray];

          Section *section = sectionsFromLevelData[sectionIndex];
          
          Unit *eUnit = section.unitsArray[unitIndex];

          DLog(@">>MODEL IS SAVED AFTER QUEUE FOR INSTANCE");
          
            if ([self checkFinishedUnit:unit]) {
                [PROGRESS_MANAGER unitInstancesDownloadCompleted:levelOrder sectionIndex:sectionIndex unitIndex:unitIndex];
                [self.delegate refreshUnitsData];
            }

            if ([DOWNLOAD_MANAGER checkInstanceExecutionEnded:levelOrder] && [self checkUnitInstancesDownloadedCorrectly:sectionsFromLevelData]) {

                DLog(@">>LEVEL DOWNLOADED COMPLETE");

                if ([[LMDownloadManager sharedInstance] checkDownloadProgress])
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setBool:YES forKey:kDataCompleteDownload];
                    [defaults synchronize];
                }

            } else {

                if (![DOWNLOAD_MANAGER checkInstanceExecutionEnded:levelOrder] && [[LMDownloadManager sharedInstance] isManagerExecutingRequests]) {

                    if ([[Utilities sharedInstance] getFreeDiskspace] > ((100 * 1024) * 1024)) {
                        [[LMDownloadManager sharedInstance] executeNextRequest];
                    } else {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                        [[LMDownloadManager sharedInstance] cancelRequestsExecution];
                        [self.delegate returnSyncStatusWithError:YES cause:kAlertOutOfStorage];
                    }
                    
                }
            }
      });
}

- (void)courseInfoRequestDidFinishWithExecution:(BOOL)withError
{

    if (!withError) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
        
        NSString *courseJSONPath = [dataPath stringByAppendingPathComponent:kInitialCourseJSON];
        
        NSDictionary *courseJSONDataParsed = [[Utilities sharedInstance] createDictionaryFromJSONAtPath:courseJSONPath];
        
        CourseStructure *courseStructure = [DATA_SERVICE dataModel].courseData;
        
        if (![courseJSONDataParsed[kCourse] isEqualToString:courseStructure.course]) {
            
            [courseStructure replaceCourseInfoIfDifferent:courseJSONDataParsed];
            
            [[DATA_SERVICE dataModel] saveModel];
            
            if (![courseJSONDataParsed[kCharacters] isEqualToString:courseStructure.characters]) {
                
                [DOWNLOAD_MANAGER executeCharactersRequest];
                
            }
            
            [DOWNLOAD_MANAGER executeCourseRequest];
            
        }
        APP_DELEGATE.isCourseDataSynced = YES;
        
        [DOWNLOAD_MANAGER setCurrentLevelForSync:PROGRESS_MANAGER.currentLevelIndex levelVersion:[USER_MANAGER takeLevelData][PROGRESS_MANAGER.currentLevelIndex][kLevelVersion]];
        
        [DOWNLOAD_MANAGER startRequestsExecution];
        
        [DOWNLOAD_MANAGER executeLevelRequestForLevelID:PROGRESS_MANAGER.currentLevelIndex];
        
    }
}

- (void)courseRequestDidFinishWithExecution:(BOOL)withError
{
    if (!withError) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
        
        NSString *coursePropPath = [dataPath stringByAppendingPathComponent:kInitialCourseZIP];
        
        NSDictionary *courseJSONDataParsed = [[Utilities sharedInstance] extractArchiveAndReturnData:coursePropPath];
        
        CourseStructure *courseStructure = [DATA_SERVICE dataModel].courseData;
        CourseProperties *courseProperties = [courseStructure properties];
        
        [courseProperties replaceIfDifferent:courseJSONDataParsed];
        
        [[DATA_SERVICE dataModel] saveModel];

        
    }
}


#pragma mark -
#pragma mark - Helper method check if base instance exists

- (BOOL)isBaseInstanceAlreadyDownloaded:(NSMutableArray *)arrayInstances baseInstance:(BaseInstance *)instance {
    
    for (BaseInstance *currentInstance in arrayInstances) {
        
        if ([currentInstance isKindOfClass:[BaseInstance class]]) {
            
            if (currentInstance.mID == instance.mID) {
                
                return YES;
            }
            
        }
        
    }
    
    return NO;
}


- (void)charactersRequestDidFinishWithExecution:(BOOL)withError
{
    if (!withError) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
        
        NSString *charactersZIPPath = [dataPath stringByAppendingPathComponent:kInitialCharactersZIP];
        
        NSDictionary *charactersJSONDataParsed = [[Utilities sharedInstance] extractArchiveAndReturnData:charactersZIPPath];
        
        Characters *charactersData = [DATA_SERVICE dataModel].charactersData;
        
        [charactersData replaceCharactersIfDifferent:charactersJSONDataParsed];
        
        [[DATA_SERVICE dataModel] saveModel];
        
    }
}

- (void)syncDidFinishWithExecution:(BOOL)hasError cause:(EAlertViewCause)cause
{
    if (hasError) {
        [self.delegate returnSyncStatusWithError:YES cause:cause];
    }
}

- (BOOL)checkFinishedUnit:(Unit *)finishedUnit {
    
    BOOL isFinished = NO;
    NSInteger downloadedInstances = 0;
    
    NSArray *downloadedInstnaces = finishedUnit.instancesArray;
    NSArray *instancesToBeDownloaded = finishedUnit.instancesInfosArray;
    
    for (int i = 0; i < [instancesToBeDownloaded count]; i++)
    {
        NSDictionary *instanceInfo = instancesToBeDownloaded[i];
        
        for (int j = 0; j < [downloadedInstnaces count]; j++)
        {
            BaseInstance *downloadedInstnace = downloadedInstnaces[j];
            
            if ([instanceInfo[kId] integerValue] == downloadedInstnace.mID)
            {
                downloadedInstances++;
                break;
            }
            
        }
    }
    
    if (([instancesToBeDownloaded count] == downloadedInstances) && ([instancesToBeDownloaded count] == [downloadedInstnaces count])) {
        isFinished = YES;
    }
    
    return isFinished;
    
}

- (BOOL)checkUnitInstancesDownloadedCorrectly:(NSArray *)sections
{
    BOOL contentIsCorrect = NO;
    
    NSInteger numberOfUnitsInLevel = 0;
    NSInteger completeUnitCounter = 0;
    
    for (int i = 0; i < [sections count]; i++) {
        
        Section *section = sections[i];
        
        NSArray *unitsFromSection = [section unitsArray];
        numberOfUnitsInLevel += [unitsFromSection count];
        
        for (Unit *unit in unitsFromSection) {
            
            DLog(@"UNIT ID:%d NEED INSTNACES:%d DOWNLOADED INSTNACES:%d FROM REQUESTED:%d", unit.unitID, unit.numberOfInstances, [unit.instancesArray count], [unit.instancesInfosArray count]);
            
            if ([unit.instancesArray count] == unit.numberOfInstances) {
                completeUnitCounter++;
            }
            
        }
        
    }
    
    if (numberOfUnitsInLevel == completeUnitCounter) {
        contentIsCorrect = YES;
    }
    
    return contentIsCorrect;
}

- (void) continueSyncing
{
    if ([[Utilities sharedInstance] getFreeDiskspace] > ((100 * 1024) * 1024)) {
        [DOWNLOAD_MANAGER executeNextRequest];
    } else {
        [DOWNLOAD_MANAGER cancelRequestsExecution];
        [self.delegate returnSyncStatusWithError:YES cause:kAlertOutOfStorage];
    }
}

@end
