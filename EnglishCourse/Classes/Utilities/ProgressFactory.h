//
//  ProgressFactory.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/26/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseStatistic.h"
#import "LevelStatistic.h"
#import "UnitStatistic.h"
#import "InstanceStatistic.h"
#import "SectionStatistic.h"
#import "PageStatistic.h"
#import "LMEnums.h"

@interface ProgressFactory : NSObject

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type;
@end
