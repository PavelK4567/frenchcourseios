//
//  ProgressFactory.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/26/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "ProgressFactory.h"

@implementation ProgressFactory

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type
{
    BaseStatistic *statistic = nil;
    
    switch (type) {
        case UserStatisticType:
            statistic = [UnitStatistic buildStatisticFromObject:dataObject
                                                    childObject:childObject
                                                  statisticType:UserStatisticType];
            break;
        
        case LevelStatisticType:
            statistic = [LevelStatistic buildStatisticFromObject:dataObject
                                                    childObject:childObject
                                                  statisticType:LevelStatisticType];
            break;
        case SectionStatisticType:
            statistic = [SectionStatistic buildStatisticFromObject:dataObject
                                                    childObject:childObject
                                                  statisticType:SectionStatisticType];
            break;
        case UnitStatisticType:
            statistic = [UnitStatistic buildStatisticFromObject:dataObject
                                                        childObject:childObject
                                                      statisticType:UnitStatisticType];
            break;
        case InstanceStatisticType:
            statistic = [InstanceStatistic buildStatisticFromObject:dataObject
                                                        childObject:childObject
                                                      statisticType:InstanceStatisticType];
            break;
        case PageStatisticType:
            statistic = [PageStatistic buildStatisticFromObject:dataObject
                                                    childObject:childObject
                                                  statisticType:PageStatisticType];
            break;
        default:
            break;
    }
    
    return statistic;
}
@end
