//
//  ECAnswerGroup.h
//  ECPractices
//
//  Created by Dimitar Shopovski on 11/26/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomVehicleButton.h"

typedef NS_ENUM(NSUInteger, TypeOfAnswer) {
    SimpleTextAnswer,
    ImageAnswerWithTranslation,
    ProgressiveChatAnswer,
};

@interface ECAnswerGroup : UIView <CustomVehicleButtonDelegate>

@property (nonatomic) BOOL isRadioGroup;
@property (nonatomic, strong) NSMutableArray *arrayAnswers;

@property (nonatomic, strong) CustomVehicleButton *currentSelectedButton;

@property (nonatomic) CGPoint centerPoint;

- (void)selectTheCorrectAnswer;

@property TypeOfAnswer answerType;

@end
