//
//  PracticeQuestionScreen.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/29/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECVehicleDisplay.h"
#import "CustomVehicleButton.h"
#import "PracticePage.h"
#import "UIView+Extras.h"

@interface PracticeQuestionScreen : ECVehicleDisplay <CustomVehicleButtonDelegate> {
    
    NSInteger currentPage;
    NSInteger numberOfPages;
}


@end
