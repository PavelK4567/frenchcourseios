//
//  MissingWordContainerViewL1.h
//  MissingWord
//
//  Created by Darko Trpevski on 12/13/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MissingWordView.h"
#import "WordListView.h"
#import "ECVehicleDisplay.h"
#import "MissingWordTitleView.h"
#import "FillTheMissingWords.h"

@interface MissingWordContainerViewL1 : ECVehicleDisplay <QuestionListDelegate,WordListDelegate,ECVehicleDisplayDelegate,UIScrollViewDelegate>
{
 
    WordListView *wordListView;
    UIScrollView *questionsScrollView;
    MissingWordTitleView *titleView;
    FillTheMissingWords *fillTheMissingWords;
    NSMutableArray *words;
    NSMutableArray *translations;
    NSMutableArray *randomWords;
    NSMutableArray *randomTranslations;
    NSString *answer;
    NSString *question;
    NSString *title;
    NSString *correctWord;
    NSInteger questionIndex;
    BOOL correctFeedback;
}

@property (nonatomic) BOOL renderCompletedViews;

- (void)loadDataForIndex:(NSInteger)index;
- (MissingWordView *)getLastQuestionView;

@end
