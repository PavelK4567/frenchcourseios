//
//  WordListView.h
//  MissingWord
//
//  Created by Darko Trpevski on 12/13/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WordListInterface.h"



@interface WordListView : UIView <WordListDelegate>
{
    UIScrollView *wordsScrollView;
}

@property (nonatomic,strong) NSArray *wordList;
@property (nonatomic,strong) NSArray *translationsList;
@property (nonatomic) CGPoint placeholderPoint;
@property (nonatomic, strong) NSString *placeholderWord;
@property (nonatomic) NSInteger wordIndex;
@property (nonatomic) NSInteger attemptsCount;
@property (nonatomic, weak) id<WordListDelegate>delegate;

- (void)loadGUI;
- (void)loadData;
- (void)removeScrollViewSubviews;
- (void)cleanWords;
@end
