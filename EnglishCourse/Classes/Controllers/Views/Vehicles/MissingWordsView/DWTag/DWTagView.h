//
//  DWTagView.h
//  MissingWord
//
//  Created by Darko Trpevski on 12/12/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"

@interface DWTagView : UIView

@property (nonatomic, strong) NSString *word;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic) BOOL selected;

@property (nonatomic, weak)   id<DWTagViewDelegate> delegate;

- (void)updateWithString:(NSString*)text
                    font:(UIFont*)font
      constrainedToWidth:(CGFloat)maxWidth
                 padding:(CGSize)padding
            minimumWidth:(CGFloat)minimumWidth;
- (void)setLabelText:(NSString*)text;
- (void)setCornerRadius:(CGFloat)cornerRadius;
- (void)setBorderColor:(CGColorRef)borderColor;
- (void)setBorderWidth:(CGFloat)borderWidth;
- (void)setTextColor:(UIColor*)textColor;
- (void)setTextShadowColor:(UIColor*)textShadowColor;
- (void)setTextShadowOffset:(CGSize)textShadowOffset;

@end




@protocol DWTagListDelegate <NSObject>

@optional

- (void)selectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex;
- (void)selectedTag:(NSString *)tagName;
- (void)tagListTagsChanged:(DWTagList *)tagList;
- (void)showToolTip:(DWTagView *)view;

@end

@protocol DWTagViewDelegate <NSObject>

@required

- (void)tagViewWantsToBeDeleted:(DWTagView *)tagView;

@end
