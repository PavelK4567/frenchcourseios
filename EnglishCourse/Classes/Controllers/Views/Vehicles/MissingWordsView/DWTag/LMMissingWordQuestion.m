//
//  LMMissingWordQuestion.m
//  MissingWord
//
//  Created by Darko Trpevski on 12/12/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMMissingWordQuestion.h"

#define CORNER_RADIUS 0.0f
#define LABEL_MARGIN_DEFAULT 0.0f
#define BOTTOM_MARGIN_DEFAULT 1.0f
#define FONT_SIZE_DEFAULT 20.0f
#define HORIZONTAL_PADDING_DEFAULT 0.0f
#define VERTICAL_PADDING_DEFAULT 1.0f
#define BACKGROUND_COLOR [UIColor clearColor]
#define TEXT_COLOR [UIColor blackColor]
#define TEXT_SHADOW_COLOR [UIColor clearColor]
#define TEXT_SHADOW_OFFSET CGSizeMake(0.0f, 0.0f)
#define BORDER_COLOR [UIColor clearColor]
#define BORDER_WIDTH 0.0f
#define HIGHLIGHTED_BACKGROUND_COLOR [UIColor clearColor]
#define DEFAULT_AUTOMATIC_RESIZE NO
#define DEFAULT_SHOW_TAG_MENU NO


@implementation LMMissingWordQuestion

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:view];
        [self setClipsToBounds:YES];
        self.automaticResize = YES;
        self.highlightedBackgroundColor = HIGHLIGHTED_BACKGROUND_COLOR;
        self.font = [UIFont systemFontOfSize:FONT_SIZE_DEFAULT];
        self.labelMargin = LABEL_MARGIN_DEFAULT;
        self.bottomMargin = BOTTOM_MARGIN_DEFAULT;
        self.horizontalPadding = HORIZONTAL_PADDING_DEFAULT;
        self.verticalPadding = VERTICAL_PADDING_DEFAULT;
        self.cornerRadius = CORNER_RADIUS;
        self.borderColor = BORDER_COLOR;
        self.borderWidth = BORDER_WIDTH;
        self.textColor = TEXT_COLOR;
        self.textShadowColor = TEXT_SHADOW_COLOR;
        self.textShadowOffset = TEXT_SHADOW_OFFSET;
        self.showTagMenu = DEFAULT_SHOW_TAG_MENU;
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self addSubview:view];
        [self setClipsToBounds:YES];
        self.highlightedBackgroundColor = HIGHLIGHTED_BACKGROUND_COLOR;
        self.font = [UIFont systemFontOfSize:FONT_SIZE_DEFAULT];
        self.labelMargin = LABEL_MARGIN_DEFAULT;
        self.bottomMargin = BOTTOM_MARGIN_DEFAULT;
        self.horizontalPadding = HORIZONTAL_PADDING_DEFAULT;
        self.verticalPadding = VERTICAL_PADDING_DEFAULT;
        self.cornerRadius = CORNER_RADIUS;
        self.borderColor = BORDER_COLOR;
        self.borderWidth = BORDER_WIDTH;
        self.textColor = TEXT_COLOR;
        self.textShadowColor = TEXT_SHADOW_COLOR;
        self.textShadowOffset = TEXT_SHADOW_OFFSET;
        self.showTagMenu = DEFAULT_SHOW_TAG_MENU;
    }
    return self;
}

- (void)display
{
    NSMutableArray *tagViews = [NSMutableArray array];
    for (UIView *subview in [self subviews]) {
        if ([subview isKindOfClass:[LMTagView class]]) {
            LMTagView *tagView = (LMTagView*)subview;
            for (UIGestureRecognizer *gesture in [subview gestureRecognizers]) {
                [subview removeGestureRecognizer:gesture];
            }
            
            [tagView.button removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
            
            [tagViews addObject:subview];
        }
        [subview removeFromSuperview];
    }
    
    CGRect previousFrame = CGRectZero;
    BOOL gotPreviousFrame = NO;
    
    NSInteger tag = 0;
    for (id text in textArray) {
        LMTagView *tagView;
        if (tagViews.count > 0) {
            tagView = [tagViews lastObject];
            [tagViews removeLastObject];
        }
        else {
            tagView = [[LMTagView alloc] init];
        }
        
        
        [tagView updateWithString:text
                             font:self.font
               constrainedToWidth:self.frame.size.width - (self.horizontalPadding * 2)
                          padding:CGSizeMake(self.horizontalPadding, self.verticalPadding)
                     minimumWidth:self.minimumWidth
         ];
        
        if (gotPreviousFrame) {
            CGRect newRect = CGRectZero;
            if (previousFrame.origin.x + previousFrame.size.width + tagView.frame.size.width + self.labelMargin > self.frame.size.width) {
                newRect.origin = CGPointMake(0, previousFrame.origin.y + tagView.frame.size.height + self.bottomMargin);
            } else {
                newRect.origin = CGPointMake(previousFrame.origin.x + previousFrame.size.width + self.labelMargin, previousFrame.origin.y);
            }
            newRect.size = tagView.frame.size;
            [tagView setFrame:newRect];
        }
        
        previousFrame = tagView.frame;
        gotPreviousFrame = YES;
        
        [tagView setBackgroundColor:[self getBackgroundColor]];
        [tagView setCornerRadius:self.cornerRadius];
        [tagView setBorderColor:self.borderColor.CGColor];
        [tagView setBorderWidth:self.borderWidth];
        [tagView setTextColor:self.textColor];
        [tagView setTextShadowColor:self.textShadowColor];
        [tagView setTextShadowOffset:self.textShadowOffset];
        [tagView setTag:tag];
        [tagView setDelegate:self];
        [tagView setHeight:35];
        if([LMHelper isWordMatchingPattern:[self.allWords objectAtIndex:tag] pattern:kWordPattern]) {
            [tagView setIsEnabled:YES];
        }
        else {
         [tagView setIsEnabled:NO];
         [tagView.label setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:20]];
         [tagView.label setTextColor:[UIColor colorWithRed:51/255 green:51/255 blue:51/255 alpha:1]];
        }
        
        [tagView setWord:[self.allWords objectAtIndex:tag]];
        
        if([self.correctWordsIndexes containsObject:[NSNumber numberWithInteger:tag]])
        {
            [tagView setIsEnabled:NO];
        }
        
        [tagView.label sizeToFit];
        tagView.button.frame = tagView.label.frame;
        tag++;
        
        [self addSubview:tagView];
        
        if (!self.viewOnly) {
            [tagView.button addTarget:self action:@selector(touchDownInside:) forControlEvents:UIControlEventTouchDown];
            [tagView.button addTarget:self action:@selector(touchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            [tagView.button addTarget:self action:@selector(touchDragExit:) forControlEvents:UIControlEventTouchDragExit];
            [tagView.button addTarget:self action:@selector(touchDragInside:) forControlEvents:UIControlEventTouchDragInside];
        }
    }
    
    sizeFit = CGSizeMake(self.frame.size.width, previousFrame.origin.y + previousFrame.size.height + self.bottomMargin + 1.0f);
    self.contentSize = sizeFit;
}


- (void)layoutSelection:(NSInteger)index
{
    for (int i = 0; i < [self.subviews count]; i++) {

        if(i != index && [[self.subviews objectAtIndex:i] isKindOfClass:[LMTagView class]]) {
            LMTagView *tagView = (LMTagView *)[self.subviews objectAtIndex:i];
            
            if(!tagView.isCorrect)
                [tagView setSelected:YES];
        }
    }
}

- (void)resetSelection
{
    for (int i = 0; i < [self.subviews count]; i++) {
        
        if([[self.subviews objectAtIndex:i] isKindOfClass:[LMTagView class]]) {
            LMTagView *tagView = (LMTagView *)[self.subviews objectAtIndex:i];
            
            [tagView setSelected:YES];
        }
    }
}

- (void)layoutCorrectTags
{
    for (id correctIndex in self.correctWordsIndexes) {
        
       NSInteger index = [correctIndex integerValue];
       if([[self.subviews objectAtIndex:index] isKindOfClass:[LMTagView class]]) {
           
           LMTagView *tagView = (LMTagView *)[self.subviews objectAtIndex:index];
           [tagView.label sizeToFit];
           tagView.width = tagView.label.mWidth;
           [tagView setIsCorrect:YES];
           [tagView setIsEnabled:NO];
           [tagView layoutCorrectWord];
       }
    }
}

- (void)setCorrectWordsIndexes:(NSArray *)correctWordsIndexes
{
    _correctWordsIndexes = [NSArray arrayWithArray:correctWordsIndexes];
}

@end
