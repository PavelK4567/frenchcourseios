//
//  WordListFlowLayout.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/18/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "WordListFlowLayout.h"
#import "LMHelper.h"
#import "LMBallonTip.h"

@implementation WordListFlowLayout


#pragma mark - loadGUI

- (void)loadGUI
{
    [self layoutGUI];
    [self addSubview:_tagList];
}


#pragma mark - layoutGUI

- (void)layoutGUI
{
    
    self.tagList = [[LMFlowTagList alloc] initWithFrame:self.frame];
    [self.tagList setAutomaticResize:YES];
    [self.tagList setTags:self.array];
    [self.tagList setTagDelegate:self];
    [self.tagList setCornerRadius:6.0];
    [self.tagList setBorderWidth:1.0];
    [self.tagList setLabelMargin:1.0];
    [self.tagList setHeight:kTagListHeight];
    [self.tagList setY:kZeroPositionValue];
    [self.tagList setX:kZeroPositionValue];

}


#pragma mark - findCorrectWordButton

- (UIButton *)findCorrectWordButton
{
    UIButton *correctWordButton;
    NSString *correctWord = [LMHelper removeCharsFromString:self.placeholderWord];
    for (id tagView in self.tagList.subviews) {
        
        if([tagView isKindOfClass:[LMFlowTagView class]] && [((LMFlowTagView *)tagView).label.text isEqualToString:correctWord])
        {
            correctWordButton = ((LMFlowTagView *)tagView).button;
        }
    };
    return correctWordButton;
}


#pragma mark - findCorrectWordPosition

- (CGPoint)findCorrectWordPosition
{
    CGPoint position;
    NSString *correctWord = [LMHelper removeCharsFromString:self.placeholderWord];
    for (id tagView in self.tagList.subviews) {
        
        if([tagView isKindOfClass:[LMFlowTagView class]] && [((LMFlowTagView *)tagView).label.text isEqualToString:correctWord]) {
            LMFlowTagView *wordView = (LMFlowTagView *)tagView;
            position = [wordView.superview convertPoint:wordView.center toView:nil];
        }
    };

    
    return position;
}


#pragma mark - findViewAtIndex

- (LMFlowTagView *)findViewAtIndex:(NSArray *)views index:(NSInteger)index
{
    LMFlowTagView *selectedView;
    
    for (int i = 0; i < views.count; i++) {
       if([views[i] isKindOfClass:[LMFlowTagView class]] && index == i) {
           selectedView = views[i];
           break;
       }
    }
    return selectedView;
}


#pragma mark - show tooltip

- (void)showToolTip:(LMFlowTagView *)view
{
   
    __weak typeof(self) weakSelf = self;
    
    LMBallonTip *toolTip = [LMBallonTip getFromNib];
    [toolTip setTranslatedWord:[self.translations objectAtIndex:view.tag]];
    [toolTip setX:view.x andY:view.y - toolTip.mHeight - 5];
    [self addSubview:toolTip];
    [toolTip setAlpha:kZeroPointValue];
    
    [UIView animateWithDuration:1.0
                          delay:kZeroPointValue
                        options: UIViewAnimationOptionAutoreverse
                     animations: ^{
                         toolTip.alpha = 1;
                         [weakSelf setUserInteractionEnabled:NO];

                     }
                     completion: ^(BOOL finished) {
                         toolTip.alpha = kZeroPointValue;
                         [weakSelf setUserInteractionEnabled:YES];
                     }];

}

#pragma mark - button action

- (void)selectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex
{
    UIButton *correctWordButton = [self findCorrectWordButton];
    
    if (self.placeholderWord)
    {
        self.attemptsCount ++;
        if ([[LMHelper removeCharsFromString:self.placeholderWord] isEqualToString:tagName]) {
            [self correctAnswerButtonAnimation:correctWordButton];
            [self resetTagNumbers];
        }
        else if(![[LMHelper removeCharsFromString:self.placeholderWord] isEqualToString:tagName] && self.attemptsCount > 2) {
            UIButton *senderButton = [self findCorrectWordButton];
            
            [self correctAnswerButtonAnimation:senderButton];
            [self resetTagNumbers];
        }
        else {
            
            [self wrongAnswerButtonAnimation:[self findViewAtIndex:self.tagList.subviews index:tagIndex]];
        }
    }
    
}


#pragma mark - correctWordAnimation

- (void)correctAnswerButtonAnimation:(UIButton *)button
{
    __weak typeof(self) weakSelf = self;
    
    CGPoint wordPoint = [self findCorrectWordPosition];
    
    NSString *correctWord = [LMHelper removeCharsFromString:[self.wordList objectAtIndex:self.wordIndex]];
    LMFlowTagView *correctWordButton = [LMHelper createButtonViewCopy:(LMFlowTagView *)[(UIButton *)[self findCorrectWordButton] superview]];
    correctWordButton.center = wordPoint;
    
    if([correctWord isEqualToString:[LMHelper removeCharsFromString:self.placeholderWord]])
    {
        [self.window addSubview:correctWordButton];
        [UIView animateWithDuration:1.0 animations:^{
            
            [correctWordButton setCenter:weakSelf.placeholderPoint];
            [weakSelf setUserInteractionEnabled:NO];
            
        } completion:^(BOOL finished) {
            
            [weakSelf setPlaceholderWord:nil];
            
            if ([weakSelf.delegate respondsToSelector:@selector(correctWordIndex:)]) {
                [weakSelf.delegate correctWordIndex:weakSelf.wordIndex];
            }
            
            [correctWordButton removeFromSuperview];
            [weakSelf setUserInteractionEnabled:YES];
        }];
    }
}


#pragma mark - wrongAnswerButtonAnimation

- (void)wrongAnswerButtonAnimation:(LMFlowTagView *)button
{
    __weak typeof(self) weakSelf = self;
    
    [UIView animateWithDuration:1.0 animations:^{
        [weakSelf setUserInteractionEnabled:NO];
        [button setBackgroundColor:[UIColor redColor]];
    } completion:^(BOOL finished) {
        [weakSelf setUserInteractionEnabled:YES];
        [button setBackgroundColor:[UIColor colorFromHexString:kButtonColor]];
    }];
    
}


#pragma mark - resetTagNumbers

- (void)resetTagNumbers
{
    self.attemptsCount = 0;
}

#pragma mark -
@end
