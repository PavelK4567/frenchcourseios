//
//  MissingWordTitleView.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/20/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MissingWordTitleView.h"

@implementation MissingWordTitleView

#pragma mark - bindGUI

- (void)bindGUI
{
    [self.firstPersonLabel setText:self.firstPersonItem.name];
    [self.secondPersonLabel setText:self.secondPersonItem.name];
    [self.firstPersonImageView setImageFromDocumentsResourceFile:self.firstPersonItem.image];
    [self.secondPersonImageView setImageFromDocumentsResourceFile:self.secondPersonItem.image];
    [self.firstPersonImageView setClipsToBounds:YES];
    [self.secondPersonImageView setClipsToBounds:YES];
    
    [self layoutGUI];
}


#pragma mark - layoutGUI

- (void)layoutGUI
{
    self.firstPersonImageView.layer.cornerRadius  = self.firstPersonImageView.mHeight / 2.0;
    self.secondPersonImageView.layer.cornerRadius = self.firstPersonImageView.mHeight / 2.0;
}

#pragma mark -
@end
