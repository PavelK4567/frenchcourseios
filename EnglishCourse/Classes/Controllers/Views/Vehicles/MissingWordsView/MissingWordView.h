//
//  MissingWordView.h
//  MissingWord
//
//  Created by Darko Trpevski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMMissingWordQuestion.h"
#import "QuestionListInterface.h"


@interface MissingWordView : UIView <DWTagListDelegate>

@property (nonatomic, strong) LMMissingWordQuestion *tagList;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) NSMutableArray *placeholderArray;
@property (nonatomic, strong) NSMutableArray *correctWordIndexes;
@property (nonatomic, strong) NSString *questionString;
@property (nonatomic, strong) NSString *answer;
@property (nonatomic, strong) NSString *correctWord;
@property (nonatomic) CGPoint placeholderPoint;
@property (nonatomic) BOOL isFinish;
@property (nonatomic, weak) id<QuestionListDelegate>delegate;

- (void)addCorrectWordIndex:(NSInteger)index;
- (void)selectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex;
- (void)resizeAllViews;
- (void)correctIndexOfWord;
- (void)insertAnswerView;
@end
