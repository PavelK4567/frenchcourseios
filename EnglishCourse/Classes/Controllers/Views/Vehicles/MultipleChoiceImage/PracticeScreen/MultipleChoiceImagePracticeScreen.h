//
//  MultipleChoiceImagePracticeScreen.h
//  ECMultiImage
//
//  Created by Dimitar Shopovski on 11/19/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InteractiveImageAnswer.h"
#import "ECVehicleDisplay.h"

@interface MultipleChoiceImagePracticeScreen : ECVehicleDisplay <InteractiveImageAnswerDelegate, FeedbackViewDelegate>

@property (nonatomic, strong) NSDictionary *dictPracticeInfo;
@property (nonatomic, strong) NSMutableArray *arrayAnswers;

@property float width;
@property float yPosAnswers;

@property (nonatomic, strong) InteractiveImageAnswer *currentActiveAnswer;

@property (nonatomic, readwrite) NSInteger nPosibleAttempts;

//GUI elements
@property (nonatomic, strong) UIButton *btnSound;

- (void)showTheQuestion;
- (IBAction)playSoundQuestion:(id)sender;


@property (nonatomic, strong) NSMutableArray *arrayPossibleAnswers;
- (void)initializeElements;
- (void)drawGUI;

@end
