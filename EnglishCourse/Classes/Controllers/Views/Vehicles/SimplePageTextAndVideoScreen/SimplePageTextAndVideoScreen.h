//
//  SimplePageTextAndVideoScreen.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import <MediaPlayer/MediaPlayer.h>
#import "ECVehicleDisplay.h"
#import "SimplePageTextAndVideo.h"

@interface SimplePageTextAndVideoScreen : ECVehicleDisplay

@property (strong, nonatomic) UIView *viewContainer;

@property (weak, nonatomic) IBOutlet UIView *simpleVideoPlayerContainer;
@property (weak, nonatomic) IBOutlet UIImageView *previewImage;
@property (weak, nonatomic) IBOutlet UIButton *playVideoButton;

@property (nonatomic, strong) UITextView *txtProfileIntroduction;

@property (nonatomic, assign) BOOL isVideoPlaying;

@property (nonatomic, strong) SimplePageTextAndVideo *simplePageModelData;

@property (nonatomic, strong) MPMoviePlayerController *myPlayerViewController;

@property (nonatomic) BOOL allowRotation;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

- (void)stopPlaying;

- (void)drawGUI;
- (void)initData;
- (void)disappearSimplePageTextAndVideo;
@end
