//
//  SimplePageTextAndVideoScreen.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SimplePageTextAndVideoScreen.h"

#define kTagSimpleLabelView 101
#define kTagVideoContainer 102
#define kTagPreviewImage 103
#define kTagPlayButton 104

#define kTagPlayer 105
#define kTagScrollView 106

#define kScrollOffest 50


@implementation SimplePageTextAndVideoScreen

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawGUI
{
    self.viewContainer = [[[NSBundle mainBundle] loadNibNamed:@"SimplePageTextAndVideoScreen" owner:self options:nil] objectAtIndex:0];
    [self.viewContainer setBackgroundColor:[APPSTYLE colorForType:@"New_Vehicle_Background"]];
    
    self.txtProfileIntroduction = (UITextView *)[self.viewContainer viewWithTag:kTagSimpleLabelView];;
    self.txtProfileIntroduction.frame = CGRectMake(8, self.txtProfileIntroduction.y, self.frame.size.width-16, 221);
    self.txtProfileIntroduction.textAlignment = NSTextAlignmentLeft;
    self.txtProfileIntroduction.textColor = [UIColor colorWithRed:70.0/255 green:66.0/255.0 blue:66.0/255.0 alpha:1.0];
    self.txtProfileIntroduction.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0];
    self.txtProfileIntroduction.editable = NO;
    self.txtProfileIntroduction.userInteractionEnabled = NO;
    
    self.simpleVideoPlayerContainer = [self.viewContainer viewWithTag:kTagVideoContainer];
    self.previewImage = (UIImageView *)[self.viewContainer viewWithTag:kTagPreviewImage];
    self.playVideoButton = (UIButton *)[self.viewContainer viewWithTag:kTagPlayButton];
    
    [self.playVideoButton addTarget:self action:@selector(playVideoAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.viewContainer];
    
    [self bringSubviewToFront:self.viewTitleLabel];
}

- (void)initData
{
    if (self.dictVehicleInfo) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:((SimplePageTextAndVideo *)self.dictVehicleInfo).videoPreview];
        
        NSData *dataFromFile = [[NSFileManager defaultManager] contentsAtPath:dataPath];
        
        self.previewImage.image = [UIImage imageWithData:dataFromFile];
        
        [self.viewTitleLabel setTitleOftheInstance:((SimplePageTextAndVideo *)self.dictVehicleInfo).title];
        self.simpleVideoPlayerContainer.y = self.viewTitleLabel.mHeight - 12;

        self.txtProfileIntroduction.text = [((SimplePageTextAndVideo *)self.dictVehicleInfo) text];
        
        [self.txtProfileIntroduction sizeToFit];
        [self.txtProfileIntroduction layoutIfNeeded];
        
        self.txtProfileIntroduction.scrollEnabled = NO;
        
        if (self.txtProfileIntroduction.frame.origin.y + self.txtProfileIntroduction.contentSize.height + 20 > LM_SCROLL_HEIGHT) {
            
            [self setContentSize:CGSizeMake(LM_WIDTH_INSTACE, (self.txtProfileIntroduction.y + self.txtProfileIntroduction.contentSize.height+20))];
            
        }
        
        [AUDIO_MANAGER playInitialSound];
    }

}

- (void)stopPlaying {
    
    if (self.isVideoPlaying) {
        [self.myPlayerViewController stop];
        self.isVideoPlaying = NO; 
    }
    
}
- (void)disappearSimplePageTextAndVideo{
  [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidEnterFullscreenNotification object:self.myPlayerViewController];
   [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:self.myPlayerViewController];
   [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self.myPlayerViewController];
  
   [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
  
}
- (IBAction)playVideoAction:(id)sender {
    
    if (![ReachabilityHelper reachable]) {
        [UIAlertView alertWithCause:kAlertNoConnection];
       
        return;
    }
    
    if (self.myPlayerViewController == nil && [self.simpleVideoPlayerContainer.subviews containsObject:self.previewImage]) {
        
        CGSize containerSize = self.simpleVideoPlayerContainer.size;
        
        MPMoviePlayerController *playerViewController = [[MPMoviePlayerController alloc] init];
        playerViewController.view.frame = CGRectMake(0, 0, containerSize.width, containerSize.height);
        playerViewController.backgroundView.backgroundColor = [UIColor whiteColor];
        for(UIView *aSubView in playerViewController.view.subviews) {
          aSubView.backgroundColor = [UIColor whiteColor];
        }
        //[playerViewController setScalingMode:MPMovieScalingModeFill];
        playerViewController.view.tag = kTagPlayer;
       
        //[self.simpleVideoPlayerContainer addSubview:playerViewController.view];
        [self.simpleVideoPlayerContainer insertSubview:playerViewController.view belowSubview:self.previewImage];
      
        playerViewController.contentURL = [NSURL URLWithString:((SimplePageTextAndVideo *)self.dictVehicleInfo).video];
      
        playerViewController.controlStyle =  MPMovieControlStyleDefault;
        playerViewController.movieSourceType = MPMovieSourceTypeFile;
        //[playerViewController setScalingMode:MPMovieScalingModeAspectFit];
        playerViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterFullScreenMode) name:MPMoviePlayerDidEnterFullscreenNotification object:playerViewController];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitFullScreenMode) name:MPMoviePlayerDidExitFullscreenNotification object:playerViewController];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishPlaying) name:MPMoviePlayerPlaybackDidFinishNotification object:playerViewController];
        
        playerViewController.shouldAutoplay = YES;
      
      self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
      self.activityIndicator.center = self.previewImage.center;
      self.activityIndicator.color = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
      self.activityIndicator.transform = CGAffineTransformMakeScale(1.3, 1.3);
      self.activityIndicator.hidesWhenStopped = YES;
      [self.activityIndicator startAnimating];
      [self.previewImage addSubview:self.activityIndicator];
      
        self.myPlayerViewController = playerViewController;
        self.myPlayerViewController.view.alpha = 0;
      
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerLoadStateChanged) name:MPMoviePlayerPlaybackStateDidChangeNotification object:playerViewController];
      
     
      [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(detectOrientation)
                                                   name:UIDeviceOrientationDidChangeNotification
                                                 object:nil];
    }
    
    UIView *videoView = [self.simpleVideoPlayerContainer viewWithTag:kTagPlayer];
    videoView.hidden = NO;
    
    self.playVideoButton.hidden = YES;
    self.previewImage.hidden = YES;

    [self.myPlayerViewController prepareToPlay];
    [self.myPlayerViewController play];
    
    self.isVideoPlaying = YES;
    
}

- (void)enterFullScreenMode
{
    
    if ([self.dictVehicleInfo isKindOfClass:[SimplePageTextAndVideo class]]) {
        DLog(@"ENTER FULL SCREEN");
        
        ((LessonViewController *)self.viewController).isViedoActive = YES;
        
    }
    
}

- (void)exitFullScreenMode
{

    if ([self.dictVehicleInfo isKindOfClass:[SimplePageTextAndVideo class]]) {
        DLog(@"EXIT FULL SCREEN");
        
        ((LessonViewController *)self.viewController).isViedoActive = NO;
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    }
    
}

- (void)finishPlaying
{
  UIView *videoView = [self.simpleVideoPlayerContainer viewWithTag:kTagPlayer];
  videoView.hidden = YES;
  
  self.playVideoButton.hidden = NO;
  self.previewImage.hidden = NO;
  if (self.isVideoPlaying)
    [self.myPlayerViewController setFullscreen:NO animated:NO];
}
#pragma Mark Full Screen for Player
-(void) detectOrientation {
  if (self && self.isVideoPlaying && self.myPlayerViewController) {
    if (([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft) ||
        ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight)) {
      //dispatch_async(dispatch_get_main_queue(), ^
                    // {
                       //self.myPlayerViewController.fullscreen = YES;
                       [self.myPlayerViewController setFullscreen:YES animated:YES];
                   //  });
    } else if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait || [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown) {

                       [self.myPlayerViewController setFullscreen:NO animated:YES];
                    
    }
  }
}

- (void)moviePlayerLoadStateChanged
{
    if (![ReachabilityHelper reachable]) {
        [self.activityIndicator stopAnimating];
        self.playVideoButton.hidden = NO;
        
        [UIAlertView alertWithCause:kAlertNoConnection];
        
        return;
    }
  NSLog(@"loadState: %d", self.myPlayerViewController.loadState);
  //[self performSelector:@selector(moviePlayerLoadStateChanged) withObject:0 afterDelay:0.250];
  if(self.myPlayerViewController.loadState != 3){
    [self performSelector:@selector(moviePlayerLoadStateChanged) withObject:0 afterDelay:0.250];
  }else if(self.myPlayerViewController.loadState == 3){
    self.previewImage.hidden = YES;
    self.myPlayerViewController.view.alpha = 1;
    [self.activityIndicator stopAnimating];
  }
}
@end
