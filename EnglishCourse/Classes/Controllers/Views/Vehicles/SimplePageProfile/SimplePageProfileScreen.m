//
//  SimplePageProfileScreen.m
//  ECPractices
//
//  Created by Dimitar Shopovski on 11/20/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "SimplePageProfileScreen.h"

@implementation SimplePageProfileScreen

- (void)initializeElements {
    
    [self.viewTitleLabel setTitleOftheInstance:[(BaseInstance *)self.dictVehicleInfo title]];
    
    [self.imgProfile setImageFromDocumentsResourceFile:[(SimplePageProfile *)self.dictVehicleInfo image]];
    self.imgProfile.y = self.viewTitleLabel.mHeight;
    
    self.txtProfileIntroduction.text = [(SimplePageProfile *)self.dictVehicleInfo text];

    [self.txtProfileIntroduction sizeToFit];
    [self.txtProfileIntroduction layoutIfNeeded];
    self.txtProfileIntroduction.scrollEnabled = NO;
    
    self.txtProfileIntroduction.y = self.viewTitleLabel.mHeight + self.imgProfile.mHeight+10;
    
//    [self.txtProfileIntroduction setFrame:CGRectMake(self.txtProfileIntroduction.origin.x, self.txtProfileIntroduction.origin.y, self.txtProfileIntroduction.size.width, self.txtProfileIntroduction.contentSize.height + 16)];

    if (self.txtProfileIntroduction.frame.origin.y + self.txtProfileIntroduction.contentSize.height + 20 > LM_SCROLL_HEIGHT) {
        
        [self setContentSize:CGSizeMake(LM_WIDTH_INSTACE, (self.txtProfileIntroduction.y + self.txtProfileIntroduction.contentSize.height+20))];
    
    }
    
    
    if ([(SimplePageProfile *)self.dictVehicleInfo audio]) {
        
        self.btnProfileSound.hidden = NO;
    }

}


- (void)drawGUI {
    

    self.btnProfileSound = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnProfileSound.frame = CGRectMake(3, 3, 30, 34);
    [self.btnProfileSound setImage:[UIImage imageNamed:@"ic_small_sound.png"] forState:UIControlStateNormal];
    [self.btnProfileSound addTarget:self action:@selector(playIntroSound:) forControlEvents:UIControlEventTouchUpInside];
    self.btnProfileSound.hidden = YES;
    [self addSubview:self.btnProfileSound];
    
    self.imgProfile = [[UIImageView alloc] initWithFrame:CGRectMake(0, 80, widthImageSimpleProfile, heightImageSimpleProfile)];
    self.imgProfile.contentMode = UIViewContentModeScaleToFill;
    [self addSubview:self.imgProfile];
    
    self.txtProfileIntroduction = [[UITextView alloc] initWithFrame:CGRectMake(8, 178, self.frame.size.width-16, 10)];
    [self.txtProfileIntroduction setBackgroundColor:[UIColor clearColor]];
    [self.txtProfileIntroduction setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17.0]];
    self.txtProfileIntroduction.textAlignment = NSTextAlignmentLeft;
    self.txtProfileIntroduction.textColor = [UIColor colorWithRed:70.0/255 green:66.0/255.0 blue:66.0/255.0 alpha:1.0];
    self.txtProfileIntroduction.editable = NO;
    self.txtProfileIntroduction.userInteractionEnabled = YES;

    [self addSubview:self.txtProfileIntroduction];
    
    [self initializeElements];
}


- (IBAction)playIntroSound:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    [btn setHighlighted:NO];
    [btn setSelected:NO];
    
    if ([(SimplePageProfile *)self.dictVehicleInfo audio]) {
        
        [AUDIO_MANAGER playAudioFromDocumentPath:[(SimplePageProfile *)self.dictVehicleInfo audio] componentSender:self.btnProfileSound];

    }

}

@end
