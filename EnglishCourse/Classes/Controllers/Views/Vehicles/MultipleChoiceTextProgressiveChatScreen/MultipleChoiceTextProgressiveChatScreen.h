//
//  MultipleChoiceTextProgressiveChatScreen.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECVehicleDisplay.h"

#import "NSBubbleData.h"
#import "LMBubbleView.h"

#import "Character.h"

#import "LMAudioManager.h"

@protocol MultipleChoiceTextProgressiveChatDelegate <NSObject>

- (void)didFinishWithExecution;

@end

@interface MultipleChoiceTextProgressiveChatScreen : ECVehicleDisplay<LMBubbleViewDelegate, NSBubbleDataDelegate, LMAudioManagerDelegate> {
    
    BOOL isViewGenerated;
    BOOL viewCompleted;
    
}

@property (strong, nonatomic) UIView *nibView;

@property (nonatomic, strong) Character *characterFirst;
@property (nonatomic, strong) Character *characterSecond;

@property (nonatomic, strong) NSArray *charactersArray;
@property (nonatomic, strong) NSArray *segmentsArray;
@property (nonatomic, strong) NSMutableArray *bubblesDataArray;

@property (nonatomic, assign) BOOL isInitialSoundPlayed;

@property (nonatomic, assign) BOOL isAudioPlaying;

@property (nonatomic, weak) id<MultipleChoiceTextProgressiveChatDelegate>delegate;

@property (nonatomic, assign) NSInteger calculatedPoints;

@property (nonatomic, assign) BOOL completedInstance;

@property (nonatomic, strong) UIScrollView *bubbledScrollView;

@property (nonatomic, assign) BOOL isReplayed;
@property (nonatomic, assign) BOOL isDisplayed;
@property (nonatomic, assign) BOOL isReplayingFinished;

- (void)initData;
- (void)setupInstance;
- (void)drawCompletedInstance;
- (void)drawInstance;
- (void)playInitialSound;
- (void)test;

- (void)stopPlaying;

- (void)resetProgressiveChat;

- (void)clearProgressiveChat;


@end
