//
//  ECLevelTableViewCell.h
//  KMGame
//
//  Created by Dimitar Shopovski on 11/14/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAProgressLabel.h"

@interface ECLevelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblLevelName;
@property (weak, nonatomic) IBOutlet UIImageView *imgLevelImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgLevelStatus; //star image
@property (weak, nonatomic) IBOutlet KAProgressLabel *kaProgressLabel;

@end
