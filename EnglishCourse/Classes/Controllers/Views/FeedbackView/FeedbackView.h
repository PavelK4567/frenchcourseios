//
//  FeedbackView.h
//  Proba
//
//  Created by Dimitar Shopovski on 11/4/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Extras.h"

@protocol FeedbackViewDelegate <NSObject>

- (void)feedbackClose:(id)sender;
- (void)feedbackViewWasShown:(id)sender;

@optional
- (void)feedSwipe;

@end

@interface FeedbackView : UIView <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView *viewBg;
@property (nonatomic, strong) UILabel *lblBodyMessage;
@property (nonatomic, strong) UILabel *lblHeaderMessage;
@property (nonatomic, strong) UILabel *lblFooterMessage;

@property (nonatomic, strong) NSString *strFeedbackMessage;
@property BOOL isCorrectAnswer;
@property (nonatomic, weak) id <FeedbackViewDelegate> delegateFeedback;

- (void)setInformation:(BOOL)isCorrect;
- (void)showWithAnimation;
- (void)fadeOutWithAnimation;

- (void)tapGestureAction:(id)sender;
- (void)settTheInfoViewOnTheOriginalPosition;

@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipe;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipe;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@property (nonatomic, assign) Feedback *correctFeedback;
@property (nonatomic, assign) Feedback *incorrectFeedback;


@end
