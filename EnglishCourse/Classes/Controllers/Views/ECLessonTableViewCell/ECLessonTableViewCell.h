//
//  ECLevelTableViewCell.h
//  KMGame
//
//  Created by Dimitar Shopovski on 11/14/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgressCircleView.h"

@interface ECLessonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblLevelName;
@property (weak, nonatomic) IBOutlet UIImageView *imgLevelImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgLevelStatus; //star image
@property (weak, nonatomic) IBOutlet ProgressCircleView *progressCircleView;

@end
