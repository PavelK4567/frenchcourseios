//
//  PieChart.h
//  Try_PieChart
//
//  Created by Imran on 23/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PieChart : UIView {

}

@property (nonatomic, assign) float m_val1;
@property (nonatomic, assign) float m_val2;

-(void)setVal1:(float)val1 setVal2:(float)val2;

@end
