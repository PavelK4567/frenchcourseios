//
//  SettingsViewController.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/29/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMBaseViewController.h"

@interface SettingsViewController : LMBaseViewController{
  __weak IBOutlet UILabel *userDetailsLabel;
  __weak IBOutlet UILabel *statusLabel;
  __weak IBOutlet UILabel *userStatusInfoLabel;
  __weak IBOutlet UILabel *vivoNumberLabel;
  __weak IBOutlet UILabel *vivoNumberDetailsLabel;
  __weak IBOutlet UILabel *enableNotificationLabel;
  __weak IBOutlet UILabel *soundEfectsLabel;
  __weak IBOutlet UILabel *nativeUserInfoLabel;
  __weak IBOutlet UILabel *versionInfoLabel;
  __weak IBOutlet UISwitch *soundEfectsSwitch;
  __weak IBOutlet UISwitch *enableNotificationSwitch;
  __weak IBOutlet UIButton *logoutButton;
}

@end
