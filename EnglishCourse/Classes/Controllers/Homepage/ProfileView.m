//
//  ProfileView.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/1/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "ProfileView.h"
#import "ProgressCircleView.h"

@implementation ProfileView{
  
  __weak IBOutlet UIView *chartView;
  __weak IBOutlet UILabel *levelnfoLabel;
  __weak IBOutlet UILabel *pointsForDiploma;
  __weak IBOutlet UILabel *score1Label;
  __weak IBOutlet UILabel *lessonInfo1Label;//c
  __weak IBOutlet UILabel *lessonInfo2Label;
  __weak IBOutlet UILabel *performanceArea1Label; //d
  __weak IBOutlet UILabel *performanceArea2Label;
  __weak IBOutlet UILabel *description1Label;
  __weak IBOutlet UILabel *description2Label;
  __weak IBOutlet UILabel *chartInfoLabel;
  __weak IBOutlet UILabel *chartInfo1Label,*chartInfo2Label;
 __weak IBOutlet ProgressCircleView *progressCircleView;
  __weak IBOutlet UIView *botomLineView;
  __weak IBOutlet UIView *upLineView;
  __weak IBOutlet UILabel *maxChartValueLabel;
  __weak IBOutlet UILabel *recomendChartValueLabel;
  __weak IBOutlet UIView *RecomendValLineView;
  __weak IBOutlet UIView *Recomend2ValLineView;
  NSMutableArray *objectForRemoving;
  
}

-(void)setUI{
  [APPSTYLE applyStyle:@"Profile_Title_Label" toLabel:levelnfoLabel];//A
  [APPSTYLE applyStyle:@"Profile_Title_Label" toLabel:score1Label];//B
  
  [APPSTYLE applyStyle:@"Label_Profile_Title" toLabel:lessonInfo1Label];
  [APPSTYLE applyStyle:@"Label_Body" toLabel:lessonInfo2Label];
  
  [APPSTYLE applyStyle:@"Label_Profile_Title" toLabel:performanceArea1Label];
  [APPSTYLE applyStyle:@"Label_Body" toLabel:performanceArea2Label];
  
  [APPSTYLE applyStyle:@"Label_Profile_Title" toLabel:description1Label];
  [APPSTYLE applyStyle:@"Label_Body" toLabel:description2Label];
  
  [APPSTYLE applyStyle:@"Label_Profile_Title" toLabel:chartInfoLabel];
  
}
-(void)setData{
  [self setCharData];
  [self setLabels];
  
  /*UIView *activityIndicatorBacground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
  activityIndicatorBacground.userInteractionEnabled = YES;
  activityIndicatorBacground.backgroundColor = [UIColor whiteColor];
  //activityIndicatorBacground.alpha = 0.5;
  [self addSubview:activityIndicatorBacground];
  
  
  UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
  [self addSubview:activityIndicator];
  activityIndicator.transform = CGAffineTransformMakeScale(1.3, 1.3);
  activityIndicator.color = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
  activityIndicator.center = CGPointMake(self.frame.size.width / 2, [[UIScreen mainScreen] bounds].size.height / 2);
  [activityIndicator startAnimating];
  
  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
  dispatch_async(queue, ^{
    [self setCharData];
    [self setLabels];
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
    [activityIndicatorBacground removeFromSuperview];
  });
  */
}
-(void)setLabels{
  levelnfoLabel.text =[NSString stringWithFormat:Localized(@"T630"),[PROGRESS_MANAGER takeCurentLevelName],[PROGRESS_MANAGER  currentModule]];
  if([self youEarnDiplomaForThisLevel]){
    score1Label.text =[NSString stringWithFormat:Localized(@"T632"),[PROGRESS_MANAGER scoreForCurrentLevel]];
  }else{
    score1Label.text =[NSString stringWithFormat:Localized(@"T631"),[self morePointsForDiploma]];
  }
    
    
    
    NSInteger lastVisitedSection = [PROGRESS_MANAGER getLastVisitedSectionIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]];
    NSInteger lastVisitedUnit = [PROGRESS_MANAGER getLastVisitedUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]];

    NSDictionary *dictMaxValues = (NSDictionary *)[PROGRESS_MANAGER maxUnitIndexForLevel:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    NSInteger maxUnitIndex = [[dictMaxValues objectForKey:@"unitIndex"] integerValue];
    NSInteger maxModuleIndex = [[dictMaxValues objectForKey:@"modulIndex"] integerValue];
    
    if (lastVisitedSection <= maxModuleIndex && lastVisitedUnit <= maxUnitIndex) {
    
        lastVisitedUnit = maxUnitIndex;
        lastVisitedSection = maxModuleIndex;

    }
    
  
    NSInteger startLesson = [PROGRESS_MANAGER getTheNumberOfTheLesson:lastVisitedSection unit:lastVisitedUnit];
    NSInteger totalLesson = [self lessonInTheLevel];
    
    performanceArea2Label.numberOfLines = 0;
    
    if (startLesson < totalLesson) {
        lessonInfo1Label.text = [NSString stringWithFormat:Localized(@"T633"), startLesson, totalLesson];
        lessonInfo2Label.text = [NSString stringWithFormat:Localized(@"T634"), totalLesson-startLesson];
        performanceArea2Label.text = [NSString stringWithFormat:Localized(@"T636"), [self earnMoreStarsForDiploma], [PROGRESS_MANAGER takeCurentLevelName]];
        
        if([self earnMoreStarsForDiploma] == 0){
        
            performanceArea2Label.text =[NSString stringWithFormat:Localized(@"T636.1"),[PROGRESS_MANAGER takeCurentLevelName]];
        }

    }else {
        
        lessonInfo1Label.text = [NSString stringWithFormat:Localized(@"T633"),startLesson,totalLesson];
        lessonInfo2Label.numberOfLines = 0;
        lessonInfo2Label.text =[NSString stringWithFormat:Localized(@"T634.1"),[PROGRESS_MANAGER takeCurentLevelName]];
        [lessonInfo2Label sizeToFit];
        performanceArea2Label.text =[NSString stringWithFormat:Localized(@"T636.1"),[PROGRESS_MANAGER takeCurentLevelName]];
        if([self earnMoreStarsForDiploma] > 0){
           performanceArea2Label.text = [NSString stringWithFormat:Localized(@"T636"), [self earnMoreStarsForDiploma], [PROGRESS_MANAGER takeCurentLevelName]];  
        }
        //lessonInfo2Label .text = @"#$#$";
    }
    
    [performanceArea2Label sizeToFit];
    
    performanceArea1Label.text =[NSString stringWithFormat:Localized(@"T635"),[PROGRESS_MANAGER earnedStars]];
  
  
  //DLog(@"weekPracticeTime %d",(int)[PROGRESS_MANAGER weekPracticeTime]);
  description1Label.text =[NSString stringWithFormat:Localized(@"T637"),(int)[PROGRESS_MANAGER weekPracticeTime]];
  if([self minuteToRecommended] > 0){
    description2Label.text =[NSString stringWithFormat:Localized(@"T638"),[self minuteToRecommended],kParam041];
  }else{
    description2Label.text =[NSString stringWithFormat:Localized(@"T639")];
  }
  
  chartInfoLabel.text =[NSString stringWithFormat:Localized(@"T640"),(int)[self dailyAverageTime]];
  chartInfo1Label.text =[NSString stringWithFormat:Localized(@"T641")];
  chartInfo2Label.text =[NSString stringWithFormat:Localized(@"T642")];
  

  //float allCompleteLesson =  [PROGRESS_MANAGER numOfLessonToCompleteTheLevel:[PROGRESS_MANAGER currentLevelIndex]];
  //float allLesson = [self lessonInTheLevel];
  //float progressProcent = allCompleteLesson / allLesson ;
  
    
    /*float firstPart = (float)[self numOfStartLesson]*100.0/(float)[self lessonInTheLevel];
    float secondPart = 100.0-firstPart;

    if (_pie == nil) {
        _pie = [[PieChart alloc] initWithFrame:CGRectMake(8, 83, 48, 48)];
        [_pie setVal1:firstPart setVal2:secondPart];
        [_pie setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:_pie];
    }
    else {
        
        [_pie setVal1:firstPart setVal2:secondPart];

    }*/
  
}
-(void)setCharData{
  if([objectForRemoving count]){
    for (int i = 0; i<  [objectForRemoving count]; i++) {
      UIView *tempView = [objectForRemoving objectAtIndex:i];
      [tempView removeFromSuperview];
    }
  }
  objectForRemoving = [[NSMutableArray alloc]init];
 
  NSMutableArray *userUsageTimeArray = [PROGRESS_MANAGER.userStatistic.userUsageTimeArray mutableCopy];
 ///////////////
  
  CFTimeInterval elapsedTime = CACurrentMediaTime() - APP_DELEGATE.startTime;
  NSDateComponents *components = [[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
  int weekDay = (int)components.weekday - 1;
  if(weekDay == 0)  weekDay = 7;
  
  NSNumber *currentDayUsage =  [userUsageTimeArray objectAtIndex:weekDay];
  NSNumber *newDayValue = @([currentDayUsage integerValue]+elapsedTime);
  
  if(userUsageTimeArray) {
    [userUsageTimeArray replaceObjectAtIndex:weekDay withObject:newDayValue];
  }
  
  //NSLog(@"### userUsageTimeArray %@",userUsageTimeArray);
  ////////////////
  
  //NSNumber *min = [userUsageTimeArray valueForKeyPath:@"@min.self"];
  NSNumber *max = [userUsageTimeArray valueForKeyPath:@"@max.self"];
  
  NSInteger maxUsageTime= ((int)(10+ ([max integerValue]/D_MINUTE))/10)*10;
  if(maxUsageTime < 30) maxUsageTime = 30;
  
  float chartViewHeight = botomLineView.y - upLineView.y;
  float basePartUnit = chartViewHeight / maxUsageTime;
  
  maxChartValueLabel.text = [NSString stringWithFormat:@"%ld",(long)maxUsageTime];
  RecomendValLineView.y = -1.5+208-basePartUnit*10;
  recomendChartValueLabel.y = -recomendChartValueLabel.mHeight/2 + 208-basePartUnit*10;
  //draw line
  for(int i=1; i< 3; i++)
  {
    NSInteger tempValue = (maxUsageTime/3*i);
    
    UIView *lineView = [[UIView alloc] initWithFrame:RecomendValLineView.frame];
    lineView.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.560];
    lineView.y = 208-(basePartUnit*tempValue);
    lineView.height = 0.5;
    [chartView insertSubview:lineView atIndex:1];
    [objectForRemoving addObject:lineView];
    
    UILabel *usageLabel = [[UILabel alloc] initWithFrame:maxChartValueLabel.frame];
    usageLabel.text = [NSString stringWithFormat:@"%ld",(long)tempValue];
    usageLabel.y = -usageLabel.mHeight/2 + 208-(basePartUnit*tempValue);
    usageLabel.font = [UIFont systemFontOfSize:14];
    usageLabel.textAlignment  = NSTextAlignmentRight;
    [chartView addSubview:usageLabel];
    [objectForRemoving addObject:usageLabel];
  }
  
  //draw line
  /*static NSString *shortDayStrings[] = {
    @" ",
    @"Mon",
    @"Tue",
    @"Wed",
    @"Thu",
    @"Fri",
    @"Sat",
    @"Sun",
  };*/
  
  NSString *shortDayStrings[] = {
    @" ",
    Localized(@"T643.2"),
    Localized(@"T643.3"),
    Localized(@"T643.4"),
    Localized(@"T643.5"),
    Localized(@"T643.6"),
    Localized(@"T643.7"),
    Localized(@"T643.1")
  };
  for(int i=0; i< [userUsageTimeArray count]; i++)
  {
    
    NSInteger usageTime= [(NSNumber*)[userUsageTimeArray objectAtIndex:i] integerValue]/D_MINUTE;
    CGRect lineRect = CGRectMake(10+35*i, 208-basePartUnit*usageTime, 15, basePartUnit*usageTime);
    UIView *usageTimeView = [[UIView alloc] initWithFrame:lineRect];
    [usageTimeView setBackgroundColor:[APPSTYLE colorForType:@"Chart_Time_Progres_Color"]];
    [chartView insertSubview:usageTimeView atIndex:1];
    [objectForRemoving addObject:usageTimeView];
    if(i>0 && i<8)
    {
      UILabel *usageLabel = [[UILabel alloc] initWithFrame:CGRectMake(8+35*i, 208, 40, 30)];
      usageLabel.text = shortDayStrings [i];
      usageLabel.font = [UIFont systemFontOfSize:13];
      usageLabel.backgroundColor = [UIColor clearColor];
      [chartView addSubview:usageLabel];
      [objectForRemoving addObject:usageLabel];
    }
  }
}


-(BOOL)youEarnDiplomaForThisLevel{
  return [PROGRESS_MANAGER scoreForCurrentLevel] >= kParam043;
}

-(int)morePointsForDiploma{
  NSInteger scoreForCurrentLevel = [PROGRESS_MANAGER scoreForCurrentLevel];
  return (int)(kParam043  - scoreForCurrentLevel);
}

-(NSInteger)lessonInTheLevel{
  return [[DataService sharedInstance] numOfLessonInLevel:[PROGRESS_MANAGER currentLevelIndex]];
}

-(NSInteger)lessonToCompleteTheLevel{
  return  [self lessonInTheLevel] - [PROGRESS_MANAGER numOfLessonToCompleteTheLevel:[PROGRESS_MANAGER currentLevelIndex]];
}

-(NSInteger)numOfLessonToCompleteTheLevel{
  return  [PROGRESS_MANAGER numOfLessonToCompleteTheLevel:[PROGRESS_MANAGER currentLevelIndex]];
}
-(NSInteger)numOfStartLesson{
  return [PROGRESS_MANAGER numOfStartLesson:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
}
-(NSInteger)earnMoreStarsForDiploma{
  return [self lessonInTheLevel] - [PROGRESS_MANAGER earnedStars];
  //return 3;
}
-(int)minuteToRecommended{
  return (int)(kParam041  - (int)[PROGRESS_MANAGER weekPracticeTime]);
}

#pragma mark - weekPracticeTime

- (NSInteger)weekPracticeTime
{
  NSMutableArray *userUsageTimeArray = [PROGRESS_MANAGER.userStatistic.userUsageTimeArray mutableCopy];
  ///////////////
  
  CFTimeInterval elapsedTime = CACurrentMediaTime() - APP_DELEGATE.startTime;
  NSDateComponents *components = [[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
  int weekDay = (int)components.weekday - 1;
  if(weekDay == 0)  weekDay = 7;
  
  NSNumber *currentDayUsage =  [userUsageTimeArray objectAtIndex:weekDay];
  NSNumber *newDayValue = @([currentDayUsage integerValue]+elapsedTime);
  
  if(userUsageTimeArray) {
    [userUsageTimeArray replaceObjectAtIndex:weekDay withObject:newDayValue];
  }

  NSNumber *weekPracticeTime =[userUsageTimeArray valueForKeyPath:@"@sum.self"];
  return [weekPracticeTime intValue] / D_MINUTE;
}


#pragma mark - dailyAverageTime

- (NSInteger)dailyAverageTime
{
  return [self weekPracticeTime] / 7;
}


@end
