//
//  AchievementsViewController.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 2/6/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "LMBaseViewController.h"
#import "AchievementsTableViewCell.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface AchievementsViewController : LMBaseViewController<UITableViewDelegate,UITableViewDataSource,AchievementsTableViewCellDelegate>

- (void)setData;

@end
