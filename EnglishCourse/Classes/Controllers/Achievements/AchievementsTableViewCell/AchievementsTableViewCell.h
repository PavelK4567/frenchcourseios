//
//  AchievementsTableViewCell.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 2/9/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AchievementsObject.h"

@protocol AchievementsTableViewCellDelegate <NSObject>

@required

- (void)pressFBButton:(NSString *)fbLink;
- (void)pressMailButton:(NSInteger)levelId;

@end


@interface AchievementsTableViewCell : UITableViewCell

@property (nonatomic, weak) id<AchievementsTableViewCellDelegate> cellDelegate;

-(void)setTextToLabel:(NSString *)text;
-(void)setData:(AchievementsObject *)currentAchievementsObject;

@end
