//
//  UserDetailsViewController.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 2/20/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "UserDetailsViewController.h"


@implementation UserDetailsViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
 
}

#pragma mark - viewDidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadGUI];
}


#pragma mark - loadGUI

- (void)loadGUI
{
    [self bindGUI];
    [self layoutGUI];
    [self getDiplomaDetails];
}


#pragma mark - bindGUI

- (void)bindGUI
{
    [continueButton.layer setCornerRadius:5];
    [firstnameLabel setText:Localized(@"T586")];
    [familyNameLabel setText:Localized(@"T587")];
    [emailLabel setText:Localized(@"T591")];
    [titleLabel setText:Localized(@"T581.1")];
    [descriptionLabel setText:Localized(@"T582")];
    [continueButton setTitle:Localized(@"T588") forState:UIControlStateNormal];
    [mainTitle setText:Localized(@"T581")];
}


#pragma mark - layoutGUI

- (void)layoutGUI
{
    [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:firstNameText];
    [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:familyNameText];
    [APPSTYLE applyStyle:@"Login_Text_Field" toTextField:emailText];
}


#pragma mark - textFieldShouldReturn

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    UIView *view = [self.view viewWithTag:textField.tag + 1];
    if (!view)
        [textField resignFirstResponder];
    else
        [view becomeFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}


#pragma mark - closeAction

- (IBAction)closeAction:(id)sender
{
    [self.userDetailsDelegate closeLevelCompletition];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)continueButton:(id)sender
{
    [self validateText:firstNameText];
    [self validateText:familyNameText];
    [self validateText:emailText];
    
    if([self validateText:firstNameText] && [self validateText:familyNameText] && [self validateText:emailText]) {
    
        ConfirmationDialog *confirmation = [ConfirmationDialog getFromNib];
        
        [self.view endEditing:YES];
        
        if(!confirmation.isShown) {
            [confirmation setEmail:emailText.text];
            [confirmation setName:firstNameText.text];
            [confirmation setPrefix:prefixText.text];
            [confirmation setFamilyName:familyNameText.text];
            [confirmation setDelegate:self];
            [self.view addSubview:confirmation];
            [confirmation bindGUI];
            [confirmation showAnimated:YES];
            [confirmation setLevelId:self.levelId];
            [confirmation setResend:self.resend];
        }
    }
    
}


#pragma mark - confirmation screen delegate

- (void)isApproved:(BOOL)status
{
    if(status) {
    
        [firstNameText setEnabled:NO];
        [familyNameText setEnabled:NO];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}


#pragma mark - getDiplomaDetails

- (void)getDiplomaDetails
{

    [API_REQUEST_MANAGER diplomaDetailsForUser:[USER_MANAGER userMsisdnString]
                                       groupID:[REQUEST_MANAGER groupID]
                                     contentID:[REQUEST_MANAGER contentID]
                               CompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSDictionary *responseDict)
    {
       
        if([responseDict objectForKey:@"statis"] == 0) {
            
            [emailText setText:(![[responseDict objectForKey:@"email"] isEqual:[NSNull null]]) ? [responseDict objectForKey:@"email"] : @""];
            [familyNameText setText:(![[responseDict objectForKey:@"lastName"] isEqual:[NSNull null]])  ? [responseDict objectForKey:@"lastName"] : @""];
            [firstNameText setText:(![[responseDict objectForKey:@"firstName"] isEqual:[NSNull null]])  ? [responseDict objectForKey:@"firstName"] : @""];

            if(![familyNameText.text isEqualToString:@""] && ![firstNameText.text isEqualToString:@""] ) {
                [familyNameText setEnabled:NO];
                [firstNameText setEnabled:NO];
            }
            
            if([emailText.text isEqualToString:@""]) {
                self.resend = NO;
            }
            else {
                self.resend = YES;
            }
        }
        
    }];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField != emailText) {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
    
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 20) ? NO : YES;
    }
    else
        return YES;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self validateText:textField];
}

- (BOOL)validateText:(UITextField *)text
{
    BOOL status = NO;
    
    if([text.text isEqualToString:@""]) {
        [text.layer setBorderColor: [[UIColor redColor] CGColor]];
        [text.layer setBorderWidth: 1.0];
        [text.layer setCornerRadius:5.0f];
        [text.layer setMasksToBounds:YES];
        
        if(text == firstNameText)
            [firstnameLabel setTextColor:[UIColor redColor]];
        else if(text == familyNameText)
            [familyNameLabel setTextColor:[UIColor redColor]];
        else
            [emailLabel setTextColor:[UIColor redColor]];
    }
    else
    {
        
        [text.layer setBorderColor:(__bridge CGColorRef)([UIColor clearColor])];
        [text.layer setBorderWidth:0.5];
        [text.layer setCornerRadius:5.0f];
        [text.layer setMasksToBounds:YES];
        
        if(text == firstNameText)
            [firstnameLabel setTextColor:[UIColor blackColor]];
        else if(text == familyNameText)
            [familyNameLabel setTextColor:[UIColor blackColor]];
        else {
            [emailLabel setTextColor:[UIColor blackColor]];
        }
        
        status = YES;
    }
    
    if(text == emailText && ![LMHelper isMailValid:text.text] && ![text.text isEqualToString:@""]) {
    
        [mailLabel setText:Localized(@"T590")];
        [mailLabel setTextColor:[UIColor redColor]];
        [mailLabel sizeToFit];
        [mailLabel setHidden:NO];
        [emailText.layer setBorderColor: [[UIColor redColor] CGColor]];
        [emailText.layer setBorderWidth: 1.0];
        [emailText.layer setCornerRadius:5.0f];
        [emailText.layer setMasksToBounds:YES];
        [emailLabel setTextColor:[UIColor redColor]];
        status = NO;
    }
    
    else {

        [mailLabel setHidden:YES];
    }
    
    return status;
}

#pragma mark -
@end
