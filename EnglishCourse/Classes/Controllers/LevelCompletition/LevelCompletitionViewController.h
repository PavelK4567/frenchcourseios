//
//  LevelCompletitionViewController.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 2/18/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "UserDetailsViewController.h"

@protocol PracticeMoreDelegate <NSObject>
- (void)practiceMoreInLevel:(NSInteger)level
                    section:(NSInteger)section
                       unit:(NSInteger)unit;
@end

@interface LevelCompletitionViewController : LMBaseViewController<UserDetailsDelegate>
{

    __weak IBOutlet UILabel *titleViewLabel;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UIImageView *diplomaImageView;
    __weak IBOutlet UILabel *diplomaTextLabel;
    __weak IBOutlet UIButton *facebookButton;
    __weak IBOutlet UIButton *createDiplomaButton;
    __weak IBOutlet UIButton *practiceModeButton;
    __weak IBOutlet UIButton *nextLevelButton;
    __weak IBOutlet UIView *titleView;
    __weak IBOutlet UIScrollView *containerScrollView;
}

@property (nonatomic,strong) NSString             *diplomaText;
@property (nonatomic       ) NSInteger            numberOfLessons;
@property (nonatomic       ) NSInteger            diplomaPoints;
@property (nonatomic,strong) NSString             *modulName;
@property (nonatomic, weak ) id <PracticeMoreDelegate> delegate;
@property (nonatomic       ) BOOL                 isVisited;

@end
