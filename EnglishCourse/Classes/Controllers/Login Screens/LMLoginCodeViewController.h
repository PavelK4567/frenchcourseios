//  LMLoginCodeViewController.h
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


#import "LMBaseViewController.h"


@interface LMLoginCodeViewController : LMBaseViewController

@property(strong, nonatomic) NSString *msisdn;

@end
