//
//  LMBubbleView.m
//  LMBubbledScrollView
//
//  Created by Zarko Popovski on 1/9/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "LMBubbleView.h"

@interface LMBubbleView (Private)

- (void) setupInternalData;

@end

@implementation LMBubbleView

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
}

- (void)setDataInternal:(NSBubbleData *)value
{
    self.data = value;
    [self setupInternalData];
}


- (void) setupInternalData
{
    
    if (!self.bubbleImage)
    {
        
        self.bubbleImage = [[UIImageView alloc] init];
        
        [self addSubview:self.bubbleImage];
    }
    
    NSBubbleType type = self.data.type;
    
    CGFloat width = self.data.view.frame.size.width;
    CGFloat height = self.data.view.frame.size.height;
    
    CGFloat x = (type == BubbleTypeSomeoneElse) ? 0 : self.frame.size.width - width - self.data.insets.left - self.data.insets.right;
    CGFloat y = 0;
    
    // Adjusting the x coordinate for avatar
    if (self.showAvatar)
    {
        [self.avatarImage removeFromSuperview];
        
        self.avatarImage = [[UIImageView alloc] initWithImage:(self.data.avatar ? self.data.avatar : [UIImage imageNamed:@"missingAvatar.png"])];
        
        self.avatarImage.layer.masksToBounds = YES;
        
        CGFloat avatarX = (type == BubbleTypeSomeoneElse) ? 6 : 268;
        CGFloat avatarY = (self.bubbleType == BUBBLE_SUMMARY)?0:(self.data.view.frame.size.height - 40);
        
        self.avatarImage.frame = CGRectMake(avatarX, avatarY, 51, 51);
        self.avatarImage.layer.cornerRadius = self.avatarImage.frame.size.width / 2;
        
        [self addSubview:self.avatarImage];
        
        CGFloat delta = self.frame.size.height - (self.data.insets.top + self.data.insets.bottom + self.data.view.frame.size.height);
        if (delta > 0) y = delta;
        
        if (type == BubbleTypeSomeoneElse) x += 54;
        if (type == BubbleTypeMine) x -= 54;
    }
    
    [self.customView removeFromSuperview];
    self.customView = self.data.view;
    self.customView.frame = CGRectMake(x + 5 + self.data.insets.left, y + self.data.insets.top, width, height);
    [self addSubview:self.customView];
    
    if (type == BubbleTypeSomeoneElse)
    {
        self.bubbleImage.image = [[UIImage imageWithImage:[UIImage imageNamed:@"img_bubble.png"] color:[UIColor colorFromHexString:self.data.colorInHEX]] stretchableImageWithLeftCapWidth:24 topCapHeight:20];
    
    }
    else {
        
        if (self.bubbleType == BUBBLE_SUMMARY) {
            self.bubbleImage.image = [[UIImage imageWithImage:[UIImage imageNamed:@"bubble_white.png"] color:[UIColor colorFromHexString:self.data.colorInHEX]] stretchableImageWithLeftCapWidth:24 topCapHeight:30];

            self.arrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(240, 8, 21, 28)];
            self.arrowImage.image = [UIImage imageWithImage:[UIImage imageNamed:@"img_arrow.png"] color:[UIColor colorFromHexString:self.data.colorInHEX]];
            
        } else {
            self.bubbleImage.image = [[UIImage imageWithImage:[UIImage imageNamed:@"img_bubble_r.png"] color:[UIColor colorFromHexString:self.data.colorInHEX]] stretchableImageWithLeftCapWidth:24 topCapHeight:20];
        }
        
        CGRect frame = self.customView.frame;
        frame.origin.x = 20;
        
        self.customView.frame = frame;
        
    }
    
    self.isSelectable = (self.data.answerObjectsArray != nil)?NO:YES;
    
    self.bubbleImage.frame = CGRectMake((type == BubbleTypeMine)?5:x, y, width + self.data.insets.left + self.data.insets.right, height + self.data.insets.top + (self.data.insets.bottom / 2));
    
    if (self.bubbleType == BUBBLE_SUMMARY)
    {
        [self addSubview:self.arrowImage];
    }
    
    self.data.btnProfileSound.y = (self.mHeight / 2) - (self.data.btnProfileSound.mHeight / 2);
    
    if (self.data.audio && self.tag > 0 && (!self.data.answerObjectsArray || [self.data.answerObjectsArray count] == 0)) {
        if (!self.drawAnswered) {
            [AUDIO_MANAGER setDelegate:self];
            
            [AUDIO_MANAGER playAudioFromDocumentPath:[self.data audio] componentSender:self.data.btnProfileSound];
        }
    }
    
    self.isExecuted = YES;
    
}

- (void)resizeToSize:(CGFloat)height
{
    [self.bubbleImage setHeight:(height + self.data.insets.top + self.data.insets.bottom)];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    NSLog(@"Class - %@, answerObjectsArray - %ld, isAnswered - %d", [touch.view class], self.data.answerObjectsArray.count, !self.data.isAnswered);
    if (![touch.view isKindOfClass:[LMNumberedMarkingLabel class]] && ([self.data.answerObjectsArray count]>1 && !self.data.isAnswered)) {
        return;
    }
    
    if (self.isSelectable && self.data.chatType == PROGRESSIVE_CHAT && !self.isExecuted) {
        [self.delegate didSelectViewWithFrame:self.frame];
    } else if (self.isSelectable && self.data.chatType == PROGRESSIVE_CHAT && self.isExecuted) {
        [AUDIO_MANAGER playAudioFromDocumentPath:[self.data audio] componentSender:self.data.btnProfileSound];
    } else {
        if (self.data.chatType == PROGRESSIVE_CHAT) {
            if(self.customView && self.customView.subviews){
                NSArray *subViews = self.customView.subviews;
                LMNumberedMarkingLabel *lmNumberedView = subViews[0];
                
                [AUDIO_MANAGER playAudioFromDocumentPath:[self.data audio] componentSender:lmNumberedView.imgSoundBtn];
            }
            
        }
    }
    
}

#pragma mark - LMAudioManager implementation

- (void)playbackComplete
{
    self.isExecuted = YES;
    
    if (self.data.answerObjectsArray == nil) {
        [self.delegate didSelectViewWithFrame:self.frame];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
