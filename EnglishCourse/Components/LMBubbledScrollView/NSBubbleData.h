//
//  NSBubbleData.h
//  LMBubbledScrollView
//
//  Created by Zarko Popovski on 1/9/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LMEnums.h"

#import "LMNumberedMarkingLabel.h"
#import "LMAudioManager.h"

typedef enum _NSBubbleType
{
    BubbleTypeMine = 0,
    BubbleTypeSomeoneElse = 1
} NSBubbleType;

@protocol NSBubbleDataDelegate <NSObject>

- (void)playSoundFromObjectAtIndex:(NSInteger)index;
- (void)playSoundFromObjectAtIndex:(NSInteger)index sender:(id)sender;
- (void)resizeAnswersCellWithHeight:(NSInteger)cellIndex cellHeight:(NSInteger)cellHeight;
- (void)resizeAnswersCellWithHeight:(NSInteger)cellHeight;
- (void)drawNextBubble;

@optional

- (void)returnPointsForCorrentAnswer:(NSInteger)points isLastAnswer:(BOOL)isLastAnswer;
- (void)savePointsForInstance;

@end

@interface NSBubbleData : NSObject<LMNumberedMarkingLabelDelegate, LMAudioManagerDelegate>

@property (readonly, nonatomic, strong) NSDate *date;
@property (readonly, nonatomic) NSBubbleType type;
@property (nonatomic, assign) IconPosition iconPosition;
@property (nonatomic, assign) ChatType chatType;
@property (readonly, nonatomic, strong) UIView *view;
@property (readonly, nonatomic) UIEdgeInsets insets;
@property (nonatomic, assign) BOOL showAvatar;
@property (nonatomic, strong) UIImage *avatar;

@property (nonatomic, assign) NSInteger indexPosition;
@property (nonatomic, strong) NSString *colorInHEX;

@property (nonatomic, strong) NSArray *answerObjectsArray;

@property (nonatomic, assign) BOOL isAnswer;
@property (nonatomic, strong) NSString *audio;

@property (nonatomic) BOOL isCellViewVisible;

@property (nonatomic, assign) NSInteger guessingAnswerCounter;
@property (nonatomic, assign) NSInteger points;

@property (nonatomic, weak) id<NSBubbleDataDelegate>delegate;

@property (nonatomic, assign) BOOL isExecuted;

@property (nonatomic, assign) BOOL isAnswered;

@property (nonatomic, assign) BOOL drawAnswered;

@property (nonatomic, assign) NSInteger characterID;

@property (nonatomic, assign)  BOOL isLastAnswer;

@property (nonatomic, strong) UIImageView *btnProfileSound;

- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type;
- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type index:(NSInteger)index chatType:(ChatType)chatType answersArray:(NSArray *)answersArray audioSample:(NSString *)audioSample drawAnswered:(BOOL)drawAnswered;
+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type;
+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type index:(NSInteger)index chatType:(ChatType)chatType answersArray:(NSArray *)answersArray audioSample:(NSString *)audioSample drawAnswered:(BOOL)drawAnswered;
- (id)initWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type;
+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type;
- (id)initWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets;
+ (id)dataWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets;

- (void)playSoundAtIndex:(NSInteger)index;

@end
