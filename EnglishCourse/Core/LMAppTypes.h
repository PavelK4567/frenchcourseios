//  LMAppTypes.h
//  Created by Kiril Kiroski on 5/5/14.
//  Copyright (c) 2014 LaMark. All rights reserved.


/**********************************************************************************************************************
 STYLE
 **********************************************************************************************************************/

/*
 userStatus, with the following values:
 8 – Not registered user
 3 – Active
 4 – Disabled
 5 – Canceled
 6 – To be canceled
 */
typedef enum {
  KAUserGuest = -1,
	KAUserStatusNotRegistered = 8,
	KAUserStatusActive = 3,
	KAUserStatusDisabled = 4,
	KAUserStatusCanceled = 5,
	KAUserStatusToBeCanceled = 6,
} KAUserStatus;


/*
 Response will return the following attributes:
 status, 0 – success, 1 – Invalid token, or some other error,100 - tehnical error, 101 - invalid parametars
 */

typedef enum {
	KAApiTokenStatusSuccess = 0,
	KAApiInvalidToken = 1,
} KAApiTokenStatus;
