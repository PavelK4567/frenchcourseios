//  NSObject+Extras.m
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "NSObject+Extras.h"
#import <objc/runtime.h>


static VoidBlock _block;


@implementation NSObject (Extras)

static UINib *__nib;
static NSString *__nibName;


+ (id)getFromNibNamed:(NSString *)nibName {
  if (![nibName isEqualToString:__nibName]) {
    __nibName = nibName;
    __nib = [UINib nibWithNibName:nibName bundle:nil];
  }

  return [__nib instantiateWithOwner:nil options:nil][0];
}

+ (id)getFromNib {
  return [self getFromNibNamed:NSStringFromClass(self)];
}

- (void)performBlock:(VoidBlock)aBlock {
  _block = [aBlock copy];

  [self performSelector:@selector(callBlock)];
}

- (void)performBlock:(VoidBlock)aBlock afterDelay:(NSTimeInterval)delay {
  _block = [aBlock copy];

  [self performSelector:@selector(callBlock) withObject:nil afterDelay:delay];
}

- (void)callBlock {
  _block();
  _block = nil;
}

@end
