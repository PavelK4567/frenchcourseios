//
//  NSString+Encode.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 3/30/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "NSString+Encode.h"

@implementation NSString (Encode)
- (NSString *)encodeString:(NSStringEncoding)encoding
{
    return (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)self,
                                                                NULL, (CFStringRef)@";/?:@&=$+{}<>,",
                                                                CFStringConvertNSStringEncodingToEncoding(encoding)));
}
@end
