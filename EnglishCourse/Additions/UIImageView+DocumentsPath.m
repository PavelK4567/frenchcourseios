//
//  UIImageView+DocumentsPath.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/12/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "UIImageView+DocumentsPath.h"

@implementation UIImageView (DocumentsPath)

- (void)setImageFromDocumentsResourceFile:(NSString *)fileName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    NSData *dataFromFile = [[NSFileManager defaultManager] contentsAtPath:dataPath];
    
    self.image = [UIImage imageWithData:dataFromFile];

}

@end
