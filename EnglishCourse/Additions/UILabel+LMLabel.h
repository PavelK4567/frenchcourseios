//
//  UILabel+LMLabel.h
//  MissingWord
//
//  Created by Darko Trpevski on 12/15/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (LMLabel)

- (void)adjustHeightWithMaxHeight:(CGFloat)maxHeight;
- (void)adjustHeightToFit;

@end
