//  NSDate+Extras.h
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


@interface NSDate (Extras)

+ (NSString *)timeAgo:(NSDate *)referentDate;

+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)theFormat;
+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)theFormat;
+ (NSString *)stringFromDateAutoupdatingCurrentLocale:(NSDate *)date;

- (NSDate *)dateToBeginningOfDay;
- (NSDate *)dateToEndOfDay;
- (BOOL)beforeDate:(NSDate *)date;
- (BOOL)beforeOrEqualToDate:(NSDate *)date;
- (BOOL)afterDate:(NSDate *)date;
- (BOOL)afterOrEqualToDate:(NSDate *)date;
- (NSString *)stringWithFormat:(NSString *)format;

+ (NSDateFormatter *)fullFormatter;
+ (NSDateFormatter *)dateFormatter;
+ (NSDateFormatter *)timeFormatter;

@end
