//
//  UIView+K1000Shadow.m
//  K1000
//
//  Created by Action Item on 4/13/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "UIView+K1000Shadow.h"

@implementation UIView (K1000Shadow)

- (void)addK1000Shadow
{
    self.layer.shadowColor = [[UIColor grayColor] colorWithAlphaComponent:0.7f].CGColor;
    self.layer.shadowOffset = CGSizeMake(1, 2);
    self.layer.shadowOpacity = 0.75;
    self.layer.shadowRadius = 0;
}
- (void)addVehicleBaseShadow
{
  self.layer.shadowColor = [[UIColor grayColor] colorWithAlphaComponent:0.7f].CGColor;
  self.layer.shadowOffset = CGSizeMake(0, 2);
  self.layer.shadowOpacity = 0.75;
  self.layer.shadowRadius = 0;
}

@end
