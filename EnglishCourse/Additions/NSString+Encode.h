//
//  NSString+Encode.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 3/30/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encode)

- (NSString *)encodeString:(NSStringEncoding)encoding;

@end
