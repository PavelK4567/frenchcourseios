//
//  LMHelper.m
//  MissingWord
//
//  Created by Darko Trpevski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMHelper.h"


@implementation LMHelper

#pragma mark - isWordMatchingPattern

+ (BOOL)isWordMatchingPattern:(NSString *)word
                      pattern:(NSString *)pattern
{
 
    NSPredicate *matchingExpression = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    
    if ([matchingExpression evaluateWithObject: word]) {
        return YES;
    }
    else {
        return NO;
    }
}


#pragma mark - getSubstringsThatMatchPattern

+ (NSArray *)getSubstringsThatMatchPattern:(NSString *)pattern
                            originalString:(NSString *)originalString
{
    NSMutableArray *arrayOfSubstring = [NSMutableArray new];
    
    NSError *error = nil;
    NSString *string = originalString;
    NSRange range = NSMakeRange(0, string.length);
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    /** TODO:Refactor this part of code **/
    NSArray *matches = [regex matchesInString:string options:NSMatchingProgress range:range];
    
    
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:0];
        NSString *word = [string substringWithRange:wordRange];
        [arrayOfSubstring addObject:word];
    }
    
    return arrayOfSubstring;
}


#pragma mark - splitQuestionIntoWords

+ (NSArray *)splitQuestionIntoWords:(NSString *)question
{
    NSArray *words = [self getSubstringsThatMatchPattern:kWordPattern originalString:question];
    NSMutableArray *allWords = [NSMutableArray new];
    
    NSString *modifiedString = question;
    
    for (NSString *word in words) {
        
        NSRange range = [modifiedString rangeOfString:word];
        if(range.location != NSNotFound) {
            NSString *lastComponent = [modifiedString substringToIndex:(range.location)];
            [allWords addObject:lastComponent];
            [allWords addObject:word];
            NSRange wordRange = range;
            wordRange.location = 0;
            wordRange.length = range.location + range.length;
            modifiedString = [modifiedString stringByReplacingCharactersInRange:wordRange withString:@""];
            
        }
    }
    
    [allWords addObject:modifiedString];
    
    NSMutableArray *splitArray = [NSMutableArray new];
    
    for(NSString *word in allWords) {
        
        if(![LMHelper isWordMatchingPattern:word pattern:kWordPattern]) {
            NSArray *words = [word componentsSeparatedByString:@" "];
            [splitArray addObjectsFromArray:words];
        }
        else
            [splitArray addObject:word];
    }
    
    return splitArray;
}


#pragma mark - placeholdersWords

+ (NSArray *)placeholdersWords:(NSArray *)words {

    NSMutableArray *placeholderList = [NSMutableArray new];
    for (NSString *word in words) {
        
        if([LMHelper isWordMatchingPattern:word pattern:kWordPattern]) {
            [placeholderList addObject:kUnderlineWord];
        }
        else {
            [placeholderList addObject:word];
        }
            
    }
    return placeholderList;
}


#pragma mark - removeChar

+ (NSString *)removeChar:(NSString *)charString
              fromString:(NSString *)word
{
    return [word stringByReplacingOccurrencesOfString:charString withString:@""];
}

#pragma mark - replce special chars from string

+ (NSString *)removeSpecialChars:(NSString *)string
{
    NSString *modifiedString = string;
    modifiedString = [modifiedString stringByReplacingOccurrencesOfString:@"<" withString:@" "];
    modifiedString = [modifiedString stringByReplacingOccurrencesOfString:@">" withString:@" "];
    return modifiedString;
}



#pragma mark - removeCharsFromString

+ (NSString *)removeCharsFromString:(NSString *)word
{
    return [self removeChar:@"<" fromString:[self removeChar:@">" fromString:word]];
}


+ (UIButton *)createButtonWithTitle:(NSString *)title
                     backgroundColor:(UIColor *)color
                          fontColor:(UIColor *)fontColor
                     highlightColor:(UIColor *)highlightColor
                 highlightFontColor:(UIColor *)highlightFontColor
                         onPosition:(CGFloat)position
{
    
    
    
    // Get your image somehow
    UIImage *image = [UIImage imageWithColor:color];
    UIImage *imagePress = [UIImage imageWithColor:highlightColor];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateHighlighted];
    [button setTitle:title forState:UIControlStateSelected];
    [button sizeToFit];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(kZeroPointValue, kButtonInnerPadding, kZeroPointValue, kButtonInnerPadding)];
    [button setX:position];

    [button setTitleColor:color forState:UIControlStateNormal];

    [button setBackgroundImage:[UIImage imageWithColor:highlightColor] forState:UIControlStateHighlighted];
    [button setTitleColor:highlightFontColor forState:UIControlStateHighlighted];
    
    [button setBackgroundImage:[UIImage imageWithColor:highlightColor] forState:UIControlStateSelected];
    [button setTitleColor:highlightFontColor forState:UIControlStateSelected];
    
    button.width = button.mWidth + (2 * kButtonInnerPadding);
    button.height = kMissingWordButtonHeight - 2;

    [button setTintColor:color];
    button.layer.shadowRadius = 0;

    button.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    button.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    button.layer.shadowOpacity = 0.2;
    button.layer.cornerRadius  = 10;
    

    UIGraphicsBeginImageContextWithOptions(button.bounds.size, NO, [UIScreen mainScreen].scale);
    [[UIBezierPath bezierPathWithRoundedRect:button.bounds
                                cornerRadius:3] addClip];
    [image drawInRect:button.bounds];
    [button setBackgroundImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    UIGraphicsEndImageContext();


    UIGraphicsBeginImageContextWithOptions(button.bounds.size, NO, [UIScreen mainScreen].scale);
    [[UIBezierPath bezierPathWithRoundedRect:button.bounds
                                cornerRadius:3] addClip];
    [imagePress drawInRect:button.bounds];
    [button setBackgroundImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateHighlighted];
    [button setTitleColor:color forState:UIControlStateNormal];
    UIGraphicsEndImageContext();
    return button;
}


#pragma mark - createButtonCopy

+ (UIButton *)createButtonCopy:(UIButton *)button
{
    NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:button];
    UIButton *buttonCopy = [NSKeyedUnarchiver unarchiveObjectWithData: archivedData];
    buttonCopy.layer.shadowRadius = 0;
    buttonCopy.layer.shadowColor = [UIColor grayColor].CGColor;
    buttonCopy.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    buttonCopy.layer.shadowOpacity = 0.9;
    buttonCopy.layer.masksToBounds = NO;
    buttonCopy.layer.cornerRadius = 10;
    return buttonCopy;
}


#pragma mark - createButtonViewCopy

+ (LMFlowTagView *)createButtonViewCopy:(LMFlowTagView *)button
{
    NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:button];
    LMFlowTagView *buttonCopy = [NSKeyedUnarchiver unarchiveObjectWithData: archivedData];
    [[buttonCopy layer] setBorderWidth:1.0f];
    [[buttonCopy layer] setBorderColor:[UIColor blackColor].CGColor];
    [buttonCopy.layer setCornerRadius:kMissingWordButtonCornerRadius];
   
    return buttonCopy;
}


+ (NSMutableArray *)shuffleArray:(NSMutableArray *)array
{
    
    NSMutableArray *randomArray = [NSMutableArray arrayWithArray:array];
    
    NSInteger count = randomArray.count * 5;
    
    for (int i = 0; i < count; i++) {
        
        int index1 = i % [randomArray count];
        int index2 = arc4random() % [randomArray count];
        
        if (index1 != index2) {
            [randomArray exchangeObjectAtIndex:index1 withObjectAtIndex:index2];
        }
        
    }
    
    return randomArray;
}


#pragma mark - saveUserInfo

+ (void)saveUserInfo:(NSString *)firstName
          secondName:(NSString *)secondName
               email:(NSString *)email
{
    NSArray *values        = [NSArray arrayWithObjects:[USER_MANAGER userMsisdn],firstName,secondName,email, nil];
    NSArray *keys          = [NSArray arrayWithObjects:@"msisdn",@"firstName",@"lastName",@"email", nil];
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:values forKeys:keys];

    NSData *userData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"userinfo"];
    NSMutableArray *userArray = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    
    if(userArray.count > 0) {
        
        int userIndex = -1;
        for (int index = 0; index < userArray.count; index++) {
        
            NSDictionary *user = (NSDictionary *)userArray[index];
            if([[user valueForKey:@"msisdn"] isEqualToString:[USER_MANAGER userMsisdn]]) {
                userIndex = index;
            }
        }

        if(userIndex != -1) {
    
            [userArray replaceObjectAtIndex:userIndex withObject:userInfo];
        }
        else {
            [userArray addObject:userInfo];
        }
    
    }
    else {
        userArray = [NSMutableArray new];
        [userArray addObject:userInfo];
    }

    NSData *statisticData = [NSKeyedArchiver archivedDataWithRootObject:userArray];
   [[NSUserDefaults standardUserDefaults] setObject:statisticData forKey:@"userinfo"];
   [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - getUserInfo

+ (NSDictionary *)getUserInfo:(NSString *)msisdn
{
    NSData *userData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"userinfo"];
    NSMutableArray *userArray = [NSKeyedUnarchiver unarchiveObjectWithData:userData];

    for (NSDictionary *user in userArray) {
        if([[user objectForKey:@"msisdn"] isEqualToString:msisdn]) return user;
    }

    return nil;
}


#pragma mark - getUsersInfo

- (NSArray *)getUsersInfo
{
    NSData *userData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"userinfo"];
    NSMutableArray *userArray = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    
    return userArray;
}

#pragma mark - Validate email


+ (BOOL)isMailValid:(NSString *)email
{
    NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegEx];
    return [regExPredicate evaluateWithObject:[email lowercaseString]];
}



#pragma mark - saveModulCertificate


+ (void)saveModulCertificate:(NSInteger)modul
                       level:(NSInteger)level
                        user:(NSString *)user
{
 
    if(![self isModuleSaved:user modul:modul level:level]) {
       
        NSData *modulData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"modulsList"];
        NSMutableArray *modulArray = [NSKeyedUnarchiver unarchiveObjectWithData:modulData];
        NSString *record           = [NSString stringWithFormat:@"%@#%ld#%ld",user,(long)modul,(long)level];
    
        if(!modulArray){
            modulArray = [[NSMutableArray alloc] init];
        }
        
        [modulArray addObject:record];
        
        NSData *statisticData = [NSKeyedArchiver archivedDataWithRootObject:modulArray];
        [[NSUserDefaults standardUserDefaults] setObject:statisticData forKey:@"modulsList"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}


#pragma mark - isModuleSaved

+ (BOOL)isModuleSaved:(NSString *)user
                modul:(NSInteger)modul
                level:(NSInteger)level
{
    
    NSData *modulData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"modulsList"];
    NSMutableArray *modulArray = [NSKeyedUnarchiver unarchiveObjectWithData:modulData];
    
    for (NSString *modulString in modulArray) {
        
        NSString *checkString = [NSString stringWithFormat:@"%@#%ld#%ld",user,(long)modul,(long)level];
        if([modulString isEqualToString:checkString])
            return YES;
    }
    return NO;
}


#pragma mark - levelCompletition

+ (void)saveLevelCompletition:(NSString *)user
                         type:(NSInteger)type
                        level:(NSInteger)level

{
    
    if(![self isLevelCompletitionSaved:user type:type level:level]) {
        
        NSData *levelData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"levelcompletition"];
        NSMutableArray *levelArray = [NSKeyedUnarchiver unarchiveObjectWithData:levelData];
        NSString *record           = [NSString stringWithFormat:@"%@#%ld#%ld",user,(long)type,(long)level];
        
        if(!levelArray){
            levelArray = [[NSMutableArray alloc] init];
        }
        
       [levelArray addObject:record];
        
        NSData *statisticData = [NSKeyedArchiver archivedDataWithRootObject:levelArray];
        [[NSUserDefaults standardUserDefaults] setObject:statisticData forKey:@"levelcompletition"];
        [[NSUserDefaults standardUserDefaults] synchronize];
   }
}


#pragma mark - isLevelCompletitionSaved

+ (BOOL)isLevelCompletitionSaved:(NSString *)user
                            type:(NSInteger)type
                           level:(NSInteger)level
{
    
    NSData *levelData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"levelcompletition"];
    NSMutableArray *levelArray = [NSKeyedUnarchiver unarchiveObjectWithData:levelData];
    
    for (NSString *levelString in levelArray) {
        
        NSString *checkString = [NSString stringWithFormat:@"%@#%ld#%ld",user,(long)type,(long)level];
        if([levelString isEqualToString:checkString])
            return YES;
    }
    
    return NO;
}

#pragma mark -


+ (NSInteger)getNumberOfGestCredits
{

    DataModel *model = DATA_SERVICE.dataModel;
    
    if(model.courseData.unitsCreditsGuest)
        return [model.courseData.unitsCreditsGuest integerValue];
    else return 1;
}

@end
