//  UILabel+Extras.m
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "UILabel+Extras.h"
#import "LMAppStyle.h"


@implementation UILabel (Extras)

#pragma mark -

+ (void)decreaseHeightOfLabel:(UILabel *)label {
  CGRect frame;
  frame = label.frame;
  CGFloat heightThatFits = [label sizeThatFits:CGSizeMake(frame.size.width, 9999)].height;
  frame.size.height = MIN(heightThatFits, frame.size.height);
  label.frame = frame;
}

+ (void)adjustHeightOfLabel:(UILabel *)label maxHeight:(CGFloat)maxHeight {
  CGRect frame;
  frame = label.frame;
  CGFloat heightThatFits = [label sizeThatFits:CGSizeMake(frame.size.width, 9999)].height;
  frame.size.height = MIN(heightThatFits, maxHeight);
  label.frame = frame;
}

+ (void)decreaseWidthOfLabel:(UILabel *)label {
  CGRect frame;
  frame = label.frame;
  float widthThatFits = [label sizeThatFits:label.frame.size].width;
  if (widthThatFits < frame.size.width)
    frame.size.width = widthThatFits;
  label.frame = frame;
}

+ (void)adjustWidthOfLabel:(UILabel *)label maxWidth:(CGFloat)maxWidth {
  CGRect frame;
  frame = label.frame;
  CGFloat widthThatFits = [label sizeThatFits:CGSizeMake(9999, frame.size.height)].width;
  frame.size.width = MIN(widthThatFits, maxWidth);
  label.frame = frame;
}

#pragma mark -

- (void)decreaseHeight {
  CGRect frame;
  frame = self.frame;
  CGFloat heightThatFits =
    [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(self.frame.size.width, MAXFLOAT) lineBreakMode:self.lineBreakMode].height;
  frame.size.height = MIN(heightThatFits, self.frame.size.height);
  self.frame = frame;
}

- (void)decreaseWidth {
  CGRect frame;
  frame = self.frame;
  CGFloat widthThatFits =
    [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(MAXFLOAT, self.frame.size.height) lineBreakMode:self.lineBreakMode].width;
  frame.size.width = MIN(widthThatFits, self.frame.size.width);
  self.frame = frame;
}

- (void)adjustHeightWithMaxHeight:(CGFloat)maxHeight {
  CGRect frame;
  frame = self.frame;
  CGFloat heightThatFits =
    [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(self.frame.size.width, MAXFLOAT) lineBreakMode:self.lineBreakMode].height;
  frame.size.height = MIN(heightThatFits, maxHeight);
  self.frame = frame;
}

- (void)adjustWidthWithMaxWidth:(CGFloat)maxWidth {
  CGRect frame;
  frame = self.frame;
  CGFloat widthThatFits =
    [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(MAXFLOAT, self.frame.size.height) lineBreakMode:self.lineBreakMode].width;
  frame.size.width = MIN(widthThatFits, maxWidth);
  self.frame = frame;
}

- (void)adjustHeightToFit {
  [self adjustHeightWithMaxHeight:MAXFLOAT];
}

- (void)adjustWidthToFit {
  [self adjustWidthWithMaxWidth:MAXFLOAT];
}

@end
