//
//  UIImage+DocumentsPath.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/23/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "UIImage+DocumentsPath.h"

@implementation UIImage (DocumentsPath)

+ (UIImage *)imageFromDocumentsResourceFile:(NSString *)fileName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    NSData *dataFromFile = [[NSFileManager defaultManager] contentsAtPath:dataPath];
    
    return [UIImage imageWithData:dataFromFile];
}

@end
