//  UIScrollView+KeyboardAvoidingAdditions.h
//  Created by Dimitar Tasev on 20140331.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.



@interface UIScrollView (KeyboardAvoidingAdditions)

- (BOOL)keyboardAvoiding_focusNextTextField;
- (void)keyboardAvoiding_scrollToActiveTextField;

- (void)keyboardAvoiding_keyboardWillShow:(NSNotification*)notification;
- (void)keyboardAvoiding_keyboardWillHide:(NSNotification*)notification;
- (void)keyboardAvoiding_updateContentInset;
- (void)keyboardAvoiding_updateFromContentSizeChange;
- (void)keyboardAvoiding_assignTextDelegateForViewsBeneathView:(UIView*)view;
- (UIView*)keyboardAvoiding_findFirstResponderBeneathView:(UIView*)view;
- (CGSize)keyboardAvoiding_calculatedContentSizeFromSubviewFrames;

@end
