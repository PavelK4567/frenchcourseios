//  KeyboardAvoidingCollectionView.h
//  Created by Dimitar Tasev on 20140331.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.



@interface KeyboardAvoidingCollectionView : UICollectionView <UITextFieldDelegate, UITextViewDelegate>

- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;

@end
